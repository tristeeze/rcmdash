<?php include "includes/admin_header.php" ?>

    <div id="wrapper">
        

        <?php include "includes/admin_navigation.php" ?>

        <div id="page-wrapper">
<div class="container-fluid">

                <!-- Page Heading -->
<div class="row">
<div class="col-lg-12">
    <h1 class="page-header">
    WELCOME TO THE ADMIN DASHBOARD
    
    <small><?php echo $_SESSION['user_firstname'] ?></small>
    </h1>
                           <?php
    
 $result = mysqli_query($connection,"SELECT AVG(process_value) AS avg FROM processed_deals"); while($row = mysqli_fetch_array($result)) { 
    
    echo "<strong>AVERAGE PROCESSED DEAL</strong> R" . round($row['avg'], 2); 

}

    ?>

                    </div>
                </div>
                <!-- /.row -->
                
                <?php echo '<h3>' . date("F") . '</h3>'; ?>
                       
                <!-- /.row -->
                
<div class="row">
              <?php 
    $current_month = date("m");
    ?>
              
       
          
  <div class="col-sm-4"> 
           
           <div class="panel panel-orange">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM users WHERE user_group = 'Sales'";
    $select_all_users = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$user_count}</div>";
    
    
    ?>
                        <div> Sales</div>
                  
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div></div>
        
  <div class="col-sm-4">
      <div class="panel panel-purple">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM users WHERE user_group = 'telesales'";
    $select_all_users = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$user_count}</div>";
    
    
    ?>
                        <div> Telesales</div>
                  
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
      
      
  </div>
  <div class="col-sm-4">
      
      <div class="panel panel-blue">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM users WHERE user_group = 'telesalesr'";
    $select_all_users = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$user_count}</div>";
    
    
    ?>
                        <div> Telesales</div>
                  
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
   
  </div>

</div>
                <!-- /.row -->
                
                <!--------STATS-------->
<div class="row">
                 <div class="col-sm-4">
      
    
      <div class="panel panel-purple">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fas fa-money-bill-wave-alt fa-4x"></i>
                    </div>
                  
                    <?php
      $sql="SELECT sum(feedback_sale_value) as total FROM feedback WHERE MONTH(feedback_date) = $current_month ";
    $result = mysqli_query($connection,$sql);
    $row = mysqli_fetch_assoc($result); 
    $the_total =  $row['total'];
 echo "<div class='huge'>R" .$the_total . "</div>";


    ?>
               <div> Current Revenue</div>
                  
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>

  </div>
  <div class="col-sm-4"> 
           
           <div class="panel panel-blue">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                       <i class="far fa-calendar-plus fa-4x" ></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM appointments WHERE MONTH(app_date) = $current_month";
    $select_all_users = mysqli_query($connection, $query);
    $app_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$app_count}</div>";

    ?>
                        <div> Appointments</div>
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div></div>
        
  <div class="col-sm-4">
      <div class="panel panel-orange">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fas fa-calendar-check fa-4x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM feedback WHERE feedback_status = 'processed' AND MONTH(feedback_date) = $current_month";
    $select_all_users = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$user_count}</div>";

    ?>
                        <div> Processed Deals</div>
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>

  </div>

</div>
           <div class="row">
               
  <div class="col-sm-4"> 
           
           <div class="panel panel-orange">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="far fa-calendar-alt fa-5x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM feedback WHERE feedback_status = 'rescheduled' AND MONTH(feedback_date) = $current_month";
    $select_all_users = mysqli_query($connection, $query);
    $app_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$app_count}</div>";
    
    
    ?>
                        <div> Rescheduled</div>
                  
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div></div>
        
  <div class="col-sm-4">
      <div class="panel panel-purple">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="far fa-calendar-minus fa-5x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM feedback WHERE feedback_status = 'now show' AND MONTH(feedback_date) = $current_month";
    $select_all_users = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$user_count}</div>";
    
    
    ?>
                        <div> No Show</div>
                  
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
      
      
  </div>
  <div class="col-sm-4">
      
      <div class="panel panel-blue">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                       <i class="fas fa-calendar-times fa-5x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM feedback WHERE feedback_status = 'cancelled' AND MONTH(feedback_date) = $current_month";
    $select_all_users = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$user_count}</div>";
    
    
    ?>
                        <div> Cancelled Appointments</div>
                  
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
  </div>

</div>
            </div>

            <!-- /.container-fluid -->

        </div>

        <!-- /#page-wrapper -->

  <?php include "includes/admin_footer.php" ?>
    <script src="https://js.pusher.com/4.4/pusher.min.js"></script>
   
  <script>
        
        
      $(document).ready(function()) {
                  
                  
     var pusher = new Pusher('d35b2485a82b61377895', {
                 
            cluster = 'ap2',
            encrypted: true
                 
                 
                    }); 
      
      var notificationChannel = pusher.subscribe('notifications');
      
      notificationChannel.bind('new_user', function(notification){
          
          var message = notification.message;
          
          console.log(message);
          
      });
      
      });              
</script>