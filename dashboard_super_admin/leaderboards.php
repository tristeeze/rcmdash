<?php include "includes/admin_header.php" ?>
   
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/Chart.min.js"></script>
    <div id="wrapper">

        <?php include "includes/admin_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                   <h1 class="page-header">
                            LEADERBOARDS
                           
                        </h1>
    <?php
    
        if(isset($_GET['source'])) {
            
            $source = $_GET['source'];
            
            
        } else {
            
            $source = '';
        }

switch($source) {
      
        
      
        
        case 'leaderboard_appointments';
        include "../views/admin/leaderboard_appointments.php";
        break;
        
        case 'team_leaderboard';
        include "../views/admin/team_leaderboard.php";
        break;     
    
        case 'telesales_leaderboard';
        include "../views/admin/telesales_leaderboard.php";
        break;   
        
        case 'sales_leaderboard';
        include "../views/admin/sales_revenue.php";
        break;
            
        default: 
        
        include "../views/admin/leaderboard_appointments.php";
        
            break;
}
    
    ?>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        
        <!-- /#page-wrapper -->

  <?php include "includes/admin_footer.php" ?>   