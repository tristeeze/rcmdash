<!-- ADMIN Navigation -->
             <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <li class="useravatarmobi">
                        <div class="useravatar">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                 <?php
              
    $current_month = date("m");
   
    $sql="SELECT sum(feedback_sale_value) as total FROM feedback WHERE MONTH(feedback_date) = $current_month ";
    $result = mysqli_query($connection,$sql);
    $row = mysqli_fetch_assoc($result);   

    ?>
            </div>
                <a class="navbar-brand" href="index.php">RCM CMS  <span style="color:#fff;"><?php echo "MONTHLY REVENUE R {$row['total']}"; ?></span> </a>
                           
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php 
                        
                        if(isset($_SESSION['user_firstname'])) {
                        echo $_SESSION['user_firstname']; 
                        }
                        
                        
                        ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                      
                        <li class="divider"></li>
                        <li>
                            <a href="../includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    
                    <?php $activePage = basename($_SERVER['PHP_SELF'], ".php"); ?>
                    
                    <li>
                        <div class="useravatar useravatardrop">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
                   
                    <li class="<?= ($activePage == 'index') ? 'active':''; ?>">
                        <a href="index.php"><i class="fas fa-globe"></i> Dashboard</a>
                    </li> 
                     <li class="<?= ($activePage == 'my-calendar') ? 'active':''; ?>">
                        <a href="my-calendar.php"><i class="fas fa-user"></i> My Calendar</a>
                    </li>
                      <li class="<?= ($activePage == 'calendar') ? 'active':''; ?>">
                        <a href="calendar.php"><i class="fas fa-user"></i> Calendar</a>
                    </li>
                  
                 <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="far fa-calendar-plus" ></i> My Appointments <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="demo" class="collapse">
                                                       <li class="<?= ($activePage == 'appointments') ? 'active':''; ?>">
                                <a href="appointments.php?source=add_appointment">Add Appointment</a>
                            </li>
                          
                             <li class="<?= ($activePage == 'appointments') ? 'active':''; ?>">
                                <a href="appointments.php?source=view_appointments">View All Appointments</a>
                            </li>                             
                               
                               <li class="<?= ($activePage == 'appointments') ? 'active':''; ?>">
                                <a href="appointments.php?source=jellis_appointments">View Jellis Appointments</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fas fa-users"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="demo3" class="collapse">
                            <li class="<?= ($activePage == 'users') ? 'active':''; ?>">
                                <a href="users.php">View All Users</a>
                            </li>
                            <li class="<?= ($activePage == 'add_user') ? 'active':''; ?>">
                                <a href="users.php?source=add_user">Add User</a>
                            </li>                            
                               <li class="<?= ($activePage == 'jhb') ? 'active':''; ?>">
                                <a href="users.php?source=jhb">Order</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fas fa-chart-pie"></i> Statistics<i class="fas fa-fw fa-caret-down"></i></a>
                        
                        <ul id="demo2" class="collapse">
                         
                    <li class="<?= ($activePage == 'view_statistics') ? 'active':''; ?>">
                        <a href="statistics.php?source=telesales_percentages"> Overview </a>
                    </li>
                    <li class="<?= ($activePage == 'view_statistics') ? 'active':''; ?>">
                        <a href="statistics.php?source=view_statistics"> View Appointments</a>
                    </li>                    
                       
                    <li class="<?= ($activePage == 'view_statistics_tele') ? 'active':''; ?>">
                        <a href="statistics.php?source=view_statistics_tele"> View Appointments Telesales</a>
                    </li>
                    <li class="<?= ($activePage == 'appointment_summaries') ? 'active':''; ?>">
                        <a href="statistics.php?source=appointment_summaries"> Consultant</a>
                    </li>
                    <li class="<?= ($activePage == 'telesales_summaries') ? 'active':''; ?>">
                        <a href="statistics.php?source=telesales_summaries"> Telesales </a>
                    </li>
                    <li class="<?= ($activePage == 'telesales_percentages') ? 'active':''; ?>">
                        <a href="statistics.php?source=telesales_percentages"> Telesales Percentages </a>
                    </li>
                    <li class="<?= ($activePage == 'p_telesales_summaries') ? 'active':''; ?>">
                        <a href="statistics.php?source=p_telesales_summaries">Public Telesales </a>
                    </li>
                    <li class="<?= ($activePage == 'revenue_chart') ? 'active':''; ?>">
                        <a href="statistics.php?source=revenue_chart">Revenue Chart </a>
                    </li>
                         </ul>
                    </li>
                    <li >
                        <a href="javascript:;" data-toggle="collapse" data-target="#leaderboards"><i class="fas fa-chart-pie"></i> Leaderboards<i class="fas fa-fw fa-caret-down"></i></a>
                        
                        <ul id="leaderboards" class="collapse">
                         <li class="<?= ($activePage == 'leaderboard_appointments') ? 'active':''; ?>">
                        <a href="leaderboards.php?source=leaderboard_appointments">Appointments Made</a>
                    </li>                         
                       <li class="<?= ($activePage == 'team_leaderboard') ? 'active':''; ?>">
                        <a href="leaderboards.php?source=team_leaderboard">Team Leaderboard</a>
                    </li>
                    <li class="<?= ($activePage == 'telesales_leaderboard') ? 'active':''; ?>">
                        <a href="leaderboards.php?source=telesales_leaderboard">Telesales Leaderboard</a>
                    </li>  
                       <li class="<?= ($activePage == 'sales_leaderboard') ? 'active':''; ?>">
                        <a href="leaderboards.php?source=sales_leaderboard">Sales Revenue Leaderboard</a>
                    </li>
                   
                         </ul>
                    </li>
                     <li>
                        <a href="appointments.php?source=view_feedback"><i class="fa fa-comment"></i> View Feedback</a>
                    </li>
                     <li class="<?= ($activePage == 'leads') ? 'active':''; ?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#leads"><i class="fa fa-calendar"></i> Leads <i class="fa fa-fw fa-caret-down"></i></a>
                       
                        <ul id="leads" class="collapse">
                            <li class="<?= ($activePage == 'view_leads') ? 'active':''; ?>">
                                <a href="leads.php?source=view_leads">VIEW LEADS</a>
                            </li>                             
                           <li class="<?= ($activePage == 'my_leads') ? 'active':''; ?>">
                                <a href="leads.php?source=my_leads">MY LEADS</a>
                            </li>  
                            <li class="<?= ($activePage == 'view_clicked_leads') ? 'active':''; ?>">
                                <a href="leads.php?source=view_clicked_leads">CLICKED LEADS</a>
                            </li>                             

                           <li class="<?= ($activePage == 'view_lead_feedback') ? 'active':''; ?>">
                                <a href="leads.php?source=view_lead_feedback">FEEDBACK</a>
                            </li>
                           <li class="<?= ($activePage == 'jellis_leads') ? 'active':''; ?>">
                                <a href="leads.php?source=jellis_leads">JELLIS</a>
                            </li>
                             <li class="<?= ($activePage == 'all_jellis_leads') ? 'active':''; ?>">
                                <a href="leads.php?source=all_jellis_leads">ALL JELLIS</a>
                            </li>
                           
                        </ul>
                    </li>
                     <li class="<?= ($activePage == 'pipeline') ? 'active':''; ?>">
                        <a href="pipeline.php"><i class="fas fa-user"></i> Pipeline</a>
                    </li>    
                       
                   <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#rates"><i class="far fa-calendar-plus" ></i> Rate Cards <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="rates" class="collapse">
                                                
                             
                            <li>
                                <a href="rates.php?source=rate_card">Rate Card</a>
                            </li>       
                               <li>
                                <a href="rates.php?source=add_rate_item">Add Rate Item</a>
                            </li>   
                               
                          
                         
                        </ul>
                    </li>
                       
                       <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#boardroom"><i class="fa fa-calendar"></i> Boardroom <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="boardroom" class="collapse">
                            <li class="<?= ($activePage == 'book_boardroom') ? 'active':''; ?>">
                                <a href="boardroom.php?source=book_boardroom">BOOK BOARDROOM</a>
                            </li> 
                               <li class="<?= ($activePage == 'view_boardroom') ? 'active':''; ?>">
                                <a href="boardroom.php?source=view_boardroom">VIEW BOARDROOM</a>
                            </li> <li class="<?= ($activePage == 'my_boardroom') ? 'active':''; ?>">
                                <a href="boardroom.php?source=my_boardroom">MY BOARDROOM</a>
                            </li>
                           
                          
                        </ul>
                    </li>
                     
                            <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#boardroom2"><i class="fa fa-calendar"></i> Boardroom Sales<i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="boardroom2" class="collapse">
                            <li class="<?= ($activePage == 'book_boardroom') ? 'active':''; ?>">
                                <a href="sales_boardroom.php?source=book_boardroom">BOOK BOARDROOM</a>
                            </li> 
                               <li class="<?= ($activePage == 'view_boardroom') ? 'active':''; ?>">
                                <a href="sales_boardroom.php?source=view_boardroom">VIEW BOARDROOM</a>
                            </li> <li class="<?= ($activePage == 'my_boardroom') ? 'active':''; ?>">
                                <a href="sales_boardroom.php?source=my_boardroom">MY BOARDROOM</a>
                            </li>
                           
                          
                        </ul>
                    </li>
                      <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#van"><i class="fa fa-calendar"></i> Book Transport <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="van" class="collapse">
                            <li>
                                <a href="book_van.php?source=book_van">BOOK VAN</a>
                            </li> 
                               <li>
                                <a href="book_van.php?source=view_van">VAN AVAILABILITY</a>
                            </li>  
                               
                               <li>
                                <a href="book_van.php?source=my_van">YOUR BOOKINGS</a>
                            </li> 
                           
                          
                        </ul>
                         </li>
                     
                   
                       <li class="<?= ($activePage == 'profile') ? 'active':''; ?>">
                        <a href="profile.php"><i class="fas fa-user"></i> Profile</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>