<?php include "includes/admin_header.php" ?>
    <div id="wrapper">

        <?php include "includes/admin_navigation.php" ?>

    <div id="page-wrapper">

            <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">

               <h1 class="page-header">STATISTICS</h1>
        <?php

        if(isset($_GET['source'])) {

            $source = $_GET['source'];

        } else {

            $source = '';
        }

        switch($source) {
        case 'view_statistics';
        include "../views/admin/view_statistics.php";
        break ;        
    
        case 'view_statistics_tele';
        include "../views/admin/view_statistics_tele.php";
        break ;

        case 'appointment_summaries';
        include "../views/admin/appointment_summaries.php";
        break;

        case 'telesales_summaries';
        include "../views/admin/telesales_summaries.php";
        break;

        case 'p_telesales_summaries';
        include "../views/admin/p_telesales_summaries.php";
        break;        
    
        case 'telesales_percentages';
        include "../views/admin/pie_charts.php";
        break;

        case 'revenue_chart';
        include "../views/admin/revenue_chart.php";
        break;
        
        default:
        include "../views/admin/view_statistics.php";
        break;
}

    ?>
    <script type="text/javascript">
    var tableToExcel = (function() {
      var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
      return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
        window.location.href = uri + base64(format(template, ctx))
      }
    })()
    </script>

    <script src="../js/tableexport.min.js" type="text/javascript"></script>
    <script src="../js/FileSaver.min.js" type="text/javascript"></script>
    <script>
      $("#statistics").tableExport();
    </script>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>

        <!-- /#page-wrapper -->

  <?php include "includes/admin_footer.php" ?>