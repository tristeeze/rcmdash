<?php include "includes/admin_header.php" ?>
    <div id="wrapper">

        <?php include "includes/admin_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

                   <h1 class="page-header">
                            APPOINTMENTS

                        </h1>
    <?php

        if(isset($_GET['source'])) {

            $source = $_GET['source'];


        } else {

            $source = '';
        }

switch($source) {
        case 'add_appointment';
        include "../views/sales/add_appointment.php";
        break ;

        case 'view_feedback';
        include "../views/sales/view_feedback_sales.php";
        break;
        
        case 'view_lead_feedback';
        include "../views/admin/view_lead_feedback.php";
        break;

        case 'add_feedback';
        include "../views/sales/add_feedback_sales.php";
        break;

        case 'reschedule_appointment';
        include "../views/sales/reschedule_appointment_sales.php";
        break;
        
                
        case 'single_appointment';
        include "../views/sales/single_appointment.php";
        break;


        case 'edit_appointment';
        include "../views/sales/edit_appointment_sales.php";
        break;
        
        case 'second_meeting';

        include "../views/sales/second_meeting.php";

        break;
        
        case 'jellis_appointments';

        include "../views/global/all_jellis_appointments.php";

        break;

        default:
        include "../views/sales/view_appointments.php";
        break;
}

    ?>
    <script src="../js/tableexport.min.js" type="text/javascript"></script>
    <script src="../js/FileSaver.min.js" type="text/javascript"></script>
<!--    <script src="../js/bootstrap.min_1.js" type="text/javascript"></script>-->

    <script>
      $("#appointments").tableExport();
    </script>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>

        <!-- /#page-wrapper -->

  <?php include "includes/admin_footer.php" ?>
