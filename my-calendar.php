<?php include "includes/admin_header.php" ?>    
        <div id="wrapper">

        <?php include "includes/admin_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                   <h1 class="page-header">
                            BOOKED APPOINTMENTS
                        
                        </h1>
    
                    
                    <?php
// Set your timezone
date_default_timezone_set('Africa/Johannesburg');

// Get prev & next month
if (isset($_GET['ym'])) {
    $ym = $_GET['ym'];
} else {
    // This month
    $ym = date('Y-m');
}
// Check format
$timestamp = strtotime($ym . '-01');
if ($timestamp === false) {
    $ym = date('Y-m');
    $timestamp = strtotime($ym . '-01');
}
// Today
$today = date('Y-m-j', time());
// For H3 title
$html_title = date('Y / m', $timestamp);
// Create prev & next month link     mktime(hour,minute,second,month,day,year)
$prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
$next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));
// You can also use strtotime!
// $prev = date('Y-m', strtotime('-1 month', $timestamp));
// $next = date('Y-m', strtotime('+1 month', $timestamp));
// Number of days in the month
$day_count = date('t', $timestamp);
 
// 0:Sun 1:Mon 2:Tue ...
$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));
//$str = date('w', $timestamp);
// Create Calendar!!
$weeks = array();
$week = '';

   

// Add empty cell
$week .= str_repeat('<td></td>', $str);
 
for ( $day = 1; $day <= $day_count; $day++, $str++) {
     
    $date = $ym . '-' . $day;
        
    if ($today == $date ) {
        $week .= '<td class="today">' . $day . '<br />';
        
    } 
else {
     $week .= '<td>' . $day . '<br />';
}
    
    

   // LOCATED IN INCLUDES/FUNCTIONS IN THE MAIN FOLDER
echo find_my_calendar();

$week .= '</td>';
     

    // End of the week OR End of the month
    if ($str % 7 == 6 || $day == $day_count) {
        if ($day == $day_count) {
            // Add empty cell
            $week .= str_repeat('<td></td>', 6 - ($str % 7));
        }
        $weeks[] = '<tr>' . $week . '</tr>';
        // Prepare for new week
        $week = '';
    }
}


?>
                    
                    <div class="container">
        <h3><a href="?ym=<?php echo $prev; ?>">&lt;</a> <?php echo $html_title; ?> <a href="?ym=<?php echo $next; ?>">&gt;</a></h3>
        <table class="table table-bordered">
            <tr>
                <th>SUN</th>
                <th>MONDAY</th>
                <th>TUESDAY</th>
                <th>WEDNESDAY</th>
                <th>THURSDAY</th>
                <th>FRIDAY</th>
                <th>SAT</th>
            </tr>
            <?php
            
             
      foreach ($weeks as $week) {
                    echo  $week;
                   
                }
    
              
            ?>
        </table>
        
        <script type='text/javascript'>
$(function(){
var overlay = $('<div id="overlay"></div>');
overlay.show();
overlay.appendTo(document.body);
$('.popup').show();
$('.close').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});

$('.x').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});
});
</script>
                         <?php
    
$query = "SELECT * FROM appointments WHERE app_consultant = '{$_SESSION['user_firstname']}' AND app_feedback = 'none' AND app_approved = 'approved' AND app_rescheduled = 'No'";
$select_appointments = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_appointments)) {
date_default_timezone_set('Africa/Johannesburg');
$app_id = $row['app_id'];
$date_now = date("Y-m-d");
$app_date = $row['app_date'];    
    
    if($date_now > $app_date) {
echo "<div class='popup'>";
echo "<div class='cnt223'>";
echo "<h1>Update Feedback</h1>";
echo "<p>";
echo "You need to update your previous appointment feedback";
echo "<br/>";
echo "<a href='appointments.php?source=add_feedback&p_id={$app_id}' >Update Feedback</a>";
echo "</p>";
echo "</div>";
echo "</div>";
} 
}
?>
    </div>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        
        <!-- /#page-wrapper -->


 <?php include "includes/admin_footer.php" ?>  