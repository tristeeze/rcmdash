<?php include'includes/header.php' ?>
    <?php include'includes/db.php' ?>

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

<style>
/* Login */
.bookCallwrap {
    width: 100%;
    background: url(images/call_bg.png);
/*    background-size: contain;*/
    background-attachment: fixed;
    background-size: cover;
    overflow: hidden;
}
    
    h1 {
        color: #3753a6;
        font-size: 36px;
        font-weight: bold;
    }
body {
    padding-top: 0px !important;
    font-size: 12px;
    font-family: 'Montserrat', sans-serif;
}
.callwrap {
    background-color: rgba(255,255,255,0.7);
    color: #333;
    width:60%;
    margin: auto;
    font-size: 14px;
}

.panel-default {
    border-color: rgba(0,0,0,0);
}
button.btn.btn-primary.btn-lg.btn-block {
    border-radius:0 50px 50px 0;
}
    .input-group-addon {
        padding: 6px 12px;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: #fff;
        border: 0px solid #ccc;
        border-radius: 100%;
}
.panel {
    background-color: #fff0 !important;
}
    
        
    .icons {
        position: fixed;
        right:0;
        top:10%;
        width:110px;
        z-index: 999999 !important;
            
    }
    
    .icons img {
        width:100px;
        display: block
    }
/* Login */
    
    @media (max-width:600px) {
        h1 {
            font-weight: bold;
            font-size: 22px;
        }
        .callwrap {
            width: 90%;
        }
        
            
    .icons {
        position: fixed;
        right:0;
        top: unset !important;
        bottom:0 !important;
        width:100% !important;
        z-index: 999999 !important;
        background-color:#fff;
        padding:10px;
            
    }
    
    .icons img {
        width:60px !important;
        display: inline !important; 
    }
    }
    
    
textarea,
input.text,
input[type="text"],
input[type="button"],
input[type="submit"],
    input[type=time],
    input[type=date],
.input-checkbox {
-webkit-appearance: none !important;
}

</style>

<?php
if(isset($_GET['id']))
{

$sql = "SELECT * FROM jellisLeads where id = ".$_GET['id']." ";

if($result = mysqli_query($connection,$sql)) 
{
  while($row = mysqli_fetch_assoc($result)) 
  {
      $the_id           = $_GET['id'];
      $name             = $row['name'];
      $surname          = $row['surname'];
      $address          = $row['address'];
      $email            = $row['email'];
      $company          = $row['companyName'];
      $number           = $row['phone'];
      $url              = $row['url'];
      $province         = $row['province'];
      $country          = $row['audit_for']; 
      
$query2 = "UPDATE jellisLeads SET clicked = 'yes' WHERE id = $the_id";

$update_jellis = mysqli_query($connection, $query2);

confirm($update_jellis);
      
  }
}

  
}
  ?>

<?php
    $user_group     = 'telesales';
    $manager        = 'manager';
    $user_status    = 'active';

//$query2 = "UPDATE jellisLeads SET clicked = 'yes' WHERE companyName = $company";
//
//$update_jellis = mysqli_query($connection, $query2);
//
//confirm($update_jellis);


     $query = "SELECT user_id FROM users WHERE (user_group = '" .$user_group . "' OR user_group = '" . $manager ."') AND user_status = '" . $user_status . "' ORDER BY RAND() LIMIT 1";
     $select_users = mysqli_query($connection,$query);
        while($row = mysqli_fetch_assoc($select_users)) {

            $user_id              = $row['user_id'];

        }

if(isset($_POST['book_call'])) {
    $to                 = $_POST['email']; 
    $app_date           = $_POST['call_date'];
    $app_time           = $_POST['call_time'];
    $subject            = 'Call Booked';
    // STYLE EMAIL
  
    // EMAIL BODY
    $message            =  "<table style='text-align:center;background-color: #ffffff;border:4px solid #f3f3f3;max-width:700px;color:#3767ae' cellpadding='5' cellspacing='5' border='0'>";
    $message           .=  "<tr style='text-align:center;background-color: #ffffff;max-width:700px;color:#3767ae'>";
    $message           .=  "<th colspan='2'><div style='text-align:center;background-color: #f3f3f3;max-width:700px;color:#3767ae'><img src='https://rcmint-sales.com/images/header.png'><br /><h1 >YOUR CALL HAS BEEN BOOKED FOR</h1><h3>DATE: " . $app_date ." AT " . $app_time."</h3><p>&nbsp;</p></div></th>";
    $message           .=  "</tr>";     
    $message           .=  "<tr style='text-align:center;background-color: #ffffff;max-width:700px;color:#3767ae'>";
    $message           .=  "<td colspan='2'><p></p><h3>Contact us for any further assistance</h3></td>";
    $message           .=  "</tr>"; 
    $message           .=  "<tr style='text-align:center;background-color: #ffffff;max-width:700px;color:#3767ae'>"; 
    $message           .=  "<td><p><a href='mailto:marketing@rightclickmedia.co.za'><img src='https://rcmint-sales.com/images/email_icon.png'></a></p></td>"; 
    $message           .=  "<td><p><a href='tel:011-867-6380'><img src='https://rcmint-sales.com/images/phone_icon.png'></a></p></td>"; 
    $message           .=  "</tr>"; 
        $message           .=  "<tr style='text-align:center;background-color: #ffffff;max-width:700px;color:#3767ae'>";
    $message           .=  "<td colspan='2'><img src='https://rcmint-sales.com/images/the_footer.png'></td>";
    $message           .=  "</tr>"; 
    $message           .=  "</table>";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    mail($to, $subject, $message, $headers);
    
}
    
    if(isset($_POST['book_call'])) {

    $is_owner           = $_POST['owner'];
    $inBusiness         = $_POST['in_business'];
    $callTime           = $_POST['call_time'];
    $callDate           = $_POST['call_date'];
    $companyName        = $_POST['company_name'];   
    $name               = $_POST['name'];
    $email              = $_POST['email'];
    $contactNumber      = $_POST['number'];
    $country            = $_POST['country'];
    $province           = $_POST['province'];
    $lead               = $_POST['lead'];
    $status             = 'assigned';
    $the_jellis_id      = $_GET['id'];
        
$query = "INSERT INTO leads(jellis_id, isOwner, inBusiness, callTime, callDate, companyName, name, email, contactNumber, country, province, sales ,status, lead)";

$query .= "VALUES('{$the_jellis_id}','{$is_owner}', '{$inBusiness}', '{$callTime}', '{$callDate}', '{$companyName}', '{$name}', '{$email}','{$contactNumber}','{$country}','{$province}','{$user_id}','{$status}','{$lead}')";
        
$query2 = "UPDATE jellisLeads SET actioned = 'booked' WHERE id = $the_jellis_id";

$create_user_query = mysqli_query($connection, $query);
$update_jellis = mysqli_query($connection, $query2);

confirm($create_user_query);
confirm($update_jellis);

$timeError = "CALL BOOKED";
//$location = 'call_scheduled.php'   ;     
//redirect($location);
//         header("Location:call_scheduled.php");
         
echo "<script> location.replace('call_scheduled.php'); </script>";
        
    }

    
    ?>

<div class="bookCallwrap">

    <!-- Page Content -->
<!--    <div class="container">-->
<?php
if(isset($_GET['id']))
{
    

$sql = "SELECT * FROM jellisLeads where id = ".$_GET['id']." ";

if($result = mysqli_query($connection,$sql)) 
{
  while($row = mysqli_fetch_assoc($result)) 
  {
      $name             =   $row['name'];
      $surname          =   $row['name'];
      $address          =   $row['address'];
      $email            =   $row['email'];
      $company          =   $row['companyName'];
      $contactNumber    =   $row['phone'];
      $province         =   $row['province']; 
      $country          =   $row['audit_for']; 

  }
}

}
  ?>
    <div class="row">
  <div class="col-sm-12">
        <div class="form-gap"></div>
<!--        <div class="container">-->
            <div class="row bookCall">
                <div class="callwrap">
                    <div class="panel panel-default">
                        <div class="panel-body ">
     
                            <div class="text-center">
  <div class="icons">
    <img src="images/icons-02.png"> <img src="images/icons-03.png"> <img src="images/icons-04.png"> <img src="images/icons-05.png"> <img src="images/icons-06.png">
</div>

<h1 class="text-center">BOOK A CALL</h1>
<div class="panel-body">

  <?php if(isset($message)) { ?>
  <div class="messagereq <?php echo $type; ?>"><?php echo $message; ?></div>
  <?php } ?>

    <form id="myForm" action="" method="post" enctype="multipart/form-data">

      <div class="form-group">
          <label for="date">Are you the owner of the company?</label><br>
            <select id="owner" name="owner" class="form-control" required>
        <option value="Yes">Yes</option>
        <option value="No">No</option>


  </select>


      </div>

      <div class="form-group">
           <label for="male">How many years have you been in business</label><br>
   <select id="cars" name="in_business" class="form-control" required>
        <option value="0-5">0-5</option>
        <option value="5-10">5-10</option>
        <option value="10+">10+ </option>

  </select>
      </div>
       <div class="form-group">

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script> 
<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
<script>
webshims.setOptions('forms-ext', {types: 'date'});
webshims.polyfill('forms forms-ext');
$.webshims.formcfg = {
en: {
dFormat: '-',
dateSigns: '-',
patterns: {
d: "yy-mm-dd"
}
}
};
</script>
           <label for="date">Date of Call</label><br>


    <input type="date" id="date" name="call_date" class="form-control" placeholder="yyyy/mm/dd" required>

      </div>   

      <div class="form-group">
           <label for="time">Preferred time of call</label><br>


    <input type="time" id="appt" name="call_time" class="form-control" placeholder="--:--" required>

      </div>


       <div class="form-group">

    <input type="text" id="company_name" name="company_name" class="form-control" placeholder="Company Name" value="<?php echo $company ?>" required>

      </div>    

      <div class="form-group">

    <input type="text" id="name" name="name" class="form-control" placeholder="Name" value="<?php echo $name ?>" required>

      </div> 


      <div class="form-group">

    <input type="email" id="email" name="email" class="form-control" placeholder="Work E-mail" value="<?php echo $email ?>" required>


      </div>                             

        <div class="form-group">

    <input type="text" id="contact" name="number" class="form-control" placeholder="Contact Number" value="<?php echo $contactNumber ?>" required>

      </div>

  <div class="form-group">
      <label for="province">Country</label><br>
     

<select id="country" name="country" class="form-control" required>
      <option >Select Country</option>
        <option value="RSA" <?php if($country == "RSA"){ echo "selected"; } ?>>South Africa</option>
        <option value="UK" <?php if($country == "UK"){ echo "selected"; } ?>>UK</option>

  </select>
           
   
      </div>                    

  <div class="form-group">
           <label for="province">Province</label><br>
   <select id="province" name="province" class="form-control">
        <option >Select Province</option>
        <option value="EC" <?php if($province == 'EC'){ echo 'selected'; }?> >Eastern Cape</option>
        <option value="FS" <?php if($province == 'FS'){ echo 'selected'; }?>>Free State</option>
        <option value="GP" <?php if($province == 'GP'){ echo 'selected'; }?>>Gauteng</option>
        <option value="KZN" <?php if($province == 'KZN'){ echo 'selected'; }?>>KwaZulu-Natal</option>
        <option value="LP" <?php if($province == 'LP'){ echo 'selected'; }?>>Limpopo</option>
        <option value="MP" <?php if($province == 'MP'){ echo 'selected'; }?>>Mpumalanga</option>
        <option value="NC" <?php if($province == 'NC'){ echo 'selected'; }?>>Northern Cape</option>
        <option value="NW" <?php if($province == 'NW'){ echo 'selected'; }?>>North West</option>
        <option value="WC" <?php if($province == 'WC'){ echo 'selected'; }?>>Western Cape</option>

  </select>
      </div>

           <div class="form-group">
           <label for="province">Product</label><br>
   <select id="cars" name="lead" class="form-control">
        <option value="<?php echo $_GET['lead'] ?>"><?php echo $_GET['lead'] ?></option>
        <option value="Adwords">Google AdWords</option>
        <option value="SEO">SEO</option>
        <option value="Youtube">YouTube</option>
        <option value="Social">Social</option>
        <option value="All">All</option>

  </select>
      </div>

        <span class="input-group-btn">
            <button class="btn btn-primary btn-lg btn-block" name="book_call" type="submit"><span>BOOK NOW</span></button>

        </span>
    </form>

    <!-- Blog Sidebar Widgets Column -->

    <hr>
    <?php include'includes/footer.php' ?>

</div><!-- Body-->

</div>
</div>
</div>
</div>
</div>
            
<!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------BOOK APPOINTMENT-->
            
            
<!--        </div> container -->
  </div>

</div>

</div><!-- loginwrap -->
