<?php include "includes/sales_header.php" ?>

    <div id="wrapper">
        

        <?php include "includes/sales_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
<div class="row">
<div class="col-lg-12">
    <h1 class="page-header">
    SALES MANAGER DASHBOARD
    <small><?php echo $_SESSION['user_firstname'] ?></small>
     <small><?php echo $_SESSION['user_team'] ?></small>
    </h1>
    
    
     <?php
              
    $current_month = date("m");
   
    $sql="SELECT sum(feedback_sale_value) as total FROM feedback WHERE MONTH(feedback_date) = $current_month AND feedback_sales = '{$_SESSION['user_firstname']}'";
    $result = mysqli_query($connection,$sql);
    $row = mysqli_fetch_assoc($result);   

    ?>
    
     
                <a class="navbar-brand" href="index.php">YOUR MONTHLY PROCESSED REVENUE <?php echo "<strong>R {$row['total']}</strong>"; ?></a>
<script type='text/javascript'>
$(function(){
var overlay = $('<div id="overlay"></div>');
overlay.show();
overlay.appendTo(document.body);
$('.popup').show();
$('.close').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});

$('.x').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});
});
</script>
                   <?php
    
// $query = "SELECT * FROM appointments WHERE app_consultant = '{$_SESSION['user_firstname']}' AND app_feedback = 'none'";
// $select_appointments = mysqli_query($connection,$query);
// while($row = mysqli_fetch_assoc($select_appointments)) {
// date_default_timezone_set('Africa/Johannesburg');
// $app_id = $row['app_id'];
// $date_now = date("Y-m-d");
// $app_date = $row['app_date'];    
    
//     if($date_now > $app_date) {
// echo "<div class='popup'>";
// echo "<div class='cnt223'>";
// echo "<h1>Update Feedback</h1>";
// echo "<p>";
// echo "You need to update your previous appointment feedback";
// echo "<br/>";
// echo "<a href='appointments.php?source=add_feedback&p_id={$app_id}' >Update Feedback</a>";
// echo "</p>";
// echo "</div>";
// echo "</div>";
// } 
// }
?>
                    </div>
                    <div class="col-lg-12">
         
                   
  <form method="post">
   
   <div class="row">

  <div class="col-sm-4"><div class="form-group">
         <label for="post_status">From Date</label>
           <input type="date" class="form-control" name="from_date">
     </div></div>
  <div class="col-sm-4"> <div class="form-group">
         <label for="post_status">To Date</label>
           <input type="date" class="form-control" name="to_date">
     </div></div>
</div>

        <input type="submit" value="search" name="filter" class="button btn"> 
</form> 
   <?php

?>

     <div class="row">
    <div class="col-sm-6">
        <?php include "../views/sales/chart_sales_leads.php" ?>
    </div>
    <div class="col-sm-6">
        <?php include "../views/sales/chart_sales_status.php" ?>
    </div>
      </div>
      
     
      
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
  <?php include "includes/sales_footer.php" ?>