<?php include "includes/sales_header.php" ?>
   
   <?php
    
    if(isset($_SESSION['user_firstname'])) {
        
        $user_firstname = $_SESSION['user_firstname'];
        
        $query = "SELECT * FROM users WHERE user_firstname = '{$user_firstname}' ";
        
        
        $select_user_profile_query = mysqli_query($connection, $query);
        
        while($row = mysqli_fetch_array($select_user_profile_query)) {

            $user_id = $row['user_id'];
            $user_password = $row['user_password'];
            $user_firstname = $row['user_firstname'];
            $user_lastname = $row['user_lastname'];
            $user_email = $row['user_email'];
            $user_number = $row['user_number'];
        }
        
    }
    
    ?>
    
<?php

    if(isset($_POST['edit_user'])) {
    
    $user_firstname = $_POST['user_firstname'];
    $user_lastname = $_POST['user_lastname'];
    $user_email = $_POST['user_email'];
    $user_password = $_POST['user_password'];
    $user_number = $_POST['user_number'];
    $usersh = password_hash($user_password, PASSWORD_DEFAULT);

   
$query = "UPDATE users SET ";
    $query .="user_firstname  = '{$user_firstname}', ";
    $query .="user_lastname = '{$user_lastname}', ";
    $query .="user_email = '{$user_email}', ";
    $query .="user_number = '{$user_number}', ";
    $query .="user_password   = '{$usersh}' ";
    $query .="WHERE user_firstname = '{$user_firstname}' ";

    
    $edit_user_query  = mysqli_query($connection, $query); 
    
    confirm($edit_user_query);
}

?>
    <div id="wrapper">

        <?php include "includes/sales_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                   <h1 class="page-header">
                            EDIT YOUR PROFILE
                            
                        </h1>
    <form action="" method="post" enctype="multipart/form-data">    
     
     <div class="form-group">
         <label>First Name</label>
         <input type="text" value="<?php echo $user_firstname ?>" class="form-control" name="user_firstname">
     </div>
     
        <div class="form-group">
         <label for="title">Last Name</label>
         <input type="text" value="<?php echo $user_lastname?>" class="form-control" name="user_lastname">
     </div>  
             
             
       <div class="form-group">
         <label for="post_status">Email</label>
           <input type="email" value="<?php echo $user_email?>" class="form-control" name="user_email">
     </div> 
         <div class="form-group">
         <label for="post_status">Contact Number</label>
           <input type="text" value="<?php echo $user_number?>" class="form-control" name="user_number">
     </div> 
     
     <div class="form-group">
         <label for="post_status">Password</label>
           <input type="password" value="<?php echo $user_password ?>" class="form-control" name="user_password">
     </div>
     
     
<div class="form-group">
    <input class="btn btn-primary" type="submit" name="edit_user" value="Update Profile">
     </div>
</form>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        
        <!-- /#page-wrapper -->

  <?php include "includes/sales_footer.php" ?>   