<?php include'includes/header.php' ?>
    <?php include'includes/db.php' ?>

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

<style>
/* Login */
.bookCallwrap {
    width: 100%;
    background: url(images/call_bg.png);
/*    background-size: contain;*/
    background-attachment: fixed;
    background-size: cover;
    overflow: hidden;
}
    
    h1 {
        color: #3753a6;
        font-size: 36px;
        font-weight: bold;
    }
body {
    padding-top: 0px !important;
    font-size: 12px;
    font-family: 'Montserrat', sans-serif;
}
.callwrap {
    background-color: rgba(255,255,255,0.7);
    color: #333;
    width:60%;
    margin: auto;
    font-size: 14px;
}

.panel-default {
    border-color: rgba(0,0,0,0);
}
button.btn.btn-primary.btn-lg.btn-block {
    border-radius:0 50px 50px 0;
}
    .input-group-addon {
        padding: 6px 12px;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: #fff;
        border: 0px solid #ccc;
        border-radius: 100%;
}
.panel {
    background-color: #fff0 !important;
}
    
        
    .icons {
        position: fixed;
        right:0;
        top:10%;
        width:110px;
        z-index: 999999 !important;
            
    }
    
    .icons img {
        width:100px;
        display: block
    }
/* Login */
    
    @media (max-width:600px) {
        h1 {
            font-weight: bold;
            font-size: 22px;
        }
        .callwrap {
            width: 90%;
        }
        
            
    .icons {
        position: fixed;
        right:0;
        top: unset !important;
        bottom:0 !important;
        width:100% !important;
        z-index: 999999 !important;
        background-color:#fff;
        padding:10px;
            
    }
    
    .icons img {
        width:60px !important;
        display: inline !important; 
    }
    }
    
    
textarea,
input.text,
input[type="text"],
input[type="button"],
input[type="submit"],
    input[type=time],
    input[type=date],
.input-checkbox {
-webkit-appearance: none !important;
}

</style>


<div class="bookCallwrap">
<?php
if(isset($_GET['id']))
{

$sql = "SELECT * FROM jellisLeads where id = ".$_GET['id']." ";

if($result = mysqli_query($connection,$sql)) 
{
  while($row = mysqli_fetch_assoc($result)) 
  {
      $the_id           = $_GET['id'];
      $name             = $row['name'];
      $surname          = $row['surname'];
      $address          = $row['address'];
      $email            = $row['email'];
      $company          = $row['companyName'];
      $contactNumber    = $row['phone'];
      $url              = $row['url'];
      $province         = $row['province'];
      $country          = $row['audit_for']; 
      
$query2 = "UPDATE jellisLeads SET clicked = 'yes' WHERE id = $the_id";

$update_jellis = mysqli_query($connection, $query2);

confirm($update_jellis);
      
  }
}

  
}
  ?>
    <!-- Page Content -->
<!--    <div class="container">-->
    
    

    <div class="row">
  <div class="col-sm-12">
        <div class="form-gap"></div>
<!--        <div class="container">-->
           <div class="row bookCall">
                            <div class="callwrap">
                             <div class="panel panel-default">
                        <div class="panel-body ">
                         <div class="text-center">

            <?php
                             


$timeError = "Book Appointment";

if(isset($_POST['add_appointment'])) {
    $to                 = $_POST['app_email']; 
    $app_date           = $_POST['app_date'];
    $app_time           = $_POST['app_time'];
    $subject            = 'Appointment Booked';
    // STYLE EMAIL
  
    // EMAIL BODY
    $message            =  "<table style='text-align:center;background-color: #ffffff;border:4px solid #f3f3f3;max-width:700px;color:#3767ae' cellpadding='5' cellspacing='5' border='0'>";
    $message           .=  "<tr style='text-align:center;background-color: #ffffff;max-width:700px;color:#3767ae'>";
    $message           .=  "<th colspan='2'><div style='text-align:center;background-color: #f3f3f3;max-width:700px;color:#3767ae'><img src='https://rcmint-sales.com/images/header.png'><br /><h1 >YOUR APPOINTMENT HAS BEEN BOOKED FOR</h1><h3>APPOINTMENT DATE: " . $app_date ." AT " . $app_time."</h3><p>&nbsp;</p></div></th>";
    $message           .=  "</tr>";     
    $message           .=  "<tr style='text-align:center;background-color: #ffffff;max-width:700px;color:#3767ae'>";
    $message           .=  "<td colspan='2'><p></p><h3>Contact us for any further assistance</h3></td>";
    $message           .=  "</tr>"; 
    $message           .=  "<tr style='text-align:center;background-color: #ffffff;max-width:700px;color:#3767ae'>"; 
    $message           .=  "<td><p><a href='mailto:marketing@rightclickmedia.co.za'><img src='https://rcmint-sales.com/images/email_icon.png'></a></p></td>"; 
    $message           .=  "<td><p><a href='tel:011-867-6380'><img src='https://rcmint-sales.com/images/phone_icon.png'></a></p></td>"; 
    $message           .=  "</tr>"; 
        $message           .=  "<tr style='text-align:center;background-color: #ffffff;max-width:700px;color:#3767ae'>";
    $message           .=  "<td colspan='2'><img src='https://rcmint-sales.com/images/the_footer.png'></td>";
    $message           .=  "</tr>"; 
    $message           .=  "</table>";
    
  

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    mail($to, $subject, $message, $headers);
    
}
if(isset($_POST['add_appointment'])) {

    $app_business       = $_POST['app_business'];
    $app_address        = $_POST['app_address'];
    $app_area           = $_POST['app_area'];
    $app_country        = $_POST['app_country'];
    $app_name           = $_POST['app_name'];   
    $app_number         = $_POST['app_number'];
    $app_alt_number     = $_POST['app_alt_number'];
    $app_website        = $_POST['app_website'];
    $app_email          = $_POST['app_email'];
    $app_comments       = $_POST['app_comments'];
    $app_competitors    = $_POST['app_competitors'];
    $app_time           = $_POST['app_time'];
    $app_date           = $_POST['app_date'];
    $unnaproved         = 'unapproved';
    $pending            = 'pending';
    $app_approved       = 'approved';
    $type               = 'meeting';
    
if ($_POST['app_area'] == 'JHB') {
    
    
     $sql2 = "SELECT users.user_firstname FROM JHB INNER JOIN users ON JHB.sales = users.user_id order by JHB.level ASC";

if($result = mysqli_query($connection,$sql2)) 
    
$sales = array();
while ($row = mysqli_fetch_assoc($result)) {
$sales[] = $row['user_firstname'];
//             $sales = array("Jeff");



    }
//         $sales = array("Vaughan", "Claudio", "Janine", "Jeff", "Elio");
    
        } elseif ($_POST['app_area'] == 'PTA') {
    
    
     $sql2 = "SELECT users.user_firstname FROM PTA INNER JOIN users ON PTA.sales = users.user_id order by PTA.level ASC";

if($result = mysqli_query($connection,$sql2)) 
    
$sales = array();
while ($row = mysqli_fetch_assoc($result)) {
$sales[] = $row['user_firstname'];
//             $sales = array("Jeff");

    
//     $sales = array("Dean", "Vaughan", "Janine", "Jeff", "Elio");
    
    } }elseif ($_POST['app_area'] == 'CT') {
    
         $sql2 = "SELECT users.user_firstname FROM PTA INNER JOIN users ON PTA.sales = users.user_id order by PTA.level ASC";

if($result = mysqli_query($connection,$sql2)) 
    
$sales = array();
while ($row = mysqli_fetch_assoc($result)) {
$sales[] = $row['user_firstname'];
//             $sales = array("Jeff");

    
    
    }
    
//         $sales = array("Craig", "Rian", "Janine");
    
        } elseif ($_POST['app_country'] == 'UK') {
    
             $sql2 = "SELECT users.user_firstname FROM UK INNER JOIN users ON UK.sales = users.user_id order by UK.level ASC";

if($result = mysqli_query($connection,$sql2)) 
    
$sales = array();
while ($row = mysqli_fetch_assoc($result)) {
$sales[] = $row['user_firstname'];
//         $sales = array("Jeff");

    
    
    }
    
//     $sales = array("Carlo", "Jeff");
    
} else {
    
    $sales = array("No_Sales");
    
}
    
//    $sales = array("Vaughan", "Claudio", "Janine", "Jeff", "Elio");
    
    foreach($sales as $item) {
    
          $results_query = "SELECT app_id FROM appointments WHERE (app_time='$app_time') AND (app_date='$app_date') AND (app_consultant='$item') AND (app_rescheduled = 'No' ) AND (app_approved = '$app_approved' OR app_approved = '$pending')";

            $row        = mysqli_query($connection, $results_query);
            $num_rows   = mysqli_num_rows($row);
            $y          = 0;
            for($x=0; $x<3; $x++){


    
          if(($num_rows == 0) AND ($x === $y)) {
   
         $query = "SELECT * FROM users WHERE user_firstname = '$item'";
         $select_users = mysqli_query($connection,$query);
              
            while($row = mysqli_fetch_assoc($select_users)) {

                $user_id            = $row['user_id'];
                $user_team          = $row['user_team'];
                $app_consultant     = $row['user_firstname'];
                $app_telesales      = 'Client';
                $jellisID           = $_GET['id'];
                $approved           = 'approved';

        $query = "INSERT INTO appointments(app_business, app_address, app_area, app_country, app_name, app_number, app_alt_number, app_website, app_email, app_comments, app_competitors, app_consultant, app_time,app_date, app_telesales, app_group, app_team,app_type,lead_id, app_approved)";

        $query .= "VALUES('{$app_business}', '{$app_address}', '{$app_area}', '{$app_country}', '{$app_name}', '{$app_number}','{$app_alt_number}','{$app_website}','{$app_email}','{$app_comments}','{$app_competitors}','{$app_consultant}','{$app_time}','{$app_date}','{$app_telesales}','{$app_group}','{$user_team}','{$type}','{$jellisID}','{$approved}')";
                
        $query3 = "UPDATE jellisLeads SET clicked = 'yes' WHERE id = $the_id";

        $create_user_query      = mysqli_query($connection, $query);
        $update_jellis_booked   = mysqli_query($connection, $query3);

        confirm($create_user_query );
        confirm($update_jellis_booked);

       echo "<script> location.replace('appointment_booked.php'); </script>";
                
  
          }  // END WHILE
              

  	} // END IF
  
 
    }        if(($num_rows == 0)) {
           break; 
        }   
            }// END FOREACH
        
        
        if(($num_rows > 0)) { 
          
          
          $query = "SELECT * FROM users WHERE user_firstname = 'Jeff'";
         $select_users = mysqli_query($connection,$query);
            while($row = mysqli_fetch_assoc($select_users)) {
                
                $user_id            = $row['user_id'];
                $user_team          = $row['user_team'];
                $app_consultant     = $row['user_firstname'];
                $app_telesales      = 'Client';
                $lead_id            = $_GET['id'];
             

        $query = "INSERT INTO appointments(app_business, app_address, app_area, app_country, app_name, app_number, app_alt_number, app_website, app_email, app_comments, app_competitors, app_consultant, app_time,app_date, app_telesales, app_group, app_team,app_type,lead_id)";

        $query .= "VALUES('{$app_business}', '{$app_address}', '{$app_area}', '{$app_country}', '{$app_name}', '{$app_number}','{$app_alt_number}','{$app_website}','{$app_email}','{$app_comments}','{$app_competitors}','{$app_consultant}','{$app_time}','{$app_date}','{$app_telesales}','{$app_group}','{$user_team}','{$type}','{$lead_id}')";

        $create_user_query = mysqli_query($connection, $query);

        confirm($create_user_query );

//        $timeError = "APPOINTMENT BOOKED: " . " " . "<a href='appointments.php'>VIEW APPOINTMENTS</a>";
                
                echo "<script> location.replace('appointment_booked.php'); </script>";
          
          
          }
              

   

            } // END IF
     

}


        echo $timeError;
?>

        <script>

        $(document).ready(function(){

            $('#myForm input, select').blur(function(){

                if(!$(this).val()){

                    $(this).addClass("error");

                } else{

                    $(this).removeClass("error");

                }

            });

        });

        </script>

  <div class="icons">
    <img src="images/icons-02.png"> <img src="images/icons-03.png"> <img src="images/icons-04.png"> <img src="images/icons-05.png"> <img src="images/icons-06.png">
</div>


    <form id="myForm" action="" method="post" enctype="multipart/form-data">    

     <div class="form-group">

         <label>Name of Business</label>

         <input type="text" class="form-control" name="app_business" value="<?php echo $company; ?>" required>

         <span class="error"></span>

     </div>

        <div class="form-group">

         <label for="title">Business Address</label>

         <input type="text" class="form-control" name="app_address" value="<?php echo $address; ?>" required>

     </div>        

       <div class="form-group">

         <label for="post_status">Area</label>
           
           <select class="form-control" name="app_area" required>
          <option value="JHB">JHB</option>
          <option value="CT">CT</option>
          <option value="PTA">PTA</option>
          <option value="UK">UK</option>
           </select>
     </div> 

                     <div class="form-group">
         <label for="post_status">Country</label>
         
         <select id="country" name="country" class="form-control" required>
      <option >Select Country</option>
        <option value="RSA" <?php if($country == "RSA"){ echo "selected"; } ?>>South Africa</option>
        <option value="UK" <?php if($country == "UK"){ echo "selected"; } ?>>UK</option>

  </select>
     </div> 

       <div class="form-group">
         <label for="post_status">Contact Full Name</label>
           <input type="text" class="form-control" name="app_name"  value="<?php echo $name." ".$surname; ?>" required>
     </div> 
     <div class="form-group">

         <label for="post_status">Contact Number</label>
           <input type="text" class="form-control" name="app_number"  value="<?php echo $contactNumber; ?>" required>
     </div>      
    <div class="form-group">

         <label for="post_status">Alternate Number</label>
           <input type="text" class="form-control" name="app_alt_number" >
     </div> 

         <div class="form-group">
         <label for="post_status">Business Website</label>
           <input type="text" class="form-control" name="app_website" value="<?php echo $url; ?>" >
     </div>          

        

        <div class="form-group">
         <label for="post_status">Comments</label>
           <input type="text" class="form-control" name="app_comments" >

     </div> 

       <div class="form-group">
         <label for="post_status">Email</label>
           <input type="email" class="form-control" name="app_email" value="<?php echo $email; ?>" >
     </div> 

            <div class="form-group">
         <label for="post_status">Business Competitor</label>
           <input type="text" class="form-control" name="app_competitors" >

     </div> 

        <div class="form-group">
         <label for="post_status">Meeting Time</label>
           <select type="text" class="form-control" name="app_time" d required>
               <option value='9:00'>9:00</option>
               <option value='10:15'>10:15</option>
               <option value='11:30'>11:30</option>
               <option value='12:45'>12:45</option>
               <option value='14:00'>14:00</option>
               <option value='15:30'>15:30</option>
            </select>
     </div> 

      <div class="form-group">
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script> 
<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
<script>
webshims.setOptions('forms-ext', {types: 'date'});
webshims.polyfill('forms forms-ext');
$.webshims.formcfg = {
en: {
dFormat: '-',
dateSigns: '-',
patterns: {
d: "yy-mm-dd"
}
}
};
</script>
         <label for="date">Meeting Date</label>

           <input type="date"  class="form-control" name="app_date" placeholder="yyyy/mm/dd"  required>
     <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
<script>
    webshims.setOptions('forms-ext', {types: 'date'});
webshims.polyfill('forms forms-ext');
</script>
<!--<input type="date" />-->
     </div>


<div class="form-group">

    <input class="btn btn-primary" type="submit" name="add_appointment" value="Add Appointment">

     </div>

</form> 

    </div>

</div>

<script type="text/javascript">

$(document).ready(function() {

    $('select').material_select();

});

</script>
                
        </div>        
                </div>
      </div>
      </div>    </div>

</div><!-- loginwrap -->