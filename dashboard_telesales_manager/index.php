<?php include "../includes/dashboard_header.php" ?>

    <div id="wrapper">
        
<script>
    sendNotification({
  title: 'New Notification',
  message: 'Your message goes here',
  icon:'https://cdn2.iconfinder.com/data/icons/mixed-rounded-flat-icon/512/megaphone-64.png',
  clickCallback: function () {
    alert('do something when clicked on notification');
  }
});
   </script>
        <?php include "includes/manager_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
<div class="row">
<div class="col-lg-12">
    <h1 class="page-header">
    TELESALES MANAGER DASHBOARD
    <small><?php echo $_SESSION['user_firstname'] ?></small>
    </h1>
</div>
               <div class="col-lg-12">
         
                   
  <form method="post">
   
   <div class="row">

  <div class="col-sm-4"><div class="form-group">
         <label for="post_status">From Date</label>
           <input type="date" class="form-control" name="from_date">
     </div></div>
  <div class="col-sm-4"> <div class="form-group">
         <label for="post_status">To Date</label>
           <input type="date" class="form-control" name="to_date">
     </div></div>
</div>

        <input type="submit" value="search" name="filter" class="button btn"> 
</form> 
   <?php

?>

     <div class="row">
    <div class="col-sm-6">
        <?php include "../views/manager/chart_manager_apps.php" ?>
    </div>
    <div class="col-sm-6">
        <?php include "../views/manager/chart_manager_status.php" ?>
    </div>
      </div>
      
     
      
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
  <?php include "includes/manager_footer.php" ?>