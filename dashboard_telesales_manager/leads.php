<?php include "../includes/dashboard_header.php" ?>
    <div id="wrapper">

        <?php include "includes/manager_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                   <h1 class="page-header">
                           JELLIS LEAD SYSTEM
                            
                        </h1>
    <?php
    
        if(isset($_GET['source'])) {
            
            $source = $_GET['source'];
            
            
        } else {
            
            $source = '';
        }

switch($source) {
        case 'my_leads';
        include "../views/global/view_my_leads.php";
        break ;
            
        case 'add_feedback';
        include "../views/global/add_lead_feedback.php";
        break ; 
        
        case 'single_lead_feedback';
        include "../views/global/single_lead_feedback.php";
        break ;
        
                      
        case 'jellis_leads';
        include "../views/global/jellis_leads.php";
        break ; 
        
        case 'view_feedback';
        include "../views/global/lead_feedback.php";
        break ;
        
        case 'view_clicked_leads';
        include "../views/global/jellis_clicked_leads.php";
        break ;   
        
        case 'view_actioned_leads';
        include "../views/global/view_my_actioned_leads.php";
        break ; 
        
        case 'edit_lead';
        include "../views/global/edit_lead_details.php";
        break ;
        
        default: 
        include "../views/global/view_leads.php";
        break;
}
    
    ?>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        
        <!-- /#page-wrapper -->

  <?php include "includes/manager_footer.php" ?>   