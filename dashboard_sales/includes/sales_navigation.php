<!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <li class="useravatarmobi">
                        <div class="useravatar">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">RCM SALES DASHBOARD</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               <li><a href="index.php">HOME</a></li>
                
                   
                   
                   
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php 
                        
                        if(isset($_SESSION['username'])) {
                        echo $_SESSION['username']; 
                        }

                        ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                      
                        <li class="divider"></li>
                        <li>
                            <a href="../includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <div class="useravatar useravatardrop">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
                    <li>
                        <a href="index.php"><i class="fas fa-globe"></i> Dashboard</a>
                    </li>
                  <li>
                        <a href="calendar.php"><i class="fas fa-calendar-alt"></i> Calendar</a>
                    </li>
                 
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-calendar"></i> Appointments <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="demo" class="collapse">
                                            <li>
                                <a href="appointments.php?source=add_appointment">Add Appointment</a>
                            </li>
                                                       <li>
                                <a href="appointments.php?source=view_appointments">View All Appointments</a>
                            </li>
                           
                          
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#leads"><i class="fa fa-calendar"></i> Leads <i class="fa fa-fw fa-caret-down"></i></a>
                       
                            <ul id="leads" class="collapse">
<!--
                            <li>
                                <a href="leads.php?source=view_leads">VIEW LEADS</a>
                            </li> 
--> 
                               <li class="<?= ($activePage == 'jellis_leads') ? 'active':''; ?>">
                                <a href="leads.php?source=jellis_leads">JELLIS</a>
                            </li>
                                     <li class="<?= ($activePage == 'view_clicked_leads') ? 'active':''; ?>">
                                <a href="leads.php?source=view_clicked_leads">CLICKED LEADS</a>
                            </li>     
                                 <li class="<?= ($activePage == 'my_leads') ? 'active':''; ?>">
                                    <a href="leads.php?source=my_leads">MY LEADS</a>
                                </li>        
                                   
                                   <li class="<?= ($activePage == 'view_actioned_leads') ? 'active':''; ?>">
                                    <a href="leads.php?source=view_actioned_leads">ACTIONED LEADS</a>
                                </li>        
                                                         
                                
                                <li class="<?= ($activePage == 'view_feedback') ? 'active':''; ?>">
                                    <a href="leads.php?source=view_feedback">FEEDBACK</a>
                                </li> 
                       
                           
                          
                        </ul>
                    </li>
                    
                            <li>
                                <a href="appointments.php?source=view_feedback"><i class="fa fa-comment"></i> View Feedback</a>
                            </li>
                             <li>
                                <a href="appointments.php?source=view_stats"><i class="fa fa-comment"></i> Stats</a>
                            </li>
                      <li>
                                <a href="pipeline.php?source=view_pipeline"><i class="fa fa-comment"></i> Pipeline</a>
                            </li>
                               <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#boardroom"><i class="fa fa-calendar"></i> Boardroom <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="boardroom" class="collapse">
                            <li>
                                <a href="boardroom.php?source=book_boardroom">BOOK BOARDROOM</a>
                            </li> 
                               <li>
                                <a href="boardroom.php?source=view_boardroom">VIEW BOARDROOM</a>
                            </li> <li>
                                <a href="boardroom.php?source=my_boardroom">MY BOARDROOM</a>
                            </li>
                           
                          
                        </ul>
                    </li>
                              <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#boardroom2"><i class="fa fa-calendar"></i>Sales Boardroom <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="boardroom2" class="collapse">
                            <li>
                                <a href="sales_boardroom.php?source=book_boardroom">BOOK BOARDROOM</a>
                            </li> 
                               <li>
                                <a href="sales_boardroom.php?source=view_boardroom">VIEW BOARDROOM</a>
                            </li> <li>
                                <a href="sales_boardroom.php?source=my_boardroom">MY BOARDROOM</a>
                            </li>
                           
                          
                        </ul>
                    </li>
                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#van"><i class="fa fa-calendar"></i> Book Transport <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="van" class="collapse">
                            <li>
                                <a href="book_van.php?source=book_van">BOOK VAN</a>
                            </li> 
                               <li>
                                <a href="book_van.php?source=view_van">VAN AVAILABILITY</a>
                            </li> 
                           
                               
                               <li>
                                <a href="book_van.php?source=my_van">YOUR BOOKINGS</a>
                            </li> 
                        </ul>
                    </li>
                     <li >
                        <a href="profile.php"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>