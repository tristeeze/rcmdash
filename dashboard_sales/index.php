<?php include "includes/sales_header.php" ?>

    <div id="wrapper">

        <?php include "includes/sales_navigation.php" ?>

        <div id="page-wrapper">
 <div class="container-fluid">

 <!-- Page Heading -->

<div class="row">

<div class="col-lg-12">

    <h1 class="page-header">

    SALES DASHBOARD

    <small><?php echo $_SESSION['user_firstname'] ?></small>
    <small><?php echo $_SESSION['user_team'] ?></small>
    <small><?php echo $_SESSION['user_group'] ?></small>

    </h1>

     <?php
 

    $current_month = date("m");

       $sql="SELECT sum(feedback_sale_value) as total FROM feedback WHERE MONTH(feedback_date) = $current_month AND feedback_sales = '{$_SESSION['user_firstname']}'";

    $result = mysqli_query($connection,$sql);

    $row = mysqli_fetch_assoc($result);   

    ?> 
   <a class="navbar-brand" href="index.php">YOUR MONTHLY CLOSED/PENDING REVENUE <?php echo "<strong>R {$row['total']}</strong>"; ?></a>

<script type='text/javascript'>

$(function(){

var overlay = $('<div id="overlay"></div>');

overlay.show();

overlay.appendTo(document.body);

$('.popup').show();

$('.close').click(function(){

$('.popup').hide();

overlay.appendTo(document.body).remove();

return false;

});



$('.x').click(function(){

$('.popup').hide();

overlay.appendTo(document.body).remove();

return false;

});

});

</script>

                   <?php
   

$query = "SELECT * FROM appointments WHERE app_consultant = '{$_SESSION['user_firstname']}' AND app_feedback = 'none' AND app_approved = 'approved' AND app_rescheduled = 'No'";

$select_appointments = mysqli_query($connection,$query);

while($row = mysqli_fetch_assoc($select_appointments)) {

date_default_timezone_set('Africa/Johannesburg');

$app_id = $row['app_id'];

$date_now = date("Y-m-d");

$app_date = $row['app_date'];    

    

    if($date_now > $app_date) {

echo "<div class='popup'>";

echo "<div class='cnt223'>";

echo "<h1>Update Feedback</h1>";

echo "<p>";

echo "You need to update your previous appointment feedback";

echo "<br/>";

echo "<a href='appointments.php?source=add_feedback&p_id={$app_id}' >Update Feedback</a>";

echo "</p>";

echo "</div>";

echo "</div>";

} 

}

?>

                    </div>

                    <div class="col-lg-12">

    <div class="row">

                 <div class="col-sm-4">

      <div class="panel panel-purple">

            <div class="panel-heading">


                <div class="row">

                    <div class="col-xs-3">
<i class="fas fa-coins fa-5x" ></i>
                       

                    </div>
                 

                    <?php

      $sql="SELECT sum(feedback_sale_value) as total FROM feedback WHERE feedback_sales = '{$_SESSION['user_firstname']}' AND MONTH(feedback_date) = $current_month ";

    $result = mysqli_query($connection,$sql);

    $row = mysqli_fetch_assoc($result); 

    $the_total =  $row['total'];

 echo "<div class='huge'>R" .$the_total . "</div>";
 ?>

               <div><h4>Current Revenue</h4></div>               

               </div>
            </div>

        </div>

  </div>

  <div class="col-sm-4">  

           <div class="panel panel-blue">

            <div class="panel-heading">
               <div class="row">

                    <div class="col-xs-3">

                       <i class="far fa-calendar-plus fa-5x" ></i>

                    </div>

                    <?php

        $query = "SELECT * FROM appointments WHERE app_consultant = '{$_SESSION['user_firstname']}' AND MONTH(app_date) = $current_month ";

    $select_all_users = mysqli_query($connection, $query);

    $app_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$app_count}</div>";

    ?>

                        <div><h4> Appointments</h4></div>

                </div>

            </div>

            

        </div></div>

        

  <div class="col-sm-4">

      <div class="panel panel-orange">

            <div class="panel-heading">
            



                <div class="row">

                    <div class="col-xs-3">

                        <i class="fas fa-calendar-check fa-5x"></i>

                    </div>

                  

                    <?php

    

    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'processed' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $user_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$user_count}</div>";



    ?>

                        <div><h4>Processed Deals</h4></div>

                </div>

            </div>

           
        </div>



  </div>



</div>

           <div class="row">

               

  <div class="col-sm-4"> 

           

           <div class="panel panel-blue">

            <div class="panel-heading">

            <div class="row">

                    <div class="col-xs-3">

                        <i class="far fa-calendar-alt fa-5x"></i>

                    </div>

                  

                    <?php

    

    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'closed' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $app_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$app_count}</div>";

    

    

    ?>

                        <div><h4>Closed</h4></div>

                  

                </div>

            </div>

           

        </div></div>

        

  <div class="col-sm-4">

      <div class="panel panel-orange">

            <div class="panel-heading">

               



                <div class="row">

                    <div class="col-xs-3">

                        <i class="far fa-calendar-minus fa-5x"></i>

                    </div>

                  

                    <?php

    

    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'pending' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $user_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$user_count}</div>";

    

    

    ?>

                        <div><h4>Pending</h4></div>

                  

                </div>

            </div>

          

        </div>

      

      

  </div>      

     
  <div class="col-sm-4">

      

      <div class="panel panel-purple">

            <div class="panel-heading">

               



                <div class="row">

                    <div class="col-xs-3">

                       <i class="fas fa-calendar-times fa-5x"></i>

                    </div>

                  

                    <?php

    

    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'rescheduled' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $user_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$user_count}</div>";

    

    

    ?>

                        <div><h4>Rescheduled</h4></div>
 </div>
            </div>

        </div>

  </div>



</div>
 <div class="row">

               

  <div class="col-sm-4"> 

           

           <div class="panel panel-orange">

            <div class="panel-heading">

            <div class="row">

                    <div class="col-xs-3">

                        <i class="far fa-calendar-alt fa-5x"></i>

                    </div>

                  

                    <?php

    

    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'cancelled' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $app_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$app_count}</div>";

    

    

    ?>

                        <div><h4>Cancelled</h4></div>

                  

                </div>

            </div>

           

        </div></div>

        

  <div class="col-sm-4">

      <div class="panel panel-purple">

            <div class="panel-heading">

               



                <div class="row">

                    <div class="col-xs-3">

                        <i class="far fa-calendar-minus fa-5x"></i>

                    </div>

                  

                    <?php

    

    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'no show' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $user_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$user_count}</div>";

    

    

    ?>

                        <div><h4>No Show</h4></div>

                  

                </div>

            </div>

          

        </div>

      

      

  </div>      

     
  <div class="col-sm-4">

      

      <div class="panel panel-blue">

            <div class="panel-heading">

               



                <div class="row">

                    <div class="col-xs-3">

                       <i class="fas fa-calendar-times fa-5x"></i>

                    </div>

                  

                    <?php

    

    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'no budget' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $user_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$user_count}</div>";

    

    

    ?>

                        <div><h4>No Budget</h4></div>
 </div>
            </div>

        </div>

  </div>



</div> <!--end-->
<div class="row">

               

  <div class="col-sm-4"> 

           

           <div class="panel panel-purple">

            <div class="panel-heading">

            <div class="row">

                    <div class="col-xs-3">

                        <i class="far fa-calendar-alt fa-5x"></i>

                    </div>

                  

                    <?php

    

    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'no close' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $app_count = mysqli_num_rows($select_all_users);



    echo "<div class='huge'>{$app_count}</div>";
 

    

    ?>

                        <div><h4>No Close</h4></div>
              

                </div>

            </div>

              </div></div>
       

  <div class="col-sm-4">

      <div class="panel panel-blue">

            <div class="panel-heading">

  <div class="row">

                    <div class="col-xs-3">

                        <i class="far fa-calendar-minus fa-5x"></i>

                    </div>
                  
                  <?php

    
    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'not interested' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $user_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$user_count}</div>";

   

    ?>

                        <div><h4>Not Interested</h4></div>


                </div>

            </div>

        </div>


  </div>      

     
  <div class="col-sm-4">

      

      <div class="panel panel-orange">

            <div class="panel-heading">

                  <div class="row">

                    <div class="col-xs-3">

                       <i class="fas fa-calendar-times fa-5x"></i>

                    </div>

                  

                    <?php

    
    $query = "SELECT * FROM feedback WHERE  feedback_sales = '{$_SESSION['user_firstname']}' AND feedback_status = 'closed' AND MONTH(feedback_date) = $current_month";

    $select_all_users = mysqli_query($connection, $query);

    $user_count = mysqli_num_rows($select_all_users);

    echo "<div class='huge'>{$user_count}</div>";

    ?>

                        <div><h4>Closed</h4></div>
 </div>
            </div>

        </div>

  </div>

</div>
      

                    </div>

                </div>

            </div>

            <!-- /.container-fluid -->

        </div>

        <!-- /#page-wrapper -->

  <?php include "includes/sales_footer.php" ?>