<?php

//required_once("config.php");

class Database {
    private $host           = 'localhost';
    private $username       = 'root';
    private $password       = '';
	private $db             = 'rcmdash';
   
    public $connection;
    
    function __construct(){
        
        $this->open_db_connection();
        
    }
    
    function open_db_connection(){
        
       $this->connection = new mysqli(DB_HOST2, DB_USER2, DB_PASSWORD2, DB_NAME2);
//       $this->connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
//    $this->connection = new mysqli($this->host, $this->username, $this->password, $this->db); 

        
        if($this->connection->connect_errno) {
            die("Database connection failed" . $this->connection->connect_error);
        }
    }
    
    
    public function query($sql) {
        
        
        $result = $this->connection->query($sql);
        
        $this->confirm_query($result);
        
        return $result;
    }
    
    
    private function confirm_query($result){
        
           if(!$result) {
                
               die("QUERY FAILED" . $this->connection->error);
        }
        
    }
    
    
    public function escape_string($string) {
        
       $escaped_string = $this->connection->real_escape_string($string);
        return $escaped_string;
        
    }
    
    public function the_insert_id() {
        
        return mysqli_insert_id($this->connection);
        
    }
    
//    public function the_insert_id() {
//        return $this->connection->insert_id;
//    }
    
}

$database = new Database();


?>