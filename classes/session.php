<?php

class Session {
    
    private $signed_in      = false;
    private $user_active    = false;
    private $superadmin     = false;
    public $id;
    // public $message;
        
    function __construct() {
    session_start();

    $this->check_the_login();
    // $this->check_message();
                
    }
    
    public function is_signed_in(){
        
        return $this->signed_in;
        return $this->user_active;
        
    }
    
    public function login($user) {
        
        if($user) {
            
            $this->id = $_SESSION['id'] = $user->id; 
            $this->user_status = $_SESSION['user_status'] = $user->user_status; 
            
            $this->signed_in = true;
        }

    } // LOGIN

    public function user_role($user) {
        
        if($user) {
            
            $this->user_role = $_SESSION['user_role'] = $user->user_role; 
            $this->superadmin = true;

        }

    } // USER_ROLE
    
    public function logout(){ 
        unset($_SESSION['id']);
        // unset($user->id);
        $this->signed_in = false;

    }

    private function check_the_login() {
    
    if(isset($_SESSION['id'])) {

        $this->id = $_SESSION['id'];
        $this->signed_in = true;
    
    } else {
        
        unset($this->id);
        $this->signed_in = false;
    }
    
    
} // CHECK LOGIN

public function get_role($id){

    $sql3="SELECT user_role FROM users WHERE id = $id";
    $result = mysqli_query($this->db,$sql3);
    $user_data = mysqli_fetch_array($result);
   

            } // get role

    
} // END CLASS

$session = new Session();

?>