<!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">RCM HUMAN RESOURCES</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               <li><a href="../index.php">HOME</a></li>
                
                   
                   
                   
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php 
                        
                        if(isset($_SESSION['username'])) {
                        echo $_SESSION['username']; 
                        }
                        
                        
                        ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        
                      
                        <li class="divider"></li>
                        <li>
                            <a href="../includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <div class="useravatar">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
                    <li>
                        <a href="index.php"><i class="fas fa-globe"></i> Dashboard</a>
                    </li>
                                  
                                       
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#boardroom"><i class="fa fa-calendar"></i>Sales Boardroom <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="boardroom" class="collapse">
                            <li>
                                <a href="sales_boardroom.php?source=book_boardroom">BOOK BOARDROOM</a>
                            </li> 
                               <li>
                                <a href="sales_boardroom.php?source=view_boardroom">VIEW BOARDROOM</a>
                            </li> 
                          
                        </ul>
                    </li>
                    
                   
                            <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#van"><i class="fa fa-calendar"></i> Book Transport <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="van" class="collapse">
                            <li>
                                <a href="book_van.php?source=book_van">BOOK VAN</a>
                            </li> 
                               <li>
                                <a href="book_van.php?source=view_van">VAN AVAILABILITY</a>
                            </li> 
                           
                               
                               <li>
                                <a href="book_van.php?source=my_van">YOUR BOOKINGS</a>
                            </li> 
                        </ul>
                    </li>
                     <li >
                        <a href="profile.php"><i class="fa fa-fw fa-file"></i> Profile</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>