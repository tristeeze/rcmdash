-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 02, 2019 at 09:01 AM
-- Server version: 5.7.27
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rightcli_salesapp2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `app_id` int(255) NOT NULL,
  `app_approved` varchar(255) NOT NULL DEFAULT 'pending',
  `app_business` varchar(255) NOT NULL,
  `app_address` varchar(255) NOT NULL,
  `app_area` varchar(255) NOT NULL,
  `app_name` varchar(255) NOT NULL,
  `app_number` varchar(255) NOT NULL,
  `app_alt_number` varchar(255) NOT NULL,
  `app_website` varchar(255) NOT NULL,
  `app_email` varchar(255) NOT NULL,
  `app_comments` varchar(255) NOT NULL,
  `app_competitors` varchar(255) NOT NULL,
  `app_consultant` varchar(255) NOT NULL,
  `app_group` varchar(255) NOT NULL DEFAULT 'no group',
  `app_team` varchar(255) NOT NULL DEFAULT 'no team',
  `app_time` time NOT NULL,
  `app_date` date DEFAULT NULL,
  `app_telesales` varchar(255) NOT NULL,
  `app_feedback` varchar(255) DEFAULT 'none',
  `app_presentation` varchar(255) DEFAULT NULL,
  `app_partners` varchar(255) DEFAULT NULL,
  `app_decision_makers` varchar(255) DEFAULT NULL,
  `app_sale` varchar(255) DEFAULT NULL,
  `app_sale_value` varchar(255) DEFAULT NULL,
  `app_processed` varchar(255) NOT NULL DEFAULT 'not processed',
  `app_status` varchar(255) DEFAULT NULL,
  `app_rescheduled` varchar(255) DEFAULT 'No',
  `app_old_date` text,
  `app_old_id` varchar(255) NOT NULL DEFAULT '0',
  `app_who_rescheduled` varchar(255) NOT NULL DEFAULT 'not applicable',
  `app_cancelled_comment` varchar(255) DEFAULT NULL,
  `app_pend_chance` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`app_id`, `app_approved`, `app_business`, `app_address`, `app_area`, `app_name`, `app_number`, `app_alt_number`, `app_website`, `app_email`, `app_comments`, `app_competitors`, `app_consultant`, `app_group`, `app_team`, `app_time`, `app_date`, `app_telesales`, `app_feedback`, `app_presentation`, `app_partners`, `app_decision_makers`, `app_sale`, `app_sale_value`, `app_processed`, `app_status`, `app_rescheduled`, `app_old_date`, `app_old_id`, `app_who_rescheduled`, `app_cancelled_comment`, `app_pend_chance`) VALUES
(307, 'unapproved', 'Coffee', 'alkslodfj', 'sldkjflk', 'Junior', '0358888888', '0119966555', 'coffee.co.za', 'info@coffee.co.za', 'coffee is good', 'tea', 'Elio', '', '', '11:30:00', '2019-07-11', 'Sameera', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', 'CANCELLED', 'No', NULL, '0', 'not applicable', NULL, NULL),
(308, 'unapproved', 'Woolworths2', 'alkslodfj', 'sldkjflk', 'skfdjlskdj', '01144695555', '0125556645', 'high5.co.za', 'info@burgerking.co.za', 'asdasdasd', 'Nespresso', 'Elio', '', '', '09:00:00', '2019-06-18', 'Sameera', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', 'CANCELLED', 'No', NULL, '0', 'not applicable', NULL, NULL),
(306, 'approved', 'Macdonalds', 'Hennie Alberts', 'Meyersdal', 'Vaughan', '0115545555', '0115555555', 'asdasd.co.za', 'info@macdonalds.co.za', 'marketing', 'Steers', 'Vaughan', '', '', '09:00:00', '2019-08-31', 'Sameera', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', 'PENDING', 'No', NULL, '0', 'not applicable', NULL, NULL),
(305, 'approved', 'Miniso', 'Centurion', 'Centurion', 'Alanna', '0145555555', '0125556645', 'www.miniso.com', 'info@miniso.co.za', 'needs a website', 'Crazy Store', 'Dean', '', '', '14:00:00', '2019-08-28', 'Ryan', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', 'NO BUDGET', 'No', NULL, '0', 'not applicable', NULL, NULL),
(304, 'approved', 'Revite', 'Hennie Alberts', 'Alberton', 'Kyle', '0115554644', '0119966555', 'www.revite.co.za', 'info@revite.co.za', 'client wants seo', 'vital', 'Kylie', '', '', '09:00:00', '2019-06-12', 'Ryan', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', 'NO BUDGET', 'No', NULL, '0', 'not applicable', NULL, NULL),
(303, 'approved', 'Bianchi', 'Sandton', 'Sandton', 'Mr Bike', '0115554444', '0115555555', 'coffee.co.za', 'info@giant.com', 'sdfsdf', 'Nespresso', 'Jeff', '', '', '09:00:00', '2019-08-30', 'Jeff', 'deal pending', 'YES', 'YES', '', 'NO', '50000', 'not processed', 'PROCESSED', 'No', NULL, '0', 'not applicable', '', 'A'),
(302, 'approved', 'Ahk Motor Spares', 'Hennie Alberts', 'Meyersdal', 'Bossman', '0114477888', '0125556645', 'coffee.co.za', 'info@burgerking.co.za', 'client wants a proposal', 'Spar', 'Janine', '', '', '09:00:00', '2019-02-18', 'Ryan', 'Website and Marketing', 'YES', 'YES', '', 'YES', '42000', 'not processed', 'PENDING', 'No', NULL, '0', 'not applicable', '', 'Not Applicable'),
(301, 'approved', 'Bobo Campers', 'Hennie Alberts', 'Meyersdal', 'Bossman', '0119801144', '0125556645', 'high5.co.za', 'info@macdonalds.co.za', 'asd', '32gi', 'Janine', '', '', '09:00:00', '2019-02-14', 'Ryan', 'Deal Closed', 'YES', 'YES', '', 'YES', '27000', 'not processed', 'PROCESSED', 'No', NULL, '0', 'not applicable', '', 'Not Applicable'),
(298, 'approved', ' Man With A Van', 'SKYPE - Hive_Five', '', 'Steven', '011 875 2365', '0716761219', 'www.manwithavan.co.za', 'stevendebecker@gmail.com', '', '', 'Claudio', '', '', '09:00:00', '2019-07-16', 'Ryan', 'closing', 'YES', 'YES', '', 'YES', '450000', 'not processed', 'CLOSED', 'No', NULL, '0', 'not applicable', '', 'Not Applicable'),
(296, 'approved', ' Lyfes Tile n Decor', '12 Sunninghill Office Park 43 Peltier Drive ', 'Sunninghill', 'Khuti Mokoena', '0105921800', '0728001546', 'www.lyfestilesdecor.com', '', '', '', 'Dean', '', '', '14:00:00', '2019-08-02', 'Sameera', NULL, NULL, NULL, NULL, NULL, NULL, 'not processed', 'CLOSED', 'No', NULL, '0', 'not applicable', NULL, NULL),
(294, 'approved', 'The Gift & Decor Company', '1239 Anvil Road', 'Robertville', 'Apostolos Vranas', '011 474 6820', '0829206286', 'giftdecor.co.za', 'apostolos@giftdecor.co.za', '', '', 'Claudio', '', '', '11:30:00', '2019-08-02', 'Michael ', NULL, NULL, NULL, NULL, NULL, NULL, 'not processed', 'CLOSED', 'No', NULL, '0', 'not applicable', NULL, NULL),
(295, 'approved', 'Control Installations & Repair Centre', 'Unit 23, Industrial Village 22 Elsecar Street ', 'KAYA SAND', 'Freddie Van Staden', '011 708 4177', '', 'http://www.circleidlers.co.za', 'circle1@webmail.co.za', '', '', 'Claudio', '', '', '09:00:00', '2019-07-31', 'Michael ', '', 'YES', 'YES', '', 'YES', '65000', 'not processed', 'PROCESSED', 'No', NULL, '0', 'not applicable', '', 'Not Applicable'),
(297, 'approved', ' Express Truck Spares', '35 Osborne Road, ', 'WADEVILLE', 'Troshal Ramadu', '0118240996', '', 'www.express-works.co.za', 'meganramadu@gmail.com', '', '', 'Kylie', '', '', '09:00:00', '2019-08-01', 'Patricia ', 'appointment rescheduled', 'NO', 'NO', 'not applicable', 'NO', '0', 'not processed', 'CLOSED', 'No', NULL, '0', 'not applicable', '', 'Not Applicable'),
(300, 'approved', 'Paint doctor', 'Hennie Alberts', 'Meyersdal', 'Bossman', '0115555555', '0115555555', 'paintdr.co.za', 'info@paintdr.co.za', 'website and marketing', 'prominent paint', 'Claudio', '', '', '09:00:00', '2019-07-05', 'Candice', 'Website and Marketing', 'NO', 'YES', '', 'YES', '42000', 'not processed', 'CLOSED', 'No', NULL, '0', 'not applicable', '', 'Not Applicable'),
(293, 'approved', ' Tarps for Africa', '98 First Road Farmall ', 'Randburg', 'Mark MD', '011 028 2100', '083 419 8424', 'www.tarpsforafrica.co.za', '', '', '', 'Claudio', '', '', '09:00:00', '2019-08-02', 'Sameera', NULL, NULL, NULL, NULL, NULL, NULL, 'not processed', 'CLOSED', 'No', NULL, '0', 'not applicable', NULL, NULL),
(299, 'approved', 'Hitchcock Michalski', '24A 8th Avenue', 'Meyersdal', 'Fiona', '+27 (0)11 482 1395', '', 'https://www.hitchcockmichalski.com', '', '', '', 'Elio', '', '', '11:30:00', '2019-06-17', 'Candice', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', 'PENDING', 'No', NULL, '0', 'not applicable', NULL, NULL),
(309, 'approved', 'Giant', 'Taiwan', 'taiwan', 'Bossman', '0115554774', '0125556645', 'giant.co.za', 'info@burgerking.co.za', 'asdasdasd', 'Steers', 'Jeff', 'no group', 'no team', '11:30:00', '2019-09-30', 'Jeff', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', NULL, 'No', NULL, '0', 'not applicable', NULL, NULL),
(310, 'approved', 'Troisport', 'Sunninghill', 'Sandton', 'skfdjlskdj', '0115565555', '0155555555', 'macdonalds.co.za', 'info@woollies.co.za', 'client wants a proposal', 'Spar', 'Jeff', 'superadmin', 'no team', '09:00:00', '2019-09-12', 'Jeff', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', NULL, 'No', NULL, '0', 'not applicable', NULL, NULL),
(311, 'unapproved', 'Ferrero', 'Walkerville', 'Walkerville', 'asdasd', '0115445555', '0119966555', 'bkq.co.za', 'info@burgerking.co.za', 'asdasdasd', 'Steers', 'Kylie', 'telesales', 'no team', '11:30:00', '2019-08-30', 'Candice', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', NULL, 'No', NULL, '0', 'not applicable', NULL, NULL),
(312, 'approved', 'Harry Potter', 'meyersdal', 'Sandton', 'skfdjlskdj', '0114448955', '0125556645', 'bkq.co.za', 'info@burgerking.co.za', 'asd', 'Steers', 'Elio', 'telesales', 'no team', '09:00:00', '2019-08-30', 'Candice', 'none', NULL, NULL, NULL, NULL, NULL, 'not processed', NULL, 'No', NULL, '0', 'not applicable', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `feedback_id` int(11) NOT NULL,
  `app_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `feedback_comment` varchar(255) NOT NULL,
  `feedback_presentation` varchar(255) NOT NULL,
  `feedback_partners` varchar(255) NOT NULL,
  `feedback_other_partners` varchar(255) NOT NULL,
  `feedback_sale` varchar(255) NOT NULL,
  `feedback_sale_value` varchar(255) NOT NULL,
  `feedback_status` varchar(255) NOT NULL,
  `feedback_pending` varchar(255) NOT NULL,
  `feedback_cancelled` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `processed_deals`
--

CREATE TABLE `processed_deals` (
  `process_id` int(11) NOT NULL,
  `process_app_id` varchar(255) NOT NULL,
  `process_business` varchar(255) NOT NULL,
  `process_consultant` varchar(255) NOT NULL,
  `process_value` varchar(255) NOT NULL,
  `process_comments` varchar(255) NOT NULL,
  `process_proof` varchar(255) NOT NULL DEFAULT 'none',
  `process_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `processed_deals`
--

INSERT INTO `processed_deals` (`process_id`, `process_app_id`, `process_business`, `process_consultant`, `process_value`, `process_comments`, `process_proof`, `process_date`) VALUES
(12, '295', 'Control Installations & Repair Centre', 'Claudio', '65000', 'good deal', 'none', '2019-08-01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_lastname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_number` varchar(255) NOT NULL,
  `user_group` varchar(255) DEFAULT 'telesalesr',
  `user_team` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL,
  `user_password` text NOT NULL,
  `user_colour` varchar(255) DEFAULT NULL,
  `user_payment` varchar(255) NOT NULL,
  `token` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_firstname`, `user_lastname`, `user_email`, `user_number`, `user_group`, `user_team`, `user_status`, `user_password`, `user_colour`, `user_payment`, `token`) VALUES
(1, 'Louise', 'Fourie', 'louise@rightclickmedia.co.za', '011 867 6380', 'admin', 'not applicable', 'active', 'design87', 'white', '', ''),
(5, 'Claudio', 'Crismann', 'claudio@rightclickmedia.co.za', '0721580142', 'Sales', 'Ironman', 'active', 'claudio@321###', 'Green', 'RCM Employee', ''),
(6, 'Ryan', 'Srnme', 'ryan@rightclickmedia.co.za', '0115555555', 'telesalesr', 'not applicable', 'active', '123', '', '', ''),
(7, 'Shimon', 'Jacobon', 'shimon@rightclickmedia.co.za', '0115555555', 'admin', 'not applicable', 'active', '123', 'white', '', '59736ac6315c25cab0f9153daa7b2b144365725c7a2a4394aff9d03872abc6eb91403d502f683ff0400b075b35b29b3e9ea3'),
(13, 'Jeff', 'Ellis', 'jeff.ellis@rightclickmedia.co.za', '0118465578', 'superadmin', 'Thor', 'active', '123', 'White', 'RCM Employee', ''),
(246, 'Janine', 'Barnard', 'janine@rightclickmedia.co.za', '0827730023', 'Sales', 'Thor', 'active', '1239', 'Purple', 'RCM Employee', ''),
(247, 'Dean', 'Geldenhuys', 'dean@rightclickmedia.co.za', '0844216035', 'Sales', 'Ironman', 'active', 'dean@321###', 'Red', 'RCM Employee', ''),
(248, 'Vaughan', 'Essen', 'vaughan@rightclickmedia.co.za', '0722844686', 'superadmin', 'Ironman', 'active', '123', 'Orange', 'RCM Employee', ''),
(249, 'Kylie', 'Purchase', 'kylie@rightclickmedia.co.za', '0716753086', 'superadmin', 'Ironman', 'active', '123', 'Pink', 'RCM Employee', ''),
(250, 'Elio', 'Crismann', 'elio@applord.co.za', '0724373229', 'Sales', 'Thor', 'active', 'elio@321###', 'Black', 'RCM Employee', ''),
(251, 'Sharmaine', 'Crismann', 'sharmaine@rightclickmedia.co.za', '0760607162', 'Sales', 'Wonderwoman', 'active', 'sharmaine@321###', 'Teal', 'RCM Employee', ''),
(252, 'Candice', 'Botha', 'candice@rightclickmedia.co.za', '0832661222', 'telesales', 'Wonderwoman', 'active', '123', 'Gold', 'RCM Employee', ''),
(253, 'Shimon', 'Jacobson', 'shimon.jacobson@gmail.com', 'tkmy', 'telesalesr', 'not applicable', NULL, 'bbf4c5fd6caf7a0d9f65ea984c391aab', NULL, 'Geopayment', ''),
(254, 'Tasmin', 'Jacobs', 'tasmin@rightclickmedia.co.za', '01155555555', 'manager', 'Wonderwoman', 'active', '123', 'white', 'RCM Employee', NULL),
(259, 'Shash', 'Test', 'shimon.jacobson@gmail.com', '0000000000', 'telesalesr', 'Not Applicable', 'active', '123', 'blue', 'Direct Bank Transfer', NULL),
(261, 'Shimon', 'Jacobson', 'shimon.jacobson@gmail.com', '0000000000', 'admin', 'Not Applicable', 'active', '123', '', 'RCM Employee', NULL),
(262, 'Sameera', 'Karrim', 'sameera@rightclickmedia.co.za', '011 867 6380', 'telesales', 'Wizard', 'active', '123', 'Gold', 'RCM Employee', NULL),
(263, 'Nikita ', 'Durieux', 'nikita@rightclickmedia.co.za', '011 867 6380', 'telesales', 'Wizard', 'active', '123', 'Gold', 'RCM Employee', NULL),
(264, 'Bianca ', 'Rosenberg', 'bianca@rightclickmedia.co.za', '011 867 6380', 'telesales', 'Wizard', 'active', '123', 'Gold', 'RCM Employee', NULL),
(265, 'Patricia ', 'Gazide', 'patricia@rightclickmedia.co.za', '011 867 6380', 'telesales', 'Wizard', 'active', '123', 'Gold', 'RCM Employee', NULL),
(266, 'Clayton ', 'Germanus', 'clayton@rightclickmedia.co.za', '011 867 6380', 'telesales', 'Wizard', 'active', '123', 'Gold', 'RCM Employee', NULL),
(267, 'Michael ', 'Donovan', 'michael@rightclickmedia.co.za', '011 867 6380', 'telesales', 'Wizard', 'active', '123', 'Gold', 'RCM Employee', NULL),
(268, 'Sharon', 'Ellis', 'sharon@rightclickmedia.co.za', '011 867 6380', 'accountant', 'Wizard', 'active', '123', 'White', 'RCM Employee', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`),
  ADD KEY `app_id` (`app_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `processed_deals`
--
ALTER TABLE `processed_deals`
  ADD PRIMARY KEY (`process_id`),
  ADD KEY `process_app_id` (`process_app_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `app_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `processed_deals`
--
ALTER TABLE `processed_deals`
  MODIFY `process_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
