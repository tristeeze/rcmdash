<?php

require './vendor/autoload.php';
$options = array(
    'cluster' => 'ap2',
    'useTLS' => true
  );

$pusher = new Pusher\Pusher('d35b2485a82b61377895', '8206b02ad694fc572d2a', '838451', $options);

if(isset($_POST['create_user'])) {

  if(!empty($_POST['user_firstname']) && !empty($_POST['user_lastname']) && !empty($_POST['user_email'])
      && !empty($_POST['user_number']) && !empty($_POST['user_payment']))
  {
    $folder_path = './user_documents/';
    $user_firstname = $_POST['user_firstname'];
    $user_lastname = $_POST['user_lastname'];
    $user_email = $_POST['user_email'];
    $user_number = $_POST['user_number'];
    $user_password = $_POST['user_password'];
    $user_payment = $_POST['user_payment'];
    $data['message'] = $user_firstname;
    $feedback_given = 'updated';
    $pusher->trigger('notifications', 'new_user', $user_firstname);
    $user_document = basename($_FILES['file']['name']);
    $user_document_temp = $folder_path . $user_document;

    move_uploaded_file($_FILES['file']['tmp_name'], $user_document_temp);  
      
    $user_check_query = "SELECT * FROM users WHERE username='$user_firstname' OR email='$user_email' LIMIT 1";

    if($user_email) {

    }

    if($user_firstname !=''&& $user_lastname !=''&& $user_email !=''&& $user_number !=''&& $user_password !=''&& $user_payment !='')
      {
        
         $query = "INSERT INTO users(user_firstname, user_lastname, user_email, user_number, user_password, user_pay_details, user_documents)";
        $query .= "VALUES('{$user_firstname}', '{$user_lastname}', '{$user_email}', '{$user_number}', '{$user_password}', '{$user_payment}', '{$user_document}')";

   $create_user_query = mysqli_query($connection, $query);
   confirm($create_user_query );

          $to      = $user_email; // Send email to our user
          $subject = 'Signup | RCM Sales'; // Give the email a subject
          $message = '

          Thanks for signing up!
          Your account has been created, your account will be activated and you will be notified.

          ------------------------
          Username: '.$user_firstname.'
          Password: '.$user_password.'
          ------------------------

          Thank You for signing up

          ';

          $header = 'From:admin@rightclickmedia.co.za' . "\r\n"; // Set from headers
          mail($to,$subject,$message,$header); // Send our email
          header("location:redirect.php");

      } else {
            $type_error = "User already exists.";
        }
      }


  

   }

if(count($_POST)>0) {
	/* Form Required Field Validation */
	foreach($_POST as $key=>$value) {
	if(empty($_POST[$key])) {
	$message = ucwords($key) . " field is required";
	$type = "error";
	break;
	}
	}
  /* firstname validation */
  if($_POST['user_firstname'] != $_POST['user_firstname']){
  $message = 'Firstname Required<br>';
  $type = "error";
  }

  /* lastname validation */
  if($_POST['user_lastname'] != $_POST['user_lastname']){
  $message = 'Lastname Required<br>';
  $type = "error";
  }

	/* Email Validation */
	if(!isset($message)) {
	if (!filter_var($_POST["user_email"], FILTER_VALIDATE_EMAIL)) {
	$message = "Invalid User Email";
	$type = "error";
	}
	}

	/* Validation to check if gender is selected */
	if(!isset($message)) {
	if(!isset($_POST["user_number"])) {
	$message = " Number field is required";
	$type = "error";
	}
	}

}

?>

<style>
.messagereq.error {
  background: red;
  padding: 10px;
  color: #fff;
  font-size: 18px;
}
.popuperror {
  position: absolute;
  width: 100%;
  height: 150px;
  text-align: center;
  font-size: 24px;
  color: #fff;
  background: rgba(178,0,0,0.8);
}
</style>

    <!-- Page Content -->
    <div class="container">
      <div class="form-gap"></div>
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-md-offset-4">
                  <div class="panel panel-default">
                      <div class="panel-body formstyle">
                        <div class="site-branding">

                            <img src="images/rcm-logo.png" width="100%" style="width:100% !important;" />

                        </div>
                          <div class="text-center">


                                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                                  <h2 class="text-center">Register</h2>
                                  <div class="panel-body">

    <form action="" method="post" enctype="multipart/form-data">

      <?php if(isset($message)) { ?>
      <div class="messagereq <?php echo $type; ?>"><?php echo $message; ?></div>
      <?php } ?>

      <?php if(isset($user_check)) {
        echo $type_error;
      } ?>

      <div class="form-group">
          <div class="input-group">
          <span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
         <input type="text" class="form-control" name="user_firstname" autocomplete="off" placeholder="First Name" value="<?php if(isset($_POST['user_firstname'])) echo $_POST['user_firstname']; ?>">
     </div>
   </div>

   <div class="form-group">
       <div class="input-group">
       <span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
         <input type="text" class="form-control" name="user_lastname" autocomplete="off" placeholder="Last Name" value="<?php if(isset($_POST['user_lastname'])) echo $_POST['user_lastname']; ?>">
     </div>
   </div>


   <div class="form-group">
       <div class="input-group">
       <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
           <input type="email" class="form-control" name="user_email" autocomplete="off" placeholder="Email" value="<?php if(isset($_POST['user_email'])) echo $_POST['user_email']; ?>">
     </div>
   </div>

   <div class="form-group">
       <div class="input-group">
       <span class="input-group-addon"><i class="glyphicon glyphicon-phone color-blue"></i></span>
           <input type="text" class="form-control" name="user_number" autocomplete="off" placeholder="Number" value="<?php if(isset($_POST['user_number'])) echo $_POST['user_number']; ?>">
     </div>
   </div>

   <div class="form-group">
       <div class="input-group">
       <span class="input-group-addon"><i class="glyphicon glyphicon-lock color-blue"></i></span>
           <input type="password" class="form-control" placeholder="Password" name="user_password">
     </div>
   </div>


      <div class="form-group">
    <label>Banking Details</label> 
          <textarea type="textarea" class="form-control"  name="user_payment" ></textarea>
   
    </div>
  <div class="form-group">
            <label>UPLOAD PROOF OF IDENTITY</label>
<input type="file" name="file">
    </div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" name="create_user" value="Register">
     </div>
</form>

</div>
</div>
</div>
</div>
</div>
</div>