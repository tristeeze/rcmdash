<!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p align="center" style="font-size:12px; text-transform:uppercase;">Copyright &copy; | Right Click Media Sales App <?php echo date("Y"); ?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>