<?php
header('Content-Type: application/json');

$db['db_host'] = "localhost";
$db['db_user'] = "root";
$db['db_pass'] = "";
$db['db_name'] = "thesalesapp";

foreach($db as $key => $value){
    
    define(strtoupper($key), $value);
}

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

//  $row = mysqli_fetch_assoc($result);
//    $january = $row['total']; 

  

$sql = "SELECT SUM(process_value) as TOTAL, process_consultant  FROM processed_deals GROUP BY process_consultant
ORDER BY process_id";

$result = mysqli_query($conn,$sql);

$data = array();
foreach ($result as $row) {
	$data[] = $row;
}

mysqli_close($conn);

echo json_encode($data);
?>