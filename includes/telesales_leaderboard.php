             <div id="chart-container">
              <canvas id="graphCanvas"></canvas>
    </div>

    <script>
        $(document).ready(function () {
            showGraph();
        });


        function showGraph()
        {
            {
                $.post("../includes/telesales_data.php",
                function (data)
                {
                    console.log(data);
                     var name = [];
                    var marks = [];
                    console.log(data);
                    for (var i in data) {
                        name.push(data[i].app_telesales);
                        marks.push(data[i].NUM_OF_CLIENTS);
                    }

                    var chartdata = {
                        labels: name,
                        datasets: [
                            {
                                label: 'APPOINTMENTS MADE',
                                backgroundColor: '#49e2ff',
                                borderColor: '#46d5f1',
                                hoverBackgroundColor: '#CCCCCC',
                                hoverBorderColor: '#666666',
                                data: marks
                            }
                        ]
                    };

                    var graphTarget = $("#graphCanvas");

                    var barGraph = new Chart(graphTarget, {
                        type: 'bar',
                        data: chartdata
                    });
                });
            }
        }
        </script>