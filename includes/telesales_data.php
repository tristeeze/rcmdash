<?php
header('Content-Type: application/json');

$db['db_host'] = "localhost";
$db['db_user'] = "root";
$db['db_pass'] = "";
$db['db_name'] = "thesalesapp";

foreach($db as $key => $value){
    
    define(strtoupper($key), $value);
}

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

//   $query = "SELECT * FROM appointments ";
//    $sales_count_query = mysqli_query($connection, $query);
//    $app_count = mysqli_num_rows($sales_count_query);


$sqlQuery = "SELECT COUNT(app_id) as NUM_OF_CLIENTS, app_telesales  FROM appointments GROUP BY app_telesales
ORDER BY app_id";


$result = mysqli_query($conn,$sqlQuery);

$data = array();
foreach ($result as $row) {
	$data[] = $row;
}

mysqli_close($conn);

echo json_encode($data);
?>