<?php  include "includes/db.php"; ?>
<?php  include "includes/header.php"; ?>



<?php
require_once './vendor/autoload.php';
require_once './classes/Config.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
    if(!isset($_GET['forgot'])){
        header("Location: index");
    }
    if(ifItIsMethod('post')){
        if(isset($_POST['email'])) {
            $email = $_POST['email'];
            $length = 50;
            $token = bin2hex(openssl_random_pseudo_bytes($length));
            if(email_exists($email)){
                if($stmt = mysqli_prepare($connection, "UPDATE users SET token='{$token}' WHERE user_email= ?")){
                    mysqli_stmt_bind_param($stmt, "s", $email);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    /**
                     *
                     * configure PHPMailer
                     *
                     *
                     */
                    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                                 // Disable verbose debug output
                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = Config::SMTP_HOST;  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = Config::SMTP_USER;                 // SMTP username
                        $mail->Password = Config::SMTP_PASSWORD;                           // SMTP password
                        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = Config::SMTP_PORT;                                    // TCP port to connect to
                        //Recipients
                        $mail->setFrom('admin@rightclickmedia.co.za', 'RCM Sales');
                        $mail->addAddress($email, 'RCM Sales');
                        //Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->CharSet = 'UTF-8';
                        $mail->Subject = "Reset your password on Salesapp";
                        $mail->Body    = "<h2 align='center' height='50px' width='100%' bgcolor='#337ab7'>Please reset password</h2><p style='color: orange;'>Click on the link to reset your password
                        <br /><a href='http://localhost/the-salesapp/salesapp/reset.php?email={$email}&token={$token}'> {$token} </a> </p>";

                        if($mail->send()){
                            $emailSent = true;
                        } else{
                            $emailSent = false;
                            echo "NOT SENT";
                        }
                    } catch (Exception $e) {
                        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                    }


                }
            }
        }
     }
?>






<!-- Page Content -->
<div class="container">

    <div class="form-gap"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="site-branding">

                          <img src="images/rcm-logo.png" width="100%" style="width:100% !important;" />

                      </div>
                        <div class="text-center">


                        <?php if(!isset( $emailSent)): ?>


                                <h3><i class="fa fa-lock fa-4x"></i></h3>
                                <h2 class="text-center">Forgot Password?</h2>
                                <p>You can reset your password here.</p>
                                <div class="panel-body">




                                    <form id="register-form" role="form" autocomplete="off" class="form" method="post">

                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                <input id="email" name="email" placeholder="email address" class="form-control"  type="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit">
                                        </div>

                                        <input type="hidden" class="hide" name="token" id="token" value="">
                                    </form>

                                </div><!-- Body-->

                            <?php else: ?>


                                <h2>Please check your email</h2>
                                <p>&nbsp;</p>
                                <a href="index.php" style="text-transform:uppercase;font-size:14px;background-color:#286090; padding: 10px 20px;color: #fff; border-radius:5px;display:block;width:100%;margin:20px 0px;">Return to login page</a>


                            <?php endIf; ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <hr>

    <?php include "includes/footer.php";?>

</div> <!-- /.container -->
