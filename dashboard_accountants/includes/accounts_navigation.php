<!-- ADMIN Navigation -->
             <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <li class="useravatarmobi">
                        <div class="useravatar">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
        
            </div>
                <a class="navbar-brand" href="index.php">RCM ACCOUNTANT  <span style="color:#fff;"></span></a>
                           
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php 
                        
                        if(isset($_SESSION['user_firstname'])) {
                        echo $_SESSION['user_firstname']; 
                        }
                        
                        
                        ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                      
                        <li class="divider"></li>
                        <li>
                            <a href="../includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li>
                        <div class="useravatar useravatardrop">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
                    <li>
                        <a href="index.php"><i class="fas fa-globe"></i> Dashboard</a>
                    </li> 
                  
                 <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="far fa-calendar-plus" ></i> Deals <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="demo" class="collapse">
                                  <li>
                                <a href="appointments.php?source=add_client_man">Add Client</a>
                            </li>               
                             <li>
                                <a href="appointments.php?source=view_appointments">View All Closed Deals</a>
                            </li>
                                                   </ul>
                    </li>
                         <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo33"><i class="far fa-calendar-plus" ></i> Clients <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="demo33" class="collapse">
                                                
                             
                            <li>
                                <a href="clients.php?source=view_clients">Contract Clients</a>
                            </li>   
                               
                            <li>
                                <a href="clients.php?source=month_2_month">Month 2 Month Clients</a>
                            </li>
                             <li>
                                <a href="clients.php?source=view_client_payments">View Client Payments</a>
                            </li>  
                               
                               <li>
                                <a href="clients.php?source=upcoming_renewals">Upcoming Renewals</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo44"><i class="far fa-calendar-plus" ></i> Addons/Cross/Upsell <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="demo44" class="collapse">
                                                
                             
                            <li>
                                <a href="clients.php?source=add_addon">Clients</a>
                            </li>   
                             <li>
                                <a href="clients.php?source=all_addons">View Addons</a>
                            </li>     
                               
                               <li>
                                <a href="clients.php?source=cross_sells">View Cross Sells</a>
                            </li>   
                            
                            <li>
                                <a href="clients.php?source=up_sells">View Up Sells</a>
                            </li>   
                               
                          
                         
                        </ul>
                    </li>
                      
                       <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#rates"><i class="far fa-calendar-plus" ></i> Rate Cards <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="rates" class="collapse">
                                                
                             
                            <li>
                                <a href="rates.php?source=rate_card">Rate Card</a>
                            </li>       
                               <li>
                                <a href="rates.php?source=add_rate_item">Add Rate Item</a>
                            </li>   
                               
                          
                         
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#van"><i class="fa fa-calendar"></i> Book Transport <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="van" class="collapse">
                            <li>
                                <a href="book_van.php?source=book_van">BOOK VAN</a>
                            </li> 
                               <li>
                                <a href="book_van.php?source=view_van">VAN AVAILABILITY</a>
                            </li> 
                           
                               
                               <li>
                                <a href="book_van.php?source=my_van">YOUR BOOKINGS</a>
                            </li> 
                        </ul>
                    </li>
                    <li >
                        <a href="profile.php"><i class="fas fa-user"></i> Profile</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>