<?php include "../includes/dashboard_header.php" ?>

<div id="wrapper">

<?php include "includes/accounts_navigation.php" ?>
       
   <div id="page-wrapper">
        
<div class="container-fluid">

                <!-- Page Heading -->
<div class="row">
<div class="col-lg-12">
    <h1 class="page-header">
    WELCOME TO THE ACCOUNTANTS DASHBOARD
    
    <small><?php echo $_SESSION['user_firstname'] ?></small>
    </h1>

    </div>
                </div>
                <!-- /.row -->
                
                <?php echo '<h3>' . date("F") . '</h3>'; ?>
                       
                <!-- /.row -->
                
<div class="row">
              <?php 
    $current_month = date("m");
    ?>
               
  <div class="col-sm-4"> 
           
           <div class="panel panel-orange">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fas fa-hands-helping fa-4x" ></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM feedback WHERE feedback_Status = 'CLOSED'";
    $select_all_users = mysqli_query($connection, $query);
    $closed_deals = mysqli_num_rows($select_all_users);

    echo "<h3>{$closed_deals} CLOSED DEALS</h3>";
    
    
    ?>
                     
                  
                </div>
            </div>
            <a href="appointments.php?source=view_appointments">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div></div>
        
  <div class="col-sm-4">
      <div class="panel panel-purple">
            <div class="panel-heading">

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fas fa-file-contract fa-4x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM all_clients WHERE c_duration != 'Monthly'";
    $select_all_users = mysqli_query($connection, $query);
    $contract_client = mysqli_num_rows($select_all_users);

    echo "<h3>{$contract_client} CONTRACT CLIENTS</h3>";
    
    ?>
                    
                  
                </div>
            </div>
            <a href="clients.php?source=view_clients">
                <div class="panel-footer">
                    <span class="pull-left">VIEW ALL</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
      
      
  </div>
  <div class="col-sm-4">
      
      <div class="panel panel-blue">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-4x"></i>
                    </div>
                  
                    <?php
    
    $query = "SELECT * FROM all_clients WHERE c_duration = 'Monthly'";
    $select_all_users = mysqli_query($connection, $query);
    $monthly_client = mysqli_num_rows($select_all_users);

    echo "<h3>{$monthly_client} MONTH 2 MONTH</h3>";
    
    
    ?>
                     
                  
                </div>
            </div>
            <a href="clients.php?source=month_2_month">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
   
  </div>

</div>
                <!-- /.row -->
                
                <!--------STATS-------->
<div class="row">
                 <div class="col-sm-4">
      
    
      <div class="panel panel-purple">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fas fa-money-bill-wave-alt fa-4x"></i>
                    </div>
                  
                    <?php
    
    $sql="SELECT sum(client_ex_vat) as total FROM clients_accounts WHERE MONTH(client_date_paid) = $current_month ";
    $result = mysqli_query($connection,$sql);
    $row = mysqli_fetch_assoc($result);
    echo "<h3>R {$row['total']} REVENUE EX VAT</h3>";

    ?>
                  
                </div>
            </div>
            <a href="clients.php?source=view_client_payments">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>

  </div>
  <div class="col-sm-4"> 
           
           <div class="panel panel-blue">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                       <i class="fas fa-money-bill-wave-alt fa-4x" ></i>
                    </div>
                  
                    <?php
    
    $sql="SELECT sum(client_inc_vat) as total FROM clients_accounts WHERE MONTH(client_date_paid) = $current_month ";
    $result = mysqli_query($connection,$sql);
    $row = mysqli_fetch_assoc($result);
    echo "<h3>R {$row['total']} REVENUE INC VAT</h3>";

    ?>
                     
                </div>
            </div>
           <a href="clients.php?source=view_client_payments">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div></div>
        
  <div class="col-sm-4">
      <div class="panel panel-orange">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fas fa-calendar-check fa-4x"></i>
                    </div>
                  
                    <?php
    
                    $current_renew = date("Y-m", strtotime($client_renew));
                    $month = date('Y-m');
                    $final = date('Y-m',strtotime("+1 month"));

                        
    $query = "SELECT * FROM all_clients WHERE MONTH(c_renew) = $final";
    $select_all_users = mysqli_query($connection, $query);
    $user_count = mysqli_num_rows($select_all_users);

    echo "<h3>{$user_count} UPCOMING RENEWALS</h3>";

    ?>
<!--                        <div><h4>UPCOMING RENEWALS</h4> </div>-->
                </div>
            </div>
            <a href="clients.php?source=upcoming_renewals">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>

  </div>

</div>
           
            </div>
            
            
            <!-- /.container-fluid -->

        </div>
        
<!--
        <div class="panel panel-red">
            <div class="panel-heading">
               

                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-4x"></i>
                    </div>
                  
                    <?php
    
//    $query = "SELECT * FROM appointments";
//    $select_all_appointments = mysqli_query($connection, $query);
//    $appointment_count = mysqli_num_rows($select_all_appointments);
//
//    echo "<div class='huge'>{$appointment_count}</div>";
    
    
    ?>
                        <div> Appointments</div>
                  
                </div>
            </div>
            <a href="statistics.php?source=view_statistics">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
-->
        
        
        <!-- /#page-wrapper -->


  <?php include "includes/accounts_footer.php" ?>
   