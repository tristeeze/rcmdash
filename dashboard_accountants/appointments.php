<?php include "../includes/dashboard_header.php" ?>
    <div id="wrapper">
       <?php include "includes/accounts_navigation.php" ?>
        <div id="page-wrapper">
                <div class="container-fluid">

                <!-- Page Heading -->

                <div class="row">
                    <div class="col-lg-12">
                  

                   <h1 class="page-header"> DEALS </h1>

    <?php

        if(isset($_GET['source'])) {      

            $source = $_GET['source'];

        } else {

            $source = '';

        }

switch($source) {

        case 'view_appointments';
        include "../views/accounts/view_appointments_accountant.php";
        break ;
       
        case 'processed_deals';
        include "../views/accounts/processed_deals.php";
        break ;
       
        case 'process_appointment';
        include "../views/accounts/process_appointment.php";
        break ;
       
        case 'add_client';
        include "../views/accounts/add_client.php";
        break ;
        
        case 'add_client_man';
        include "../views/accounts/add_client_manual.php";
        break ;

        default: 
        include "../views/accounts/process_appointment.php";
        break;

}

        ?>
                    </div>
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->
        </div>
       

        <!-- /#page-wrapper -->
  <?php include "includes/accounts_footer.php" ?>   