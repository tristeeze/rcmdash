<?php include "../includes/dashboard_header.php" ?>
    <div id="wrapper">
       <?php include "includes/accounts_navigation.php" ?>
        <div id="page-wrapper">
                <div class="container-fluid">

                <!-- Page Heading -->

                <div class="row">
                    <div class="col-lg-12">
                  

                   <h1 class="page-header"> CLIENTS </h1>

    <?php

        if(isset($_GET['source'])) {      

            $source = $_GET['source'];

        } else {

            $source = '';

        }

switch($source) {

        case 'view_clients';
        include "../views/accounts/view_clients.php";
        break ;
        case 'month_2_month';
        include "../views/accounts/month_2_month.php";
        break ;
       
        case 'view_client_payments';
        include "../views/accounts/view_payments.php";
        break ;   
        
        case 'add_payment';
        include "../views/accounts/add_payment.php";
        break  ; 
        
        
        case 'edit_client';
        include "../views/accounts/edit_client.php";
        break  ;   
    
        case 'add_addon';
        include "../views/accounts/add_addon.php";
        break  ;   
    
        case 'addon_form';
        include "../views/accounts/addon_form.php";
        break  ; 
        
        case 'all_addons';
        include "../views/accounts/all_addons.php";
        break  ;  
    
        case 'cross_sells';
        include "../views/accounts/all_cross_sells.php";
        break  ;  
        
        case 'cross_sell_form';
        include "../views/accounts/cross_sell_form.php";
        break  ;      
    
        case 'up_sells';
        include "../views/accounts/up_sells.php";
        break  ; 
        
        case 'up_sell_form';
        include "../views/accounts/up_sell_form.php";
        break  ;  
    
        case 'upcoming_renewals';
        include "../views/accounts/upcoming_renewals.php";
        break  ;
    
        case 'renew_client';
        include "../views/accounts/renew_client.php";
        break  ;
        
        case 'client_details';
        include "../views/accounts/client_details.php";
        break  ;   
        
        case 'price_breakdown';
        include "../views/accounts/price_breakdown.php";
        break  ;  
        
        case 'client_history';
        include "../views/accounts/client_history.php";
        break  ;
        
        case 'camp_clients';
        include "../views/accounts/camp_clients.php";
        break  ; 
        
        case 'camp_history';
        include "../views/accounts/camp_history.php";
        break  ;
        
        case 'view_comments';
        include "../views/accounts/view_comments.php";
        break  ;
        
        case 'add_comment';
        include "../views/accounts/add_comment.php";
        break  ;


        default: 
        include "../views/accounts/view_clients.php";
        break;

}

        ?>
                    </div>
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->
        </div>
       

        <!-- /#page-wrapper -->
  <?php include "includes/accounts_footer.php" ?>   