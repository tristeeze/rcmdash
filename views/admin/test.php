<?php include'includes/header.php' ?>
    <?php include'includes/db.php' ?>

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

<style>
/* Login */
.bookCallwrap {
    width: 100%;
    background: url(images/call_bg.png);
/*    background-size: contain;*/
    background-attachment: fixed;
    background-size: cover;
    overflow: hidden;
}
    
    h1 {
        color: #3753a6;
        font-size: 36px;
        font-weight: bold;
    }
body {
    padding-top: 0px !important;
    font-size: 12px;
    font-family: 'Montserrat', sans-serif;
}
.callwrap {
    background-color: rgba(255,255,255,0.7);
    color: #333;
    width:60%;
    margin: auto;
    font-size: 14px;
}

.panel-default {
    border-color: rgba(0,0,0,0);
}
button.btn.btn-primary.btn-lg.btn-block {
    border-radius:0 50px 50px 0;
}
    .input-group-addon {
        padding: 6px 12px;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: #fff;
        border: 0px solid #ccc;
        border-radius: 100%;
}
.panel {
    background-color: #fff0 !important;
}
    
        
    .icons {
        position: fixed;
        right:0;
        top:10%;
        width:110px;
        z-index: 999999 !important;
            
    }
    
    .icons img {
        width:100px;
        display: block
    }
/* Login */
    
    @media (max-width:600px) {
        h1 {
            font-weight: bold;
            font-size: 22px;
        }
        .callwrap {
            width: 90%;
        }
        
            
    .icons {
        position: fixed;
        right:0;
        top: unset !important;
        bottom:0 !important;
        width:100% !important;
        z-index: 999999 !important;
        background-color:#fff;
        padding:10px;
            
    }
    
    .icons img {
        width:60px !important;
        display: inline !important; 
    }
    }
    
    
textarea,
input.text,
input[type="text"],
input[type="button"],
input[type="submit"],
    input[type=time],
    input[type=date],
.input-checkbox {
-webkit-appearance: none !important;
}

</style>

<?php
    $company    = $_GET['company'];
    $name       = $_GET['name'];
    $email      = $_GET['email'];
    $number     = $_GET['number'];
    $lead       = $_GET['lead'];
    
    if(isset($_POST['book_call'])) {

    $is_owner           = $_POST['owner'];
    $inBusiness         = $_POST['in_business'];
    $callTime           = $_POST['call_time'];
    $callDate           = $_POST['call_date'];
    $companyName        = $_POST['company_name'];   
    $name               = $_POST['name'];
    $email              = $_POST['email'];
    $contactNumber      = $_POST['number'];
    $country            = $_POST['country'];
    $province           = $_POST['province'];
    $lead               = $_POST['lead'];
    $status             = 'unclaimed';
        
$query = "INSERT INTO leads(isOwner, inBusiness, callTime, callDate, companyName, name, email, contactNumber, country, province, status, lead)";

$query .= "VALUES('{$is_owner}', '{$inBusiness}', '{$callTime}', '{$callDate}', '{$companyName}', '{$name}', '{$email}','{$contactNumber}','{$country}','{$province}','{$status}','{$lead}')";

$create_user_query = mysqli_query($connection, $query);

confirm($create_user_query );

$timeError = "CALL BOOKED";
//$location = 'call_scheduled.php'   ;     
//redirect($location);
//         header("Location:call_scheduled.php");
         
echo "<script> location.replace('call_scheduled.php'); </script>";
        
    }

    
    ?>

<div class="bookCallwrap">

    <!-- Page Content -->
<!--    <div class="container">-->

    <div class="row">
  <div class="col-sm-12">
        <div class="form-gap"></div>
<!--        <div class="container">-->
           <div class="book_appointment">
            
            <?php

$timeError = "Book Appointment";

if(isset($_POST['add_appointment'])) {

    $app_business       = $_POST['app_business'];
    $app_address        = $_POST['app_address'];
    $app_area           = $_POST['app_area'];
    $app_country        = $_POST['app_country'];
    $app_name           = $_POST['app_name'];   
    $app_number         = $_POST['app_number'];
    $app_alt_number     = $_POST['app_alt_number'];
    $app_website        = $_POST['app_website'];
    $app_email          = $_POST['app_email'];
    $app_comments       = $_POST['app_comments'];
    $app_competitors    = $_POST['app_competitors'];
    $app_time           = $_POST['app_time'];
    $app_date           = $_POST['app_date'];
    $unnaproved         = 'unapproved';
    $pending            = 'pending';
    $app_approved       = 'approved';
    $type               = 'meeting';
    

      
// CHECK TO SEE IF THE Vaughan HAS A MEETING IN THAT TIME ALREADY
    $results_query = "SELECT app_id FROM appointments WHERE (app_time='$app_time') AND (app_date='$app_date') AND (app_consultant='Vaughan') AND (app_rescheduled = 'No' ) AND (app_approved = '$app_approved' OR app_approved = '$pending')";

    $row        = mysqli_query($connection, $results_query);
    $num_rows   = mysqli_num_rows($row); 

      if(($num_rows == 0)) {
   
  	     $query = "SELECT * FROM users WHERE user_firstname = 'Vaughan'";
         $select_users = mysqli_query($connection,$query);
            while($row = mysqli_fetch_assoc($select_users)) {
                
                $user_id            = $row['user_id'];
                $user_team          = $row['user_team'];
                $app_consultant     = $row['user_firstname'];
                $app_telesales      = 'Client';

        $query = "INSERT INTO appointments(app_business, app_address, app_area, app_country, app_name, app_number, app_alt_number, app_website, app_email, app_comments, app_competitors, app_consultant, app_time,app_date, app_telesales, app_group, app_team,app_type)";

        $query .= "VALUES('{$app_business}', '{$app_address}', '{$app_area}', '{$app_country}', '{$app_name}', '{$app_number}','{$app_alt_number}','{$app_website}','{$app_email}','{$app_comments}','{$app_competitors}','{$app_consultant}','{$app_time}','{$app_date}','{$app_telesales}','{$app_group}','{$user_team}','{$type}')";

        $create_user_query = mysqli_query($connection, $query);

        confirm($create_user_query );

        $timeError = "APPOINTMENT BOOKED: " . " " . "<a href='appointments.php'>VIEW APPOINTMENTS</a>";

  	} 

     } // VAUGHAN

 else {
// CHECK TO SEE IF THE Claudio HAS A MEETING IN THAT TIME ALREADY
    $results_query = "SELECT app_id FROM appointments WHERE (app_time='$app_time') AND (app_date='$app_date') AND (app_consultant='Claudio') AND (app_rescheduled = 'No' ) AND (app_approved = '$app_approved' OR app_approved = '$pending')";

    $row        = mysqli_query($connection, $results_query);
    $num_rows   = mysqli_num_rows($row); 
     
     if(($num_rows == 0)) {
   
  	     $query = "SELECT * FROM users WHERE user_firstname = 'Claudio'";
         $select_users = mysqli_query($connection,$query);
            while($row = mysqli_fetch_assoc($select_users)) {
                
                $user_id            = $row['user_id'];
                $user_team          = $row['user_team'];
                $app_consultant     = $row['user_firstname'];
                $app_telesales      = 'Client';

        $query = "INSERT INTO appointments(app_business, app_address, app_area, app_country, app_name, app_number, app_alt_number, app_website, app_email, app_comments, app_competitors, app_consultant, app_time,app_date, app_telesales, app_group, app_team,app_type)";

        $query .= "VALUES('{$app_business}', '{$app_address}', '{$app_area}', '{$app_country}', '{$app_name}', '{$app_number}','{$app_alt_number}','{$app_website}','{$app_email}','{$app_comments}','{$app_competitors}','{$app_consultant}','{$app_time}','{$app_date}','{$app_telesales}','{$app_group}','{$user_team}','{$type}')";

        $create_user_query = mysqli_query($connection, $query);

        confirm($create_user_query );

        $timeError = "APPOINTMENT BOOKED: " . " " . "<a href='appointments.php'>VIEW APPOINTMENTS</a>";

  	} 

     }
        

}  // CLAUDIO 
    else {
// CHECK TO SEE IF THE Janine HAS A MEETING IN THAT TIME ALREADY
    $results_query = "SELECT app_id FROM appointments WHERE (app_time='$app_time') AND (app_date='$app_date') AND (app_consultant='Janine') AND (app_rescheduled = 'No' ) AND (app_approved = '$app_approved' OR app_approved = '$pending')";

    $row        = mysqli_query($connection, $results_query);
    $num_rows   = mysqli_num_rows($row); 
     
     if(($num_rows == 0)) {
   
  	     $query = "SELECT * FROM users WHERE user_firstname = 'Janine'";
         $select_users = mysqli_query($connection,$query);
            while($row = mysqli_fetch_assoc($select_users)) {
                
                $user_id            = $row['user_id'];
                $user_team          = $row['user_team'];
                $app_consultant     = $row['user_firstname'];
                $app_telesales      = 'Client';

        $query = "INSERT INTO appointments(app_business, app_address, app_area, app_country, app_name, app_number, app_alt_number, app_website, app_email, app_comments, app_competitors, app_consultant, app_time,app_date, app_telesales, app_group, app_team,app_type)";

        $query .= "VALUES('{$app_business}', '{$app_address}', '{$app_area}', '{$app_country}', '{$app_name}', '{$app_number}','{$app_alt_number}','{$app_website}','{$app_email}','{$app_comments}','{$app_competitors}','{$app_consultant}','{$app_time}','{$app_date}','{$app_telesales}','{$app_group}','{$user_team}','{$type}')";

        $create_user_query = mysqli_query($connection, $query);

        confirm($create_user_query );

        $timeError = "APPOINTMENT BOOKED: " . " " . "<a href='appointments.php'>VIEW APPOINTMENTS</a>";

  	} 

     }
        

}  // JANINE
    else {
// CHECK TO SEE IF THE Jeff HAS A MEETING IN THAT TIME ALREADY
    $results_query = "SELECT app_id FROM appointments WHERE (app_time='$app_time') AND (app_date='$app_date') AND (app_consultant='Jeff') AND (app_rescheduled = 'No' ) AND (app_approved = '$app_approved' OR app_approved = '$pending')";

    $row        = mysqli_query($connection, $results_query);
    $num_rows   = mysqli_num_rows($row); 
     
     if(($num_rows == 0)) {
   
  	     $query = "SELECT * FROM users WHERE user_firstname = 'Jeff'";
         $select_users = mysqli_query($connection,$query);
            while($row = mysqli_fetch_assoc($select_users)) {
                
                $user_id            = $row['user_id'];
                $user_team          = $row['user_team'];
                $app_consultant     = $row['user_firstname'];
                $app_telesales      = 'Client';

        $query = "INSERT INTO appointments(app_business, app_address, app_area, app_country, app_name, app_number, app_alt_number, app_website, app_email, app_comments, app_competitors, app_consultant, app_time,app_date, app_telesales, app_group, app_team,app_type)";

        $query .= "VALUES('{$app_business}', '{$app_address}', '{$app_area}', '{$app_country}', '{$app_name}', '{$app_number}','{$app_alt_number}','{$app_website}','{$app_email}','{$app_comments}','{$app_competitors}','{$app_consultant}','{$app_time}','{$app_date}','{$app_telesales}','{$app_group}','{$user_team}','{$type}')";

        $create_user_query = mysqli_query($connection, $query);

        confirm($create_user_query );

        $timeError = "APPOINTMENT BOOKED: " . " " . "<a href='appointments.php'>VIEW APPOINTMENTS</a>";

  	} 
     }
     } //Jeff
    else {
// CHECK TO SEE IF THE Jeff HAS A MEETING IN THAT TIME ALREADY
    $results_query = "SELECT app_id FROM appointments WHERE (app_time='$app_time') AND (app_date='$app_date') AND (app_consultant='Elio') AND (app_rescheduled = 'No' ) AND (app_approved = '$app_approved' OR app_approved = '$pending')";

    $row        = mysqli_query($connection, $results_query);
    $num_rows   = mysqli_num_rows($row); 
     
     if(($num_rows == 0)) {
   
  	     $query = "SELECT * FROM users WHERE user_firstname = 'Elio'";
         $select_users = mysqli_query($connection,$query);
            while($row = mysqli_fetch_assoc($select_users)) {
                
                $user_id            = $row['user_id'];
                $user_team          = $row['user_team'];
                $app_consultant     = $row['user_firstname'];
                $app_telesales      = 'Client';

        $query = "INSERT INTO appointments(app_business, app_address, app_area, app_country, app_name, app_number, app_alt_number, app_website, app_email, app_comments, app_competitors, app_consultant, app_time,app_date, app_telesales, app_group, app_team,app_type)";

        $query .= "VALUES('{$app_business}', '{$app_address}', '{$app_area}', '{$app_country}', '{$app_name}', '{$app_number}','{$app_alt_number}','{$app_website}','{$app_email}','{$app_comments}','{$app_competitors}','{$app_consultant}','{$app_time}','{$app_date}','{$app_telesales}','{$app_group}','{$user_team}','{$type}')";

        $create_user_query = mysqli_query($connection, $query);

        confirm($create_user_query );

        $timeError = "APPOINTMENT BOOKED: " . " " . "<a href='appointments.php'>VIEW APPOINTMENTS</a>";

  	} 
     }
     } //Elio
    
    
} // END

        echo $timeError;
?>

        <script>

        $(document).ready(function(){

            $('#myForm input, select').blur(function(){

                if(!$(this).val()){

                    $(this).addClass("error");

                } else{

                    $(this).removeClass("error");

                }

            });

        });

        </script>

<div class="card">

    <div class="card-content">

    <form id="myForm" action="" method="post" enctype="multipart/form-data">    

     <div class="form-group">

         <label>Name of Business</label>

         <input type="text" class="form-control" name="app_business" required>

         <span class="error"></span>

     </div>

        <div class="form-group">

         <label for="title">Business Address</label>

         <input type="text" class="form-control" name="app_address" required>

     </div>        

       <div class="form-group">

         <label for="post_status">Area</label>
           <input type="text" class="form-control" name="app_area" required> 
     </div> 

                     <div class="form-group">
         <label for="post_status">Country</label>
          <select  class="form-control" name="app_country" required>
              <option value="JHB">JHB</option>
              <option value="CT">CT</option>
              <option value="PTA">PTA</option>
              <option value="UK">UK</option>
          </select>
     </div> 

       <div class="form-group">
         <label for="post_status">Contact Full Name</label>
           <input type="text" class="form-control" name="app_name" required>
     </div> 
     <div class="form-group">

         <label for="post_status">Contact Number</label>
           <input type="text" class="form-control" name="app_number" required>
     </div>      
    <div class="form-group">

         <label for="post_status">Alternate Number</label>
           <input type="text" class="form-control" name="app_alt_number" >
     </div> 

         <div class="form-group">
         <label for="post_status">Business Website</label>
           <input type="text" class="form-control" name="app_website" >
     </div>          

        

        <div class="form-group">
         <label for="post_status">Comments</label>
           <input type="text" class="form-control" name="app_comments" required>

     </div> 

       <div class="form-group">
         <label for="post_status">Email</label>
           <input type="email" class="form-control" name="app_email" >
     </div> 

            <div class="form-group">
         <label for="post_status">Business Competitor</label>
           <input type="text" class="form-control" name="app_competitors" required >

     </div> 

        <div class="form-group">
         <label for="post_status">Meeting Time</label>
           <select type="text" class="form-control" name="app_time" d required>
               <option value='9:00'>9:00</option>
               <option value='10:15'>10:15</option>
               <option value='11:30'>11:30</option>
               <option value='12:45'>12:45</option>
               <option value='14:00'>14:00</option>
               <option value='15:30'>15:30</option>
            </select>
     </div> 

      <div class="form-group">

         <label for="post_status">Meeting Date</label>

           <input type="date" class="form-control" name="app_date" required>

     </div>



<div class="form-group">

    <input class="btn btn-primary" type="submit" name="add_appointment" value="Add Appointment">

     </div>

</form> 

    </div>

</div>

<script type="text/javascript">

$(document).ready(function() {

    $('select').material_select();

});

</script>
            
            
            
      </div>
      </div>

</div><!-- loginwrap -->