<a href="users.php?source=add_user_order&area=JHB" class="btn btn-dark" style="float:right">ADD SALES JHB</a>
<h3>JHB</h3>

<?php

$query = "SELECT * FROM JHB INNER JOIN users ON JHB.sales = users.user_id order by JHB.level ASC";
$result = mysqli_query($connection,$query);
if(mysqli_num_rows($result)>0)
{
	?>
	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>SALES</th>
		
		</tr>
		<tbody class="sortable">
	
		<?php
		while($row=mysqli_fetch_object($result))
		{
			?>
			<tr id="<?php echo $row->id;?>">
				<td><?php echo $row->id;?></td>
				<td><?php echo $row->user_firstname;?></td>
		
			</tr>
			<?php
		}
		?>
	</tbody>
	</table>
	<?php
}
?>

	

<script type="text/javascript">
	$(function(){
		$('.sortable').sortable({
			stop:function()
			{
				var ids = '';
				$('.sortable tr').each(function(){
					id = $(this).attr('id');
					if(ids=='')
					{
						ids = id;
					}
					else
					{
						ids = ids+','+id;
					}
				})
				$.ajax({
					url:'save_order.php',
					data:'ids='+ids,
					type:'post',
					success:function()
					{
						alert('Order saved successfully');
					}
				})
			}
		});
	});
</script>

<a href="users.php?source=add_user_order&area=PTA" class="btn btn-dark" style="float:right">ADD SALES PTA</a>

<h3>PTA</h3>
<?php

$query = "SELECT * FROM PTA INNER JOIN users ON PTA.sales = users.user_id order by PTA.level ASC";
$result = mysqli_query($connection,$query);
if(mysqli_num_rows($result)>0)
{
	?>
	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>SALES</th>
		
		</tr>
		<tbody class="sortablePTA">
	
		<?php
		while($row=mysqli_fetch_object($result))
		{
			?>
			<tr id="<?php echo $row->id;?>">
				<td><?php echo $row->id;?></td>
				<td><?php echo $row->user_firstname;?></td>
		
			</tr>
			<?php
		}
		?>
	</tbody>
	</table>
	<?php
}
?>

	

<script type="text/javascript">
	$(function(){
		$('.sortablePTA').sortable({
			stop:function()
			{
				var ids = '';
				$('.sortablePTA tr').each(function(){
					id = $(this).attr('id');
					if(ids=='')
					{
						ids = id;
					}
					else
					{
						ids = ids+','+id;
					}
				})
				$.ajax({
					url:'save_order_pta.php',
					data:'ids='+ids,
					type:'post',
					success:function()
					{
						alert('Order saved successfully');
					}
				})
			}
		});
	});
</script>

<a href="users.php?source=add_user_order&area=CT" class="btn btn-dark" style="float:right">ADD SALES CPT</a>

<h3>CPT</h3>
<?php

$query = "SELECT * FROM CT INNER JOIN users ON CT.sales = users.user_id order by CT.level ASC";
$result = mysqli_query($connection,$query);
if(mysqli_num_rows($result)>0)
{
	?>
	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>SALES</th>
		
		</tr>
		<tbody class="sortableCT">
	
		<?php
		while($row=mysqli_fetch_object($result))
		{
			?>
			<tr id="<?php echo $row->id;?>">
				<td><?php echo $row->id;?></td>
				<td><?php echo $row->user_firstname;?></td>
		
			</tr>
			<?php
		}
		?>
	</tbody>
	</table>
	<?php
}
?>

	

<script type="text/javascript">
	$(function(){
		$('.sortableCT').sortable({
			stop:function()
			{
				var ids = '';
				$('.sortableCT tr').each(function(){
					id = $(this).attr('id');
					if(ids=='')
					{
						ids = id;
					}
					else
					{
						ids = ids+','+id;
					}
				})
				$.ajax({
					url:'save_order_cpt.php',
					data:'ids='+ids,
					type:'post',
					success:function()
					{
						alert('Order saved successfully');
					}
				})
			}
		});
	});
</script>
<!--  <a href="users.php?source=add_user">Add User</a>-->

<a href="users.php?source=add_user_order&area=UK" class="btn btn-dark" style="float:right">ADD SALES UK</a>

<h3>UK</h3>
<?php

$query = "SELECT * FROM UK INNER JOIN users ON UK.sales = users.user_id order by UK.level ASC";
$result = mysqli_query($connection,$query);
if(mysqli_num_rows($result)>0)
{
	?>
	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>SALES</th>
		
		</tr>
		<tbody class="sortableUK">
	
		<?php
		while($row=mysqli_fetch_object($result))
		{
			?>
			<tr id="<?php echo $row->id;?>">
				<td><?php echo $row->id;?></td>
				<td><?php echo $row->user_firstname;?></td>
		
			</tr>
			<?php
		}
		?>
	</tbody>
	</table>
	<?php
}
?>

	

<script type="text/javascript">
	$(function(){
		$('.sortableUK').sortable({
			stop:function()
			{
				var ids = '';
				$('.sortableUK tr').each(function(){
					id = $(this).attr('id');
					if(ids=='')
					{
						ids = id;
					}
					else
					{
						ids = ids+','+id;
					}
				})
				$.ajax({
					url:'save_order_uk.php',
					data:'ids='+ids,
					type:'post',
					success:function()
					{
						alert('Order saved successfully');
					}
				})
			}
		});
	});
</script>