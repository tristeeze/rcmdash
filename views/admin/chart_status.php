<?php

if(isset($_POST['filter'])) {
    
    $from_date  = $_POST['from_date'];
    $to_date    = $_POST['to_date'];

if($stmt = $connection->query("SELECT feedback_status, COUNT(feedback_id) FROM feedback WHERE feedback_date BETWEEN '$from_date' AND '$to_date' GROUP BY feedback_status")){

$php_data_array = Array(); // create PHP array

while ($row = $stmt->fetch_row()) {
   
   $php_data_array[] = $row; // Adding to array
   }
}

echo "<script>
        var my_2d2 = ".json_encode($php_data_array)."
</script>";

}  else {
    
   if($stmt = $connection->query("SELECT feedback_status, COUNT(feedback_id) FROM feedback GROUP BY feedback_status")){

$php_data_array = Array(); // create PHP array

while ($row = $stmt->fetch_row()) {
   
   $php_data_array[] = $row; // Adding to array
   }
}

echo "<script>
        var my_2d2 = ".json_encode($php_data_array)."
</script>";
    
} 
?>

<h3 >APPOINTMENT STATUS</h3>

<div id="chart_div"></div>
<hr>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
 google.charts.load('current', {'packages':['corechart']});
     // Draw the pie chart when Charts is loaded.
      google.charts.setOnLoadCallback(draw_my_chart);
      // Callback that draws the pie chart
      function draw_my_chart() {
        // Create the data table .
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'appstatus');
        data.addColumn('number', 'Nos');
		for(i = 0; i < my_2d2.length; i++)
    data.addRow([my_2d2[i][0], parseInt(my_2d2[i][1])]);
// above row adds the JavaScript two dimensional array data into required chart format
    var options = {
                       width:450,
                       height:400};

        // Instantiate and draw the chart
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
</script>