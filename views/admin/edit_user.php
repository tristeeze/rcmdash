<?php

if(isset($_GET['edit_user'])){
    
    $the_user_id = $_GET['edit_user'];
    
    $query = "SELECT * FROM users WHERE user_id = $the_user_id ";
    $select_users_query = mysqli_query($connection,$query);
        while($row = mysqli_fetch_assoc($select_users_query)) {
            
        $user_id            = $row['user_id'];
        $user_firstname     = $row['user_firstname'];
        $user_lastname      = $row['user_lastname'];
        $user_email         = $row['user_email'];
        $user_number        = $row['user_number'];
        $user_group         = $row['user_group'];  
        $user_team          = $row['user_team'];  
        $user_status        = $row['user_status'];  
        $user_password      = $row['user_password'];
        $user_colour        = $row['user_colour'];
        $usersh             = password_hash($user_password, PASSWORD_DEFAULT);
        

}
    
}

if(isset($_POST['edit_user'])) {
    $user_id            = $the_user_id;
    $user_firstname     = $_POST['user_firstname'];
    $user_lastname      = $_POST['user_lastname'];
    $user_email         = $_POST['user_email'];
    $user_number        = $_POST['user_number'];
    $user_group         = $_POST['user_group'];   
    $user_team          = $_POST['user_team'];   
    $user_status        = $_POST['user_status']; 
    $user_password      = $_POST['user_password'];
    $user_colour        = $_POST['user_colour'];
    $usersh             = password_hash($user_password, PASSWORD_DEFAULT);
  
    $query = "UPDATE users SET ";
    $query .="user_firstname    = '{$user_firstname}', ";
    $query .="user_lastname     = '{$user_lastname}', ";
    $query .="user_email        = '{$user_email}', ";
    $query .="user_number       = '{$user_number}', ";
    $query .="user_group        =  '{$user_group}', ";
    $query .="user_team         =  '{$user_team}', ";
    $query .="user_status       =  '{$user_status}', ";
    $query .="user_password     = '{$usersh}', ";
    $query .="user_colour       = '{$user_colour}' ";
    

    $query .="WHERE user_id = {$user_id} ";

    
    $edit_user_query  = mysqli_query($connection, $query); 
    
    confirm($edit_user_query);
    
    echo "USER EDITED: " . " " . "<a href='users.php'>VIEW USERS</a> " ;
}


?>
    
    <form action="" method="post" enctype="multipart/form-data">    
     
     <div class="form-group">
         <label>First Name</label>
         <input type="text" class="form-control" name="user_firstname" value="<?php echo $user_firstname ?>">
     </div>
     
        <div class="form-group">
         <label for="title">Last Name</label>
         <input type="text" class="form-control" name="user_lastname" value="<?php echo $user_lastname ?>">
     </div>  
                
       <div class="form-group">
         <label for="post_status">Email</label>
           <input type="email" class="form-control" name="user_email" value="<?php echo $user_email ?>">
     </div> 
                  
       <div class="form-group">
         <label for="post_status">Number</label>
           <input type="test" class="form-control" name="user_number" value="<?php echo $user_number ?>">
     </div> 
     <div class="form-group">
       <label for="post_status">User Group</label>
        <select class="form-control" name="user_group" id="" value="<?php echo $user_group ?>">
            <option value="subscriber"> User Group</option>
            <option value="superadmin">Super Admin</option>
            <option value="admin">Admin</option>
            <option value="manager">Telesales Manager</option>
            <option value="salesmanager">Sales Manager</option>
            <option value="Sales">Sales</option>
            <option value="telesales">Telesales</option>
            <option value="telesalesr">External Telesales</option>
            <option value="accountant">Accountant</option>
            <option value="reception">Reception</option>
            <option value="reception_sales">Reception Sales</option>
        </select>
     </div> 
             <div class="form-group">
             <label for="post_status">User Team</label>
        <select class="form-control" name="user_team" id="" value="<?php echo $user_team ?>">
             <option value="Ironman"> Ironman</option>
            <option value="Thor"> Thor</option>
            <option value="Hulk"> Hulk</option>
            <option value="Wonderwoman"> Wonderwoman</option>
            <option value="Wizard"> Wizard</option>
            <option value="Wizard"> Muggle</option>
        </select>
     </div> 
     <div class="form-group">
        <select class="form-control" name="user_status" id="" value="<?php echo $user_status ?>">
            <option value="inactive"> User Status</option>
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
        </select>
     </div>        

     <div class="form-group">
         <label for="post_status">Password</label>
           <input type="password" class="form-control" name="user_password" value="<?php echo $user_password ?>">
     </div>
       
     <div class="form-group">
         <label for="post_status" >Colour</label>
           <input type="text" class="form-control" name="user_colour" value="<?php echo $user_colour ?>">
     </div>

     
<div class="form-group">
    <input class="btn btn-primary" type="submit" name="edit_user" value="Edit User">
     </div>
</form>
    