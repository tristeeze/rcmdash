<?php

if(isset($_GET['p_id'])) {
  
   $the_app_id = ($_GET['p_id']);
}

$query = "SELECT * FROM appointments WHERE app_id = $the_app_id ";
$select_app_by_id = mysqli_query($connection,$query);

while($row = mysqli_fetch_assoc($select_app_by_id)) {
        $app_id            = $row['app_id'];
        $app_business = $row['app_business'];
        $app_consultant = $row['app_consultant'];
        $app_address = $row['app_address'];
        $app_area = $row['app_area'];
        $app_name = $row['app_name'];
        $app_number = $row['app_number'];
        $app_alt_number = $row['app_alt_number'];
        $app_website = $row['app_website'];
        $app_comments = $row['app_comments'];
        $app_email = $row['app_email'];
        $app_competitors = $row['app_competitors'];
        $app_rescheduled = $row['app_rescheduled'];
        $app_time = $row['app_time'];
        $app_old_date = $row['app_date'];
        $app_telesales = $row['app_telesales'];
        $app_who_rescheduled = $row['app_who_rescheduled'];
}

$timeError = "Book Appointment";

if(isset($_POST['reschedule_appointment'])) {
    
    $app_business = $_POST['app_business'];
    $app_address = $_POST['app_address'];
    $app_area = $_POST['app_area'];
    $app_name = $_POST['app_name'];   
    $app_number = $_POST['app_number'];
    $app_alt_number = $_POST['app_alt_number'];
    $app_website = $_POST['app_website'];
    $app_email = $_POST['app_email'];
    $app_comments = $_POST['app_comments'];
    $app_competitors = $_POST['app_competitors'];
    $app_consultant = $_POST['app_consultant'];
    $app_rescheduled =  $_POST['app_rescheduled'];
    $app_who_rescheduled =  $_POST['app_who_rescheduled'];
    
         
      
    $query = "UPDATE appointments SET ";
    $query .="app_business  = '{$app_business}', ";
    $query .="app_address  = '{$app_address}', ";
    $query .="app_area  = '{$app_area}', ";
    $query .="app_name  = '{$app_name}', ";
    $query .="app_number  = '{$app_number}', ";
    $query .="app_alt_number  = '{$app_alt_number}', ";
    $query .="app_website  = '{$app_website}', ";
    $query .="app_email  = '{$app_email}', ";
    $query .="app_approved  = 'approved', ";
    $query .="app_comments  = '{$app_comments}' ";
    $query .= "WHERE app_id = {$the_app_id} ";

    $update_app = mysqli_query($connection,$query);
    
    confirm($update_app); 
        
    
    
    $timeError = "Appointment for " . $app_business  . " Has Been Updated <a href='appointments.php?source=view_appointments'>View Appointments</a>";
}
    echo $timeError;
    


?>
     <h3>Update Appointment For <?php echo $app_business ?> </h3>
    
    <form action="" method="post" enctype="multipart/form-data">    
     
     <div class="form-group">
         <label>Name of Business</label>
         <?php 
      echo "<input type='text' class='form-control' name='app_business' value='$app_business' >";
         ?>
     </div>
     
        <div class="form-group">
         <label for="title">Business Address</label>
         <?php
            
            echo "<input type='text' class='form-control' name='app_address' value='$app_address'>";
                
                ?>
     </div>  
                
       <div class="form-group">
         <label for="post_status">Area</label>
         <?php
           
           echo "<input type='text' class='form-control' name='app_area' value='$app_area'>";
               
               ?>
     </div> 
                  
       <div class="form-group">
         <label for="post_status">Contact Full Name</label>
            <?php
           
           echo "<input type='text' class='form-control' name='app_name' value='$app_name'>"
               ?>
     </div> 
     <div class="form-group">
         <label for="post_status">Contact Number</label>
            <?php
           
           echo "<input type='text' class='form-control' name='app_number' value='$app_number'>";
               
               ?>
     </div>      
    <div class="form-group">
         <label for="post_status">Alternate Number</label>
            <?php
           
           echo "<input type='text' class='form-control' name='app_alt_number' value='$app_alt_number'>";
               
               ?>
     </div> 
         <div class="form-group">
         <label for="post_status">Business Website</label>
             <?php
           
           echo "<input type='text' class='form-control' name='app_website' value='$app_website'>";
               
               ?>
     </div>          
        
        <div class="form-group">
         <label for="post_status">Comments</label>
            <?php
           
           echo "<input type='text' class='form-control' name='app_comments' value='$app_comments'>";
            
            ?>
     </div> 
       <div class="form-group">
         <label for="post_status">Email</label>
            <?php
           
           echo "<input type='email' class='form-control' name='app_email' value='$app_email'>";
           
           ?>
     </div> 
            <div class="form-group">
         <label for="post_status">Business Competitor</label>
           <?php
           
           echo "<input type='text' class='form-control' name='app_competitors' value='$app_competitors'>" ;
                
                ?>
     </div> 
   
     
<div class="form-group">
    <input class="btn btn-primary" type="submit" name="reschedule_appointment" value="Update Appointment">
     </div>
</form>