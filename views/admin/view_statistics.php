<!--FILTER SYSTEM-->
   <form method="post">

   <div class="row">
 
  <div class="col-sm-6"><div class="form-group">
         <label for="post_status">From Date</label>
           <input type="date" class="form-control" name="from_date">
     </div></div>
  <div class="col-sm-6"> <div class="form-group">
         <label for="post_status">To Date</label>
           <input type="date" class="form-control" name="to_date">
     </div></div>
</div>
         <select class='form-control' name='app_status' id='' >
            <option >Select Status</option>
             <option value="CLOSED">CLOSED</option>
             <option value="PENDING" value="PENDING">PENDING</option>
             <option value="RESCHEDULED">RESCHEDULED</option>
             <option value="NO SHOW">NO SHOW</option>
             <option value="UNQUALIFIED">UNQUALIFIED</option>
             <option value="NO MONEY">NO BUDGET</option>
             <option value="NOT INTERESTED">NOT INTERESTED</option>
             <option value="CANCELLED">CANCELLED</option>
             <option value="NO CLOSE">NO CLOSE</option>
        </select>

        <input type="submit" value="search" name="filter">
</form>

<?php include("../includes/search_form.php"); ?>

<div style="overflow-x:auto">
        <table id="statistics" class="table table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>Business</th>
            <th>Name</th>
            <th>Nr</th>
            <th>Email</th>
            <th>Date</th>
            <th>Sales</th>
            <th>Team</th>
            <th>Comments</th>
            <th>Presentation</th>
            <th>Partners</th>
            <th>Other Partners</th>
            <th>Sale</th>
            <th>Value</th>
            <th>Status</th>
            <th>Pending</th>
            <th>Cancelled Reason</th>
        </tr>

    </thead>
    <tbody id="myTable">

      <script type="text/javascript">
      var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
          , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(table, name) {
          if (!table.nodeType) table = document.getElementById(table)
          var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
          window.location.href = uri + base64(format(template, ctx))
        }
      })()
      </script>

      <script src="../js/tableexport.min.js" type="text/javascript"></script>
      <script src="../js/FileSaver.min.js" type="text/javascript"></script>

   <?php

        if(isset($_POST['filter'])){
            $sale_name                  = $_POST['app_consultant'];
            $app_selected_status        = $_POST['app_status'];
            $from_date                  = $_POST['from_date'];
            $to_date                    = $_POST['to_date'];

$query2 = "SELECT * FROM feedback INNER JOIN appointments ON feedback.app_id = appointments.app_id WHERE feedback_status = '$app_selected_status' AND feedback_date BETWEEN '$from_date' AND '$to_date' ORDER BY feedback_date DESC" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {
    
$feedback_id                = $row['feedback_id'];
$feedback_business          = $row['feedback_business'];
$feedback_date              = $row['feedback_date'];
$feedback_sales             = $row['feedback_sales'];
$feedback_team              = $row['feedback_team'];
$feedback_comment           = $row['feedback_comment'];
$feedback_presentation      = $row['feedback_presentation'];
$feedback_partners          = $row['feedback_partners'];
$feedback_other_partners    = $row['feedback_other_partners'];
$feedback_sale              = $row['feedback_sale'];
$feedback_sale_value        = $row['feedback_sale_value'];
$feedback_status            = $row['feedback_status'];
$feedback_pending           = $row['feedback_pending'];
$feedback_cancelled         = $row['feedback_cancelled'];
$app_name                   = $row['app_name'];
$app_contact                = $row['app_number'];
$app_mail                   = $row['app_email'];

echo "<tr>";
echo "<td>$feedback_id</td>";
echo "<td>$feedback_business</td>";
echo "<td>$app_name</td>";
echo "<td>$app_contact</td>";
echo "<td>$app_mail</td>";
echo "<td>$feedback_date</td>";
echo "<td>$feedback_sales</td>";
echo "<td>$feedback_team</td>";
echo "<td>$feedback_comment</td>";
echo "<td>$feedback_presentation</td>";
echo "<td>$feedback_partners</td>";
echo "<td>$feedback_other_partners</td>";
echo "<td>$feedback_sale</td>";
echo "<td>$feedback_sale_value</td>";
echo "<td>$feedback_status</td>";
echo "<td>$feedback_pending</td>";
echo "<td>$feedback_cancelled</td>";
echo "</tr>";

         }
        } else {

             $sql="SELECT sum(feedback_sale_value) as total FROM feedback ";

$result = mysqli_query($connection,$sql);

$row = mysqli_fetch_assoc($result);

echo "<strong>TOTAL:</strong> " . $row['total'];
            
$query2 = "SELECT * FROM feedback INNER JOIN appointments ON feedback.app_id = appointments.app_id ORDER BY feedback_date DESC";
$select_appointments2 = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments2)) {
    
$feedback_id                = $row['feedback_id'];
$feedback_business          = $row['feedback_business'];
$feedback_date              = $row['feedback_date'];
$feedback_sales             = $row['feedback_sales'];
$feedback_team              = $row['feedback_team'];
$feedback_comment           = $row['feedback_comment'];
$feedback_presentation      = $row['feedback_presentation'];
$feedback_partners          = $row['feedback_partners'];
$feedback_other_partners    = $row['feedback_other_partners'];
$feedback_sale              = $row['feedback_sale'];
$feedback_sale_value        = $row['feedback_sale_value'];
$feedback_status            = $row['feedback_status'];
$feedback_pending           = $row['feedback_pending'];
$feedback_cancelled         = $row['feedback_cancelled'];
$app_name                   = $row['app_name'];
$app_contact                = $row['app_number'];
$app_mail                   = $row['app_email'];

echo "<tr>";
echo "<td>$feedback_id</td>";
echo "<td>$feedback_business</td>";
echo "<td>$app_name</td>";
echo "<td>$app_contact</td>";
echo "<td>$app_mail</td>";
echo "<td>$feedback_date</td>";
echo "<td>$feedback_sales</td>";
echo "<td>$feedback_team</td>";
echo "<td>$feedback_comment</td>";
echo "<td>$feedback_presentation</td>";
echo "<td>$feedback_partners</td>";
echo "<td>$feedback_other_partners</td>";
echo "<td>$feedback_sale</td>";
echo "<td>$feedback_sale_value</td>";
echo "<td>$feedback_status</td>";
echo "<td>$feedback_pending</td>";
echo "<td>$feedback_cancelled</td>";

echo "</tr>";
}
        }

 ?>