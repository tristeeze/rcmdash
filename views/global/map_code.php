<?php

if(isset($_GET['p_id'])) {
  
   $the_app_id = ($_GET['p_id']);
}

$query = "SELECT * FROM appointments WHERE app_id = $the_app_id ";
$select_app_by_id = mysqli_query($connection,$query);

while($row = mysqli_fetch_assoc($select_app_by_id)) {
        $app_id            = $row['app_id'];
        $app_business = $row['app_business'];
        $app_consultant = $row['app_consultant'];
        $app_address = $row['app_address'];
        $app_area = $row['app_area'];
        $app_name = $row['app_name'];
        $app_number = $row['app_number'];
        $app_alt_number = $row['app_alt_number'];
        $app_website = $row['app_website'];
        $app_comments = $row['app_comments'];
        $app_email = $row['app_email'];
        $app_competitors = $row['app_competitors'];
        $app_rescheduled = $row['app_rescheduled'];
        $app_time = $row['app_time'];
        $app_old_date = $row['app_date'];
        $app_telesales = $row['app_telesales'];
        $app_who_rescheduled = $row['app_who_rescheduled'];
}
$app_address = 'whatever';
         
?>
   <input  id='pac-input' class='controls' type='text' placeholder='Search Box' value='<?php $app_address ?>' >

  
    <div id="map"></div>
   
    <script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN3eYc8tiNOn67QLx7VHP0NSvXfvIpTLg&libraries=places&callback=initAutocomplete"
         async defer></script>