<?php
// Set your timezone
date_default_timezone_set('Africa/Johannesburg');

// Get prev & next month
if (isset($_GET['ym'])) {
    $ym = $_GET['ym'];
} else {
    // This month
    $ym = date('Y-m');
}
// Check format
$timestamp = strtotime($ym . '-01');
if ($timestamp === false) {
    $ym = date('Y-m');
    $timestamp = strtotime($ym . '-01');
}
// Today
$today = date('Y-m-j', time());
// For H3 title
$html_title = date('Y / m', $timestamp);
// Create prev & next month link     mktime(hour,minute,second,month,day,year)
$prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
$next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));
// You can also use strtotime!
// $prev = date('Y-m', strtotime('-1 month', $timestamp));
// $next = date('Y-m', strtotime('+1 month', $timestamp));
// Number of days in the month
$day_count = date('t', $timestamp);
 
// 0:Sun 1:Mon 2:Tue ...
$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));
//$str = date('w', $timestamp);
// Create Calendar!!
$weeks = array();
$week = '';

   

// Add empty cell
$week .= str_repeat('<td></td>', $str);
 
for ( $day = 1; $day <= $day_count; $day++, $str++) {
     
    $date = $ym . '-' . $day;
        
    if ($today == $date ) {
        $week .= '<td class="today">' . $day . '<br />';
        
    } 
else {
     $week .= '<td>' . $day . '<br />';
}
    
    
       $query = "SELECT * FROM boardroom WHERE meeting_date = '{$date}'  ";
    
$select_booking = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_booking)) {
$meeting_id = $row['meeting_id'];
$meeting_member = $row['meeting_member'];
$meeting_client = $row['meeting_client'];
$meeting_date     = $row['meeting_date'];
$meeting_start_time     = $row['meeting_start_time'];
$meeting_end_time     = $row['meeting_end_time'];

    $week .= '<div class="app_calendar">' .  $meeting_start_time . " - " . $meeting_end_time .  "<br />" . $meeting_member . " <strong>" . $meeting_client  . "</strong>" . '</div>' ;
 }
    
   
    $week .= '</td>';
     

    // End of the week OR End of the month
    if ($str % 7 == 6 || $day == $day_count) {
        if ($day == $day_count) {
            // Add empty cell
            $week .= str_repeat('<td></td>', 6 - ($str % 7));
        }
        $weeks[] = '<tr>' . $week . '</tr>';
        // Prepare for new week
        $week = '';
    }
}


?>
                    
                    <div class="container">
        <h3><a href="?ym=<?php echo $prev; ?>">&lt;</a> <?php echo $html_title; ?> <a href="?ym=<?php echo $next; ?>">&gt;</a></h3>
        <div style="overflow-x:auto">
        <table class="table table-bordered">
            <tr>
                <th>SUNDAY</th>
                <th>MONDAY</th>
                <th>TUESDAY</th>
                <th>WEDNESDAY</th>
                <th>THURSDAY</th>
                <th>FRIDAY</th>
                <th>SATURDAY</th>
            </tr>
            <?php
            
             
      foreach ($weeks as $week) {
                    echo  $week;
                   
                }
    
              
            ?>
        </table>
    </div>
    </div>

            
        <!-- /#page-wrapper -->

  