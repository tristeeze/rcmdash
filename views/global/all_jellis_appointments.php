 <style>
* {
  box-sizing: border-box;
}

#myInput {
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>
<?php include("../includes/search_form.php"); ?>
<div style="overflow-x:auto">
<table  class="table table-bordered">
    <thead>
        <tr class="header">
            <th>ID</th>
            <th>BUSINESS</th>
            <th>AREA</th>
            <th>MAP</th>
            <th>NAME</th>
            <th>PHONE</th>
            <th>TIME</th>
            <th>DATE</th>
            <th>VIEW</th>
            <th>2ND APP</th>
            <th>EDIT</th>
            <th>FEEDBACK</th>

        </tr>

    </thead>
    <tbody id="myTable">

        <script type="text/javascript">
        var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
          , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(table, name) {
          if (!table.nodeType) table = document.getElementById(table)
          var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
          window.location.href = uri + base64(format(template, ctx))
        }
      })()
      </script>
        <script src="../js/tableexport.min.js" type="text/javascript"></script>
        <script src="../js/FileSaver.min.js" type="text/javascript"></script>
                                   
   <?php
$query = "SELECT * FROM users WHERE user_firstname = '{$_SESSION['user_firstname']}'";
$get_colour = mysqli_query($connection,$query);
        
while($row = mysqli_fetch_assoc($get_colour)) {
    $user_colour    =  $row['user_colour'];
    $user_firstname =  $row['user_firstname'];
  
}
        
        $no_lead = 0;
        $no_lead2 = 'NULL';
        
   
$query = "SELECT * FROM appointments WHERE lead_id != $no_lead ORDER BY app_date DESC";
$select_appointments = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_appointments)) {
    
    $app_id                 = $row['app_id'];
    $app_business           = $row['app_business'];
    $app_area               = $row['app_area'];
    $app_address            = $row['app_address'];
    $app_country            = $row['app_country'];
    $app_name               = $row['app_name'];
    $app_number             = $row['app_number'];
    $app_alt_number         = $row['app_alt_number'];
    $app_website            = $row['app_website'];
    $app_comments           = $row['app_comments'];
    $app_email              = $row['app_email'];
    $app_competitors        = $row['app_competitors'];
    $app_time               = $row['app_time'];
    $app_date               = $row['app_date'];
    $app_feedback           = $row['app_feedback'];
    $lead_id                = $row['lead_id'];
    $updated                = 'updated';
    $app_google_map         = $app_address . ', ' . $app_area . ', ' . $app_country;

    // SET BACKGROUND COLOUR DEPENDING ON IF APP FEEDBACK HAS BEEN GIVEN
    if ($app_feedback != $updated) {
        $app_color = '#d4942e';
    } else {
        $app_color = '#4e8f43'; 
    }
    // SET BORDER TO COLOUR IF IT COMES FROM JELLIS
    if($lead_id != NULL AND $lead_id != 0) {
        $border = '#2f4f4f';
    } else {
        $border = 'transparent';
    }

echo "<tr style='border-left:4px solid" . $border . " '>";
echo "<td>$app_id</td>";
echo "<td>$app_business</td>";
echo "<td>$app_area</td>";    
echo "<td><a href='http://maps.google.com/?q=$app_google_map' target='_blank'><i class='fas fa-map-marker-alt'></i></a></td>"; 
    
//    echo "<td><a href='map.php?view_map&p_id={$app_id}'><i class='fas fa-map-marker-alt'></i></a></td>";    
echo "<td>$app_name</td>";    
echo "<td><a href='tel:$app_number'>$app_number</a></td>";
echo "<td>$app_time</td>";
echo "<td>$app_date</td>";
echo "<td><a href='appointments.php?source=single_appointment&p_id={$app_id}'>View</a></td>";
echo "<td><a href='appointments.php?source=second_meeting&p_id={$app_id}'>2nd App</a></td>";
echo "<td><a href='appointments.php?source=edit_appointment&p_id={$app_id}'>Edit</a></td>";
echo "<td style='background-color:" . $app_color . "'><a style='font-weight:bold; color:#fff;' href='appointments.php?source=add_feedback&p_id={$app_id}'>Update</a></td>";
    

echo "</tr>";

       }                        
 ?>
                
                            </tbody>
                        </table>
                        <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 1; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

                            <script type='text/javascript'>
$(function(){
var overlay = $('<div id="overlay"></div>');
overlay.show();
overlay.appendTo(document.body);
$('.popup').show();
$('.close').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});

$('.x').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});
});
</script>
                         <?php
    

    
        $user_group = $_SESSION['user_group'];

    
    if($user_group != 'superadmin') {
    
$query = "SELECT * FROM appointments WHERE app_consultant = '{$_SESSION['user_firstname']}' AND app_feedback = 'none' AND app_approved = 'approved' AND app_rescheduled = 'No' ";
$select_appointments = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_appointments)) {
date_default_timezone_set('Africa/Johannesburg');
    
        $app_id     = $row['app_id'];
        $date_now   = date("Y-m-d");
        $app_date   = $row['app_date'];    
    
    if($date_now > $app_date) {
        
echo "<div class='popup'>";
echo "<div class='cnt223'>";
echo "<h1>Update Feedback</h1>";
echo "<p>";
echo "You need to update your previous appointment feedback";
echo "<br/>";
echo "<a href='appointments.php?source=add_feedback&p_id={$app_id}' >Update Feedback</a>";
echo "</p>";
echo "</div>";
echo "</div>";
} 
}    }
        
?>
</div>