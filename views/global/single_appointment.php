<?php

if(isset($_GET['p_id'])) {
  
   $the_app_id = ($_GET['p_id']);
}

$query = "SELECT * FROM appointments WHERE app_id = $the_app_id ";
$select_app_by_id = mysqli_query($connection,$query);

while($row = mysqli_fetch_assoc($select_app_by_id)) {
        $app_id = $row['app_id'];
        $app_approved = $row['app_approved'];
        $app_business = $row['app_business'];
        $app_address = $row['app_address'];
        $app_area = $row['app_area'];
        $app_name = $row['app_name'];
        $app_number = $row['app_number'];
        $app_alt_number = $row['app_alt_number'];
        $app_website = $row['app_website'];
        $app_comments = $row['app_comments'];
        $app_comment = $row['app_comment'];
        $app_email = $row['app_email'];
        $app_competitors = $row['app_competitors'];
        $app_time = $row['app_time'];
        $app_date = $row['app_date'];
        $app_old_date = $row['app_old_date'];
        $app_old_id = $row['app_old_id'];
        $app_who_rescheduled = $row['app_who_rescheduled'];
        $app_rescheduled = $row['app_rescheduled'];
       
}

?>

<div style="overflow-x:auto">
<table class="table table-bordered">
   
        <tr>
            <td><strong>APP ID</strong></td>
            <td><?php echo $app_id ?></td>
        </tr>
           <tr>
            <td><strong>APP OLD ID</strong></td>
            <td><?php echo $app_old_id ?></td>
        </tr>
         <tr>
            <td><strong>APP APPROVED</strong></td>
            <td><?php echo $app_approved ?></td>
        </tr>
        <tr>
            <td><strong>BUSINESS NAME</strong></td>
            <td><?php echo $app_business ?></td>
        </tr>
         <tr>
            <td><strong>CLIENT NAME</strong></td>
            <td><?php echo $app_name ?></td>
        </tr>
        <tr>
            <td><strong>APPOINTMENT ADDRESS</strong></td>
            <td><?php echo $app_address ?></td>
        </tr>
        <tr>
            <td><strong>APPOINTMENT AREA</strong></td>
            <td><?php echo $app_area ?></td>
        </tr>
            <tr>
            <td><strong>APPOINTMENT RESCHEDULED</strong></td>
            <td><?php echo $app_rescheduled ?></td>
        </tr>
                   <tr>
            <td><strong>RESCHEDULED BY</strong></td>
            <td><?php echo $app_who_rescheduled ?></td>
        </tr>
                <tr>
            <td><strong>APPOINTMENT OLD DATE</strong></td>
            <td><?php echo $app_old_date ?></td>
        </tr>
          <tr>
            <td><strong>APPOINTMENT DATE</strong></td>
            <td><?php echo $app_date ?></td>
        </tr>
                  <tr>
            <td><strong>APPOINTMENT TIME</strong></td>
            <td><?php echo $app_time ?></td>
        </tr>
        <tr>
            <td><strong>CLIENT CONTACT NUMBER</strong></td>
            <td><a href='tel:<?php echo $app_number ?>'><?php echo $app_number ?></a></td>
        </tr>
           <tr>
            <td><strong>CLIENT ALTERNATIVE CONTACT NUMBER</strong></td>
            <td><a href='tel:<?php echo $app_alt_number ?>'><?php echo $app_alt_number ?></a></td>
        </tr>         
        <tr>
            <td><strong>CLIENT EMAIL</strong></td>
            <td><a href='mailto:<?php echo $app_email ?>'><?php echo $app_email ?></a></td>
        </tr>
                   <tr>
            <td><strong>BUSINESS WEBSITE</strong></td>
            <td><a href='<?php echo $app_website ?>' target="_blank"><?php echo $app_website ?></a></td>
        </tr>
        <tr>
            <td><strong>BUSINESS COMPETITOR</strong></td>
            <td><?php echo $app_competitors ?></td>
        </tr>
             <tr>

            <td><strong>APP COMMENT</strong></td>

            <td><?php echo $app_comments ?></td>

        </tr>    
            
              <tr>

            <td><strong>WHY UNNAPROVED</strong></td>

            <td><?php echo $app_comment ?></td>

        </tr>
</table>
</div>
     