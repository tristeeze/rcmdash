<div style="overflow-x:auto">
<?php include("../includes/search_form.php"); ?>

<table id="leads" class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>BY</th>
            <th>SALES</th>
            <th>OWNER</th>
            <th>YRS/BUS</th>
            <th>DATE</th>
            <th>TIME</th>
            <th>COMPANY</th>
            <th>NAME</th>
            <th>EMAIL</th>
            <th>NR</th>
            <th>CTRY</th>
            <th>PROVINCE</th>
            <th>PRODUCT</th>
            <th>STATUS</th>
<!--            <th>IMAGE</th>-->
            <th>FEED<br />BACK</th>
            <th>MEETING</th>
            <th>AUDIT</th>
                 
        </tr>

    </thead>
    <tbody id="myTable">
    
                                   
   <?php
   $user_id         = $_SESSION['id']; 

    
    $query = "SELECT leads.*, users.* FROM leads INNER JOIN users ON users.user_id = leads.sales WHERE status != 'approved' AND sales = '$user_id'"; 

        
$select_leads = mysqli_query($connection,$query);
        
while($row = mysqli_fetch_assoc($select_leads)) {
   
$lead_id                    = $row['id'];
$jellisId                   = $row['jellis_id'];
$lead_owner                 = $row['isOwner'];
$sales_person               = $row['user_firstname'];
$lead_business_years        = $row['inBusiness'];
$lead_date                  = $row['callDate'];
$lead_time                  = $row['callTime'];
$lead_company               = $row['companyName'];
$lead_name                  = $row['name'];
$lead_email                 = $row['email'];
$lead_number                = $row['contactNumber'];
$lead_country               = $row['country'];
$lead_province              = $row['province'];
$lead_sales                 = $row['sales'];
$lead_status                = $row['status'];
$lead_lead                  = $row['lead'];
$lead_by                    = $row['add_by'];
$lead_file                  = $row['lead_file'];
    
    if($lead_by == 'Client') {
        $color = "#c12f40";
    } else {
        $color = "#898989";
    }
    
$query = "SELECT * FROM users WHERE user_id = '$lead_by'  ";

    $select_tele = mysqli_query($connection,$query);
    while($row = mysqli_fetch_assoc($select_tele)) {
    $added_by = $row['user_firstname'];

}
    
$query = "SELECT PdfUrl FROM jellisLeads WHERE id = '$jellisId'  ";

$select_audit = mysqli_query($connection,$query);

while($row = mysqli_fetch_assoc($select_audit)) {

$audit = $row['PdfUrl'];

}
   


echo "<tr style='border-left:8px solid $color'>";
echo "<td>$lead_id</td>";
echo "<td>$added_by</td>";
echo "<td>$sales_person</td>";
echo "<td>$lead_owner</td>";
echo "<td>$lead_business_years</td>";   
echo "<td>$lead_date</td>";
echo "<td>$lead_time</td>";
echo "<td>$lead_company</td>";
echo "<td>$lead_name</td>";
echo "<td><a href='mailto:$lead_email'>$lead_email</a></td>";
echo "<td><a href='tel:$lead_number'>$lead_number</a></td>";
echo "<td>$lead_country</td>";
echo "<td>$lead_province</td>";
echo "<td>$lead_lead</td>";
echo "<td>$lead_status</td>";
//echo "<td><a href='$lead_file' download>Download</a></td>";
echo "<td><a href='leads.php?source=add_feedback&p_id={$lead_id}'>Update</a>/ <a href='leads.php?source=single_lead_feedback&p_id={$lead_id}'>View</a></td>";
echo "<td><a href='appointments.php?source=add_appointment&lead_id={$lead_id}&jellis_id={$jellisId}'>Set</a></td>";
//echo "<td><a href='leads.php?claim={$lead_id}&id={$user_id}'Feedback</a></td>";
    if(!$audit) {
        echo "<td>NONE</td>";

    } else {
        echo "<td><a href='$audit' download><i class='fas fa-download'></i></a></td>";

    }
echo "</tr>";

       }                  
    
 
 ?>
                            </tbody>
                        </table>
</div>