<?php

// Set your timezone

date_default_timezone_set('Africa/Johannesburg');



// Get prev & next month

if (isset($_GET['ym'])) {

    $ym = $_GET['ym'];

} else {

    // This month

    $ym = date('Y-m');

}

// Check format

$timestamp = strtotime($ym . '-01');

if ($timestamp === false) {

    $ym = date('Y-m');

    $timestamp = strtotime($ym . '-01');

}

// Today

$today = date('Y-m-j', time());

// For H3 title

$html_title = date('Y / m', $timestamp);

// Create prev & next month link     mktime(hour,minute,second,month,day,year)

$prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));

$next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));

// You can also use strtotime!

// $prev = date('Y-m', strtotime('-1 month', $timestamp));

// $next = date('Y-m', strtotime('+1 month', $timestamp));

// Number of days in the month

$day_count = date('t', $timestamp);

 

// 0:Sun 1:Mon 2:Tue ...

$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));

//$str = date('w', $timestamp);

// Create Calendar!!

$weeks = array();

$week = '';



   



// Add empty cell

$week .= str_repeat('<td></td>', $str);

 

for ( $day = 1; $day <= $day_count; $day++, $str++) {

     

    $date = $ym . '-' . $day;

        

    if ($today == $date ) {

        $week .= '<td class="today">' . $day . '<br />';

        

    } 

else {

     $week .= '<td>' . $day . '<br />';

}


  $user_id         = $_SESSION['id']; 
//  $query2 = "SELECT * FROM leads WHERE callDate = '{$date}' AND sales = '{$user_id}' ORDER BY callTime";

//  $query2 = "SELECT leads.*, users.user_firstname, users.user_team FROM leads INNER JOIN users ON users.user_id = leads.sales WHERE callDate = '{$date}' AND users.user_status = 'active' AND users.user_team = '{$_SESSION['user_team']}' ORDER BY callTime";
//    
//$select_appointments2 = mysqli_query($connection,$query2);
//while($row = mysqli_fetch_assoc($select_appointments2)) {
//    
//    $app_time               = $row['callTime'];
//    $app_consultant         = $row['user_firstname'];
//    $app_business           = $row['companyName'];
//    $icon                   = "<i class='fas fa-phone'></i>";
//
//    
//    $week .= '<div class="app_calendar" style="background-color: #f3f3f3 ; border-left:4px solid ' . $app_color .'">' . $icon . " " .  $app_time . " <strong>" . $app_consultant . ",". $app_2nd_consultant . " </strong>" . $app_business . " " .  $app_area . '<a href="appointments.php?source=single_appointment&p_id='."{$app_id}".'"> VIEW </a>' . "<hr />" .  '</div>' ;
//    
// }      

    

$query = "SELECT * FROM appointments WHERE app_team = '{$_SESSION['user_team']}' AND app_date = '{$date}' AND  app_rescheduled = 'No'  ORDER BY app_time";

$select_appointments = mysqli_query($connection,$query);

while($row = mysqli_fetch_assoc($select_appointments)) {

$app_id                 = $row['app_id'];
$app_time               = $row['app_time'];
$app_consultant         = $row['app_consultant'];
$app_2nd_consultant     = $row['app_2nd_sales'];
$app_business           = $row['app_business'];
$app_approved           = $row['app_approved'];
$app_area               = $row['app_area'];
$lead_id                = $row['lead_id'];
$approved               = 'approved';
$pending                = 'pending';
$icon                   = "<i class='fas fa-map-marker-alt'></i>";


    if ($app_approved != $approved) {
        $app_color = '#daac62';
    } else {
        $app_color = '#a9d7a2'; 
    }
    
    if($lead_id != NULL AND $lead_id != 0) {
        $border = '#2f4f4f';
    } else {
        $border = 'transparent';
    }


    $week .= '<div class="app_calendar" style="background-color: #f3f3f3 ; border-left:4px solid ' . $app_color .'; border-right:4px solid ' . $border .'">' . $icon . " " .  $app_time . " <strong>" . $app_consultant . ",". $app_2nd_consultant . " </strong>" . $app_business . " " .  $app_area . '<a href="appointments.php?source=single_appointment&p_id='."{$app_id}".'"> VIEW </a>' . "<hr />" .  '</div>' ;

 }

    $week .= '</td>';

    // End of the week OR End of the month

    if ($str % 7 == 6 || $day == $day_count) {

        if ($day == $day_count) {

            // Add empty cell

            $week .= str_repeat('<td></td>', 6 - ($str % 7));

        }

        $weeks[] = '<tr>' . $week . '</tr>';

        // Prepare for new week

        $week = '';

    }

}


?>

                    <div class="container">

        <h3><a href="?ym=<?php echo $prev; ?>">&lt;</a> <?php echo $html_title; ?> <a href="?ym=<?php echo $next; ?>">&gt;</a></h3>

        <table class="table table-bordered">

            <tr>
                <th>SUN</th>
                <th>MONDAY</th>
                <th>TUESDAY</th>
                <th>WEDNESDAY</th>
                <th>THURSDAY</th>
                <th>FRIDAY</th>
                <th>SAT</th>

            </tr>

            <?php

      foreach ($weeks as $week) {

                    echo  $week;

                }

            ?>

        </table>
</div>