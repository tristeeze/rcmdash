<div style="overflow-x:auto">
<?php include("../includes/search_form.php"); ?>
<table id="leads" class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>COMPANY</th>
            <th>OWNER</th>
<!--            <th>YRS/BUS</th>-->
            <th>DATE</th>
            <th>TIME</th>
            <th>NAME</th>
            <th>MAIL</th>
            <th>NUMBER</th>
            <th>AREA</th>
<!--            <th>PROVINCE</th>-->
            <th>PROD</th>
            <th>TELES</th>
            
            <th>AUDIT</th>
            <th>FEEDBACK</th>
            <th>EDIT</th>
        </tr>
    </thead>
    <tbody id="myTable">
    
       
   <?php
   $user_id         = $_SESSION['id']; 
    
$query = "SELECT * FROM leads  ";
        
$select_leads = mysqli_query($connection,$query);
        
while($row = mysqli_fetch_assoc($select_leads)) {
   
$lead_id                    = $row['id'];
$jellisId                   = $row['jellis_id'];
$lead_owner                 = $row['isOwner'];
$lead_business_years        = $row['inBusiness'];
$lead_date                  = $row['callDate'];
$lead_time                  = $row['callTime'];
$lead_company               = $row['companyName'];
$lead_name                  = $row['name'];
$lead_email                 = $row['email'];
$lead_number                = $row['contactNumber'];
$lead_country               = $row['country'];
$lead_province              = $row['province'];
//$lead_sales                 = $row['sales'];
$lead_status                = $row['status'];
$lead_lead                  = $row['lead'];
$lead_telesales             = $row['sales'];
$lead_file                  = $row['lead_file'];
$lead_added_by              = $row['add_by'];
    
    if($lead_added_by == 'Client') {
        $color = "#c12f40";
    } else {
        $color = "#898989";
    }
    
    
    
    
    
$query = "SELECT * FROM users WHERE user_id = '$lead_telesales'  ";
        
$select_tele = mysqli_query($connection,$query);
        
while($row = mysqli_fetch_assoc($select_tele)) {
    
    $tele = $row['user_firstname'];
    
}
    
$query = "SELECT * FROM users WHERE user_id = '$lead_added_by'  ";
        
$select_tele = mysqli_query($connection,$query);
        
while($row = mysqli_fetch_assoc($select_tele)) {
    
    $added_by = $row['user_firstname'];
    
}
    
$query = "SELECT PdfUrl FROM jellisLeads WHERE id = '$jellisId'  ";
        
$select_audit = mysqli_query($connection,$query);
        
while($row = mysqli_fetch_assoc($select_audit)) {
    
    $audit = $row['PdfUrl'];
    
}
    
    
    
echo "<tr style='border-left:8px solid $color'>";
echo "<td>$lead_id</td>";
echo "<td>$lead_company</td>";
echo "<td>$lead_owner<br /> $lead_business_years years in business</td>";
//echo "<td></td>";   
echo "<td>$lead_date</td>";
echo "<td>$lead_time</td>";

echo "<td>$lead_name</td>";
echo "<td><a href='mailto:$lead_email'><i class='fas fa-envelope'></i></a></td>";
echo "<td><a href='tel:$lead_number'>$lead_number</a></td>";
echo "<td><strong>$lead_province</strong>,$lead_country</td>";
//echo "<td></td>";
echo "<td>$lead_lead</td>";
echo "<td>$tele</td>";


    
    if(!$audit) {
        echo "<td>NONE</td>";

    } else {
        echo "<td><a href='$audit' download><i class='fas fa-download'></i></a></td>";

    }
    
        if($lead_status == 'approved') {
        echo "<td>N/A</td>";
    } else {
    echo "<td><a href='leads.php?source=single_lead_feedback&p_id={$lead_id}'>View</a></td>";

    }
echo "<td><a href='leads.php?source=edit_lead&id={$lead_id}'>EDIT</a></td>";
//echo "<td><a href='leads.php?claim={$lead_id}&id={$user_id}'>Claim</a></td>";
//echo "<td><a href='leads.php?source=add_feedback&p_id={$lead_id}'>Add Feedback</a></td>";
echo "</tr>";

       }                      
 ?>
                            </tbody>
                        </table>
</div>


<?php 

if(isset($_GET['claim'])) {
    
    $the_lead_id        = $_GET['claim'];    
    $time               = $lead_time;
    $leadHour           = date('H:00:00', strtotime($time));
    $lead_sales         = $_SESSION['user_firstname'];
    $app_type           = 'call';

    
    // CHECK TO SEE IF THE SALES HAS A MEETING IN THAT TIME ALREADY
    $results_query = "SELECT app_id FROM appointments WHERE DATE('H:00:00', app_time) = '$leadHour' AND app_date='$lead_date' AND app_consultant='$lead_sales' AND app_rescheduled = 'No'  AND app_approved = 'approved' OR app_approved = 'pending'";

    $row = mysqli_query($connection, $results_query);
    $num_rows = mysqli_num_rows($row); 
    
    // CHECK TO SEE IF THE SECOND SALES HAS A MEETING IN THAT TIME ALREADY

    $results_query = "SELECT app_id FROM appointments WHERE date('H:00:00', strtotime(app_time)) = '$leadHour' AND app_date='$lead_date' AND app_2nd_sales = '$lead_sales' AND (app_rescheduled = 'No' ) AND (app_approved = '$app_approved' OR app_approved = '$pending') ";

    $row = mysqli_query($connection, $results_query);
    $num_rows2 = mysqli_num_rows($row);
    
     if(($num_rows > 0) OR ($num_rows2 > 0)) {

  	  $timeError = "Sorry... that time slot is not available"; 	

  	} else {
         
        $query = "SELECT * FROM users WHERE user_firstname = '$lead_sales'";
        $select_users = mysqli_query($connection,$query);
        while($row  = mysqli_fetch_assoc($select_users)) {
            
            $user_team  = $row['user_team'];
            $user_group = $row['user_group'];
//         
//        $query = "INSERT INTO appointments(app_business, app_area, app_country, app_name, app_number, app_email, app_consultant, app_time, app_date, app_group, app_team, app_type, lead_id)";
//
//        $query .= "VALUES('{$lead_company}','{$lead_province}','{$lead_country}','{$lead_name}','{$lead_number}','{$lead_email}','{$lead_sales}','{$lead_time}','{$lead_date}','{$user_group}','{$user_team}','{$app_type}','{$the_lead_id}')";
//
//        $create_user_query = mysqli_query($connection, $query);
//
//        confirm($create_user_query );
            
        $query = "UPDATE leads SET sales = $user_id, status = 'Claimed'  WHERE id = $the_lead_id ";
        $activate_user_query = mysqli_query($connection, $query);
        header("Location:leads.php");

        $timeError = "APPOINTMENT ADDED: " . " " . "<a href='appointments.php'>VIEW APPOINTMENTS</a>";
         
     }


     }
    
}


    

?>