<?php
if(isset($_GET['p_id'])) {
  
   $the_article_id = ($_GET['p_id']);
}


$message = 'SUBMIT ARTICLE';
if(isset($_POST['submit_article'])) {
    
    $folder_path = '../article_proofs/';
    $article_id = $the_article_id;
    $article_writer = $_SESSION['user_firstname'];
    $article_title = $_POST['article_title'];
    $article_body = $_POST['article_body'];
    $article_meta = $_POST['article_meta'];
    $article_date = date("Y/m/d");
    $article_disclaimer = 'Yes';
    $date = new DateTime(date("Y-m-d"));
    $date->modify('+7 day');
    $article_deadline = $date->format('Y-m-d');
    $article_proof = basename($_FILES['file']['name']);
    $article_proof_temp = $folder_path . $article_proof;
    
    move_uploaded_file($_FILES['file']['tmp_name'], $article_proof_temp);
    
     $query = "INSERT INTO submitted_article(article_id,article_sub_writer,article_sub_title, article_sub_body, article_sub_meta, article_sub_validation, submitted_article_disclaimer)";
    
     $query .= "VALUES('{$article_id}','{$article_writer}', '{$article_title}', '{$article_body}','{$article_meta}', '{$article_proof}', '{$article_disclaimer}')";
    
    $query .= "ON DUPLICATE KEY UPDATE article_sub_title='{$article_title}', article_sub_body='{$article_body}', article_sub_meta ='{$article_meta}', article_sub_validation = '{$article_proof}', submitted_article_disclaimer='{$article_disclaimer}'";
    

    $query3 = "UPDATE articles SET ";
    $query3 .="article_status  = 'Re-Submitted' ";
    $query3 .= "WHERE article_id = {$the_article_id} AND article_status = 'Submitted'";
        
    $query2 = "UPDATE articles SET ";
    $query2 .="article_status  = 'Submitted' ";
    $query2 .= "WHERE article_id = {$the_article_id} AND article_status = 'claimed'";    

 
        $create_article_query = mysqli_query($connection, $query); 
        $create_article_query = mysqli_query($connection, $query3); 
        $create_article_query = mysqli_query($connection, $query2); 
 
        confirm($create_article_query);
     
        $message = "ARTICLE ADDED: " . " " . "<a href='articles.php'>VIEW ARTICLES</a>";
}
 
        echo $message;
        
?>
         <script>
        $(document).ready(function(){
            $('#myForm input, select').blur(function(){
                if(!$(this).val()){
                    $(this).addClass("error");
                } else{
                    $(this).removeClass("error");
                }
            });
        });
        </script>
        
       <div class="card">
    <div class="card-content">
  
    <form id="myForm" action="" method="post" enctype="multipart/form-data">    
     
     <div class="form-group">
         <label>Article Title</label>
         <input type="text" class="form-control" name="article_title" >
         <span class="error"></span>
     </div> 
       
     <div class="form-group">
         <label>Article Body</label>
         <textarea type="text" class="form-control" name="article_body"  ></textarea>
         <span class="error"></span>
     </div> 
          <div class="form-group">
         <label>Article Meta Description</label>
         <input type="text" class="form-control" name="article_meta" >
         <span class="error"></span>
     </div> 
    <div class="form-group">
        <label>Upload Proof of Article</label>
                 <input type="file"  name="file">
        </div>         
          <div class="form-group">
        
                 <input type="checkbox" name="disclaimer" value="Yes" required> Agree to Disclaimer
        </div>         
      
<div class="form-group">
    <input class="btn btn-primary" type="submit" name="submit_article" value="Submit Article">
     </div>
</form> 
    </div>
</div>
   <script>
        CKEDITOR.replace( 'article_body' );
                </script>
                
<script type="text/javascript">
$(document).ready(function() {
    $('select').material_select();
});
</script>