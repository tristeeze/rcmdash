<?php

$timeError = "Book Appointment";

if(isset($_POST['add_appointment'])) {
    
    $app_business = $_POST['app_business'];
    $app_address = $_POST['app_address'];
    $app_area = $_POST['app_area'];
    $app_name = $_POST['app_name'];   
    $app_number = $_POST['app_number'];
    $app_alt_number = $_POST['app_alt_number'];
    $app_website = $_POST['app_website'];
    $app_email = $_POST['app_email'];
    $app_comments = $_POST['app_comments'];
    $app_competitors = $_POST['app_competitors'];
    $app_consultant = $_POST['app_consultant'];
    $app_time = $_POST['app_time'];
    $app_date = $_POST['app_date'];
    $app_telesales = $_SESSION['user_firstname'];
    $app_tele_payment = $_POST['user_payment'];
    $app_group = 'telesalesr';
   
    
     $no_go = array( "Johannesburg CBD", "Pretoria CBD", "Lenasia", "Marshalltown", "Soweto", "Tembisa", "Rosettenville", "Alexandera", 
"Diepkloof", "Mamelodi", "Daveyton", "Vosloorus", "Cosmo city", "Germiston CBD", "Krugersdorp CBD", "Eldorado Park", "Ennerdale", "
Katlahong", "Thokoza", "Hillbrow");
    
    $results_query = "SELECT app_id FROM appointments WHERE app_time = '$app_time' AND app_date='$app_date' AND app_consultant='$app_consultant' AND (app_approved = '$app_approved' OR app_approved = '$pending')  ";
    $results_query2 = "SELECT app_id FROM appointments WHERE app_business ='$app_business' AND app_number = '$app_number'  ";
    $row = mysqli_query($connection, $results_query);
    $num_rows = mysqli_num_rows($row);
    
    if($num_rows > 0) {
  	  $timeError = "Sorry... that time slot is not available"; 	
  	}   elseif(in_array($app_area, $no_go)){
    
    $timeError = "Sorry... that area is not allowed";
}
  
 else {

 
  
$query = "INSERT INTO appointments(app_pay_method, app_business, app_address, app_area, app_name, app_number, app_alt_number, app_website, app_email, app_comments, app_competitors, app_consultant, app_time, app_date, app_telesales, app_group)";

$query .= "VALUES('{$app_tele_payment}','{$app_business}', '{$app_address}', '{$app_area}', '{$app_name}', '{$app_number}','{$app_alt_number}','{$app_website}','{$app_email}','{$app_comments}','{$app_competitors}','{$app_consultant}','{$app_time}','{$app_date}','{$app_telesales}','{$app_group}')";
    
$create_user_query = mysqli_query($connection, $query);
    
confirm($create_user_query );
    
    $timeError = "APPOINTMENT ADDED: " . " " . "<a href='appointments.php'>VIEW APPOINTMENTS</a> " ;
}
    
    echo $timeError;
}

?>
   
   <script>
$(document).ready(function(){
    $('#myForm input, select').blur(function(){
        if(!$(this).val()){
            $(this).addClass("error");
        } else{
            $(this).removeClass("error");
        }
    });
});
</script>
    <p>Please read our faq document before booking an appointment</p>
    <form action="" method="post" enctype="multipart/form-data">    
     
     <div class="form-group">
         <label>Name of Business</label>
         <input type="text" class="form-control" name="app_business">
     </div>
     
        <div class="form-group">
         <label for="title">Business Address</label>
         <input type="text" class="form-control" name="app_address">
     </div>  
                
       <div class="form-group">
         <label for="post_status">Area</label>
           <input type="text" class="form-control" name="app_area">
     </div> 
                  
       <div class="form-group">
         <label for="post_status">Contact Full Name</label>
           <input type="text" class="form-control" name="app_name">
     </div> 
     <div class="form-group">
         <label for="post_status">Contact Number</label>
           <input type="text" class="form-control" name="app_number">
     </div>      
    <div class="form-group">
         <label for="post_status">Alternate Number</label>
           <input type="text" class="form-control" name="app_alt_number">
     </div> 
         <div class="form-group">
         <label for="post_status">Business Website</label>
           <input type="text" class="form-control" name="app_website">
     </div>          
        
        <div class="form-group">
         <label for="post_status">Comments</label>
           <input type="text" class="form-control" name="app_comments">
     </div> 
       <div class="form-group">
         <label for="post_status">Email</label>
           <input type="email" class="form-control" name="app_email">
     </div> 
            <div class="form-group">
         <label for="post_status">Business Competitor</label>
           <input type="text" class="form-control" name="app_competitors">
     </div> 
        <div class="form-group">
         <label for="post_status">Meeting Time</label>
           <select type="text" class="form-control" name="app_time">
<!--           <option value='9:00'>9:00</option>-->
           <option value='10:15'>10:15</option>
           <option value='11:30'>11:30</option>
           <option value='12:45'>12:45</option>
           <option value='14:00'>14:00</option>
           <option value='15:30'>15:30</option>
            </select>
     </div> 
     <div class="form-group">
         <label for="post_status">Meeting Date</label>
           <input type="date" class="form-control" name="app_date">
     </div>
     
     <div class="form-group">
        <label for="post_status">Sales Consultant</label>
        <select class='form-control' name='app_consultant' id=''>
       <?php
   
$query = "SELECT * FROM users WHERE user_group = 'Sales'";
$select_users = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_users)) {
$user_id = $row['user_id'];
$user_firstname = $row['user_firstname'];
$user_lastname = $row['user_lastname'];
$user_email = $row['user_email'];
$user_number = $row['user_number'];
$user_group = $row['user_group'];
$user_status = $row['user_status'];
$user_colour = $row['user_colour'];
    
    echo "<option value='$user_firstname'>$user_colour</option>";
  
}
    ?>
        </select>
     </div> 
      <div class="form-group">
       
            <label>Preferred Payment Method For This Appointment</label>
           <select class="form-control select" name="user_payment">
           <option value="Airtime">Airtime</option>
           <option value="Wallet">Money wallet</option>
           <option value="Transfer">Direct Bank Transfer</option>
          </select>
   
   </div>
     
<div class="form-group">
    <input class="btn btn-primary" type="submit" name="add_appointment" value="Add Appointment">
     </div>
</form>