<style>
#myInput {
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>
<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">

<div style="overflow-x:auto">
<table id="myTable" class="table table-bordered">
    <thead>
        <tr class="header">
            <th>Id</th>
            <th>Name</th>
            <th>Status</th>
            <th>Manager</th>
            <th>Address</th>
            <th>Area</th>
            <th>Full Name</th>
            <th>Phone</th>
            <th>Alt Number</th>
            <th>Website</th>
            <th>Comments</th>
            <th>Email</th>
            <th>Time</th>
            <th>Date</th>
            <th>Reschedule</th>

        </tr>

    </thead>
    <tbody>
                               
                                   
   <?php
//$Appointments = new Appointments;
//$Appointments->view_appointments();        
   
$query = "SELECT * FROM appointments WHERE app_telesales = '{$_SESSION['user_firstname']}'";
$select_appointments = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_appointments)) {
$app_id             = $row['app_id'];
$app_business       = $row['app_business'];
$app_address        = $row['app_address'];
$app_area           = $row['app_area'];
$app_name           = $row['app_name'];
$app_number         = $row['app_number'];
$app_alt_number     = $row['app_alt_number'];
$app_website        = $row['app_website'];
$app_comments       = $row['app_comments'];
$app_email          = $row['app_email'];
$app_competitors    = $row['app_competitors'];
$app_time           = $row['app_time'];
$app_date           = $row['app_date'];
$app_approved       = $row['app_approved'];
$app_comment        = $row['app_comment'];

echo "<tr>";
echo "<td>$app_id</td>";
echo "<td>$app_business</td>";
echo "<td>$app_approved</td>";
echo "<td>$app_comment</td>";
echo "<td>$app_address</td>";
echo "<td>$app_area</td>";    
echo "<td>$app_name</td>";    
echo "<td>$app_number</td>";
echo "<td>$app_alt_number</td>";
echo "<td>$app_website</td>";
echo "<td>$app_comments</td>";
echo "<td>$app_email</td>";
echo "<td>$app_time</td>";
echo "<td>$app_date</td>";
echo "<td><a href='appointments.php?source=reschedule_appointment&p_id={$app_id}'>Reschedule</a></td>";
    
echo "</tr>";

       }                        
 ?>
                            </tbody>
                        </table>
                        <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 1; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</div>