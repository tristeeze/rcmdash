<?php

if(isset($_GET['p_id'])) {
 
   $the_app_id = ($_GET['p_id']);

}

$query2 = "SELECT * FROM feedback WHERE app_id = $the_app_id ";
$select_app_by_id = mysqli_query($connection,$query2);


while($row = mysqli_fetch_assoc($select_app_by_id)) {
        $client_contract = $row['feedback_contract'];

}

$query2 = "SELECT * FROM all_clients WHERE app_id = $the_app_id" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $c_id                   = $row['c_id'];
        $client_name            = $row['c_client'];
        $client_date            = $row['c_date'];
        $client_renew           = $row['c_renew'];
        $client_renewals        = $row['c_renewals'];
        $client_duration        = $row['c_duration'];
        $client_pay_intervals   = $row['c_pay_intervals'];
        $client_method          = $row['c_method'];
        $client_payment_nr      = $row['c_payment_nr'];
        $client_location        = $row['c_location'];
        $client_sales           = $row['c_sales'];
        $client_team            = $row['c_team'];
        $client_telesales       = $row['c_telesales'];
        $client_status          = $row['c_status'];
        $client_package         = $row['c_package'];
        $client_total_ex        = $row['c_total'];
        $client_total_inc       = $row['c_total_inc'];
        $client_month_ex        = $row['c_month_ex'];
        $client_month_inc       = $row['c_month_inc'];
        $client_due             = $row['c_due'];
        $client_paid            = $row['c_paid'];
        $client_payments_left   = $row['c_payments_left'];
        $monthly_payment        = round($row['c_total'] / $row['c_pay_intervals'],2);


                     } 

$query = "SELECT * FROM appointments WHERE app_id = $the_app_id ";
$select_app_by_id = mysqli_query($connection,$query);


while($row = mysqli_fetch_assoc($select_app_by_id)) {

        $app_id         = $row['app_id'];
        $app_approved   = $row['app_approved'];
        $app_business   = $row['app_business'];
        $app_address    = $row['app_address'];
        $app_area       = $row['app_area'];
        $app_name       = $row['app_name'];
        $app_number     = $row['app_number'];
        $app_alt_number = $row['app_alt_number'];
        $app_website    = $row['app_website'];
        $app_comments   = $row['app_comments'];
        $app_email      = $row['app_email'];
        $app_telesales  = $row['app_telesales'];     
        $app_consultant = $row['app_consultant'];     

}
  $sql="SELECT sum(client_ex_vat) as total FROM clients_accounts WHERE c_id = $c_id";
  $result = mysqli_query($connection,$sql);
  $own_total = mysqli_fetch_assoc($result);  
    
     if ($own_total['total'] > 0) {
    $total_own = $own_total['total'];
}    else {
    $total_own = '0';
}

$total_outstanding = $client_total_ex - $own_total['total'];

?>

<?php echo "<div class='buttons'><a href='../contracts/$client_contract' class='btn btn-dark' download><i class='fas fa-download'></i> DOWNLOAD CONTRACT</a> <a href='clients.php?source=add_comment&p_id={$c_id}' class='btn btn-dark'><i class='fas fa-plus'></i> ADD COMMENT </a> <a class='btn btn-dark' href='clients.php?source=view_comments&p_id={$c_id}'><i class='fas fa-comments'></i> VIEW COMMENTS</a></div>"; ?>


<h3>CLIENT INFO</h3>

<table class="table table-bordered">
    <tr>
        <td><strong>APP ID</strong></td>
        <td><?php echo $c_id ?></td>
    </tr>
    <tr>
        <td><strong>SALES</strong></td>
        <td><?php echo $app_consultant ?></td>
    </tr>
    <tr>
        <td><strong>APP TELESALES</strong></td>
        <td><?php echo $app_telesales ?></td>
    </tr>
    <tr>
        <td><strong>BUSINESS NAME</strong></td>
        <td><?php echo $client_name ?></td>
    </tr>
    <tr>
        <td><strong>CLIENT NAME</strong></td>
        <td><?php echo $app_name ?></td>
    </tr>
    <tr>
        <td><strong>CLIENT CONTACT NUMBER</strong></td>
        <td><a href='tel:<?php echo $app_number ?>'><?php echo $app_number ?></a></td>
    </tr>
    <tr>
        <td><strong>CLIENT ALTERNATIVE CONTACT NUMBER</strong></td>
        <td><a href='tel:<?php echo $app_alt_number ?>'><?php echo $app_alt_number ?></a></td>
    </tr>         
    <tr>
        <td><strong>CLIENT EMAIL</strong></td>
        <td><a href='mailto:<?php echo $app_email ?>'><?php echo $app_email ?></a></td>
    </tr>
    <tr>
        <td><strong>BUSINESS WEBSITE</strong></td>
        <td><a href='<?php echo $app_website ?>' target="_blank"><?php echo $app_website ?></a></td>
    </tr>
    <tr>
        <td><strong>COMMENTS</strong></td>
        <td><?php echo $app_comments ?></td>
    </tr>    
    <tr>
        <td><strong>CONTRACT TOTAL (ex vat)</strong></td>
        <td><?php echo "R " . $client_total_ex ?></td>
    </tr>    
    <tr>
        <td><strong>TOTAL PAID TO DATE (ex vat)</strong></td>
        <td><?php echo "R " . $own_total['total']; ?></td>
    </tr>
        <tr>
        <td><strong>TOTAL OUTSTANDING (ex vat)</strong></td>
        <td><?php echo "R " . $total_outstanding ; ?></td>
    </tr>
</table>


<table id="mytable" class="table table-bordered exportable" style="">
    <thead>

        <tr>
            <th colspan="2"> <h3 class="text-center">CLIENT PACKAGES</h3></th>
        </tr>

    </thead>
    <tbody id="myTable">

     
     
   <?php 
        
if(isset($_GET['p_id'])) {
 
   $the_client_id = ($_GET['p_id']);

}
                
$query2 = "SELECT * FROM all_clients WHERE app_id = $the_client_id " ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {
    $c_id = $row['c_id'];
    
}
        
          
$query2 = "SELECT * FROM price_breakdown WHERE c_id = $c_id " ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {
    
    $client_name = $row['client_name'];
    $seo_price = $row['seo_price'];
    $display_price = $row['display_price'];
    $search_price = $row['search_price'];
    $development_price = $row['development_price'];
    $maintenance_price = $row['maintenance_price'];
    $animation_price = $row['animation_price'];
    $video_price = $row['video_price'];
    $hosting_price = $row['hosting_price'];
    $domain_price = $row['domain_price'];
    $ssl_price = $row['ssl_price'];
    $content_price = $row['content_price'];
    $newsletter_price = $row['newsletter_price'];
    $fb_social_price = $row['fb_social_price'];
    $fb_lead_price = $row['fb_lead_price'];
    $instagram_price = $row['instagram_price'];
    $pinterest_price = $row['pinterest_price'];
    $linkedin_price = $row['linkedin_price'];
    $youtube_price = $row['youtube_price'];
    $graphic_price = $row['graphic_price'];
    $ci_price = $row['ci_price'];
    $app_maint_price = $row['app_maint_price'];
    $app_dev_price = $row['app_dev_price'];
    $manage_price = $row['manage_price'];
    $mportal_price = $row['mportal_price'];
    $cars_price = $row['cars_price'];
    $petrol_price = $row['petrol_price'];
    
    
     echo   '<tr><th>ID</th><td>' . $c_id . '</td>' . '</tr>';
     echo   '<tr><th>CLIENT</th><td>' .$client_name . '</td></tr>';
     echo   '<tr><th>SEO</th><td>' . $seo_price . '</td></tr>';
     echo   '<tr><th>GOOGLE DISPLAY</th><td>' . $display_price . '</td></tr>';
     echo   '<tr><th>GOOGLE SEARCH</th><td> ' . $search_price . ' </td></tr>';
     echo   '<tr><th>WEB DEVELOPMENT</th><td> ' . $development_price . ' </td></tr>';
     echo   '<tr><th>WEB MAINTENANCE</th><td> ' . $maintenance_price . ' </td></tr>';
     echo   '<tr><th>ANIMATION</th><td> ' . $animation_price . ' </td></tr>';
     echo   '<tr><th>VIDEO</th><td> ' . $video_price . ' </td></tr>';
     echo   '<tr><th>HOSTING</th><td> ' . $hosting_price . ' </td></tr>';
     echo   '<tr><th>DOMAIN</th><td> ' . $domain_price . ' </td></tr>';
     echo   '<tr><th>SSL</th><td> ' . $ssl_price . ' </td></tr>';
     echo   '<tr><th>CONTENT</th><td> ' . $content_price . ' </td></tr>';
     echo   '<tr><th>NEWSLETTER</th><td> ' . $newsletter_price . ' </td></tr>';
     echo   '<tr><th>FB SOCIAL</th><td> ' . $fb_social_price . ' </td></tr>';
     echo   '<tr><th>FB LEAD</th><td> ' . $fb_lead_price  . ' </td></tr>';
     echo   '<tr><th>INSTAGRAM</th><td> ' . $instagram_price . ' </td></tr>';
     echo   '<tr><th>PINTEREST</th><td> ' . $pinterest_price . ' </td></tr>';
     echo   '<tr><th>LINKEDIN</th><td> ' . $linkedin_price . ' </td></tr>';
     echo   '<tr><th>YOUTUBE</th><td> ' . $youtube_price . ' </td></tr>';
     echo   '<tr><th>GRAPHICS</th><td> ' . $graphic_price . ' </td></tr>';
     echo   '<tr><th>CI</th><td> ' . $ci_price . ' </td></tr>';
     echo   '<tr><th>APP DEV</th><td> ' . $app_dev_price . ' </td></tr>';
     echo   '<tr><th>APP MAINTENANCE</th><td> ' . $app_maint_price . ' </td></tr>';
     echo   '<tr><th>MANAGEMENT</th><td> ' . $manage_price . ' </td></tr>';
     echo   '<tr><th>MARKETING PORTAL</th><td> ' . $mportal_price . ' </td></tr>';
     echo   '<tr><th>2ND HAND CARS</th><td> ' . $cars_price . ' </td></tr>';
     echo   '<tr><th>E-PETROL</th><td> ' . $petrol_price . ' </td></tr>';

    
}
    

 ?>
    </tbody>
</table>

<?php include("../includes/date_form.php"); ?>
<?php include("../includes/export_element.php"); ?>
<?php include("../includes/search_form.php"); ?>
                   
                    <table id="mytable" class="table table-bordered exportable">
    <thead>
       <tr>
           <th colspan="18"> <h3 class="text-center">CLIENT PAYMENT HISTORY</h3></th>
       </tr>
        <tr>
            <th>ID</th>
            <th>DATE PAID</th>
            <th>DEPOSIT</th>
            <th>EX VAT</th>
            <th>VAT</th>
            <th>INC VAT</th>
            <th>INVOICED</th>
            <th>REF</th>
            <th>DUE DATE</th>
            <th>COMMENT</th>
            <th>BOARD</th>
            <th>TOTAL</th>
            <th>PROOF</th>
          
        </tr>

    </thead>
    <tbody id="myTable">

   <?php

        if(isset($_POST['filter'])){
            $sale_name = $_POST['app_consultant'];
            $app_selected_status = $_POST['app_status'];
            $from_date = $_POST['from_date'];
            $to_date = $_POST['to_date'];


$query2 = "SELECT * FROM clients_accounts WHERE client_date_paid BETWEEN '$from_date' AND '$to_date' ORDER BY client_date_paid DESC" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $app_id                 = $row['client_id'];
        $client_name            = $row['client_name'];
        $client_date_paid       = $row['client_date_paid'];
        $client_deposit         = $row['client_deposit'];
        $client_ex_vat          = $row['client_ex_vat'];
        $client_vat             = $row['client_vat'];
        $client_inc_vat         = $row['client_inc_vat'];
        $client_inv             = $row['client_inv'];
        $client_due             = $row['client_due'];
        $client_os_inc_vat      = $row['client_os_inc_vat'];
        $client_comment         = $row['client_comment'];
        $client_board_figure    = $row['client_board_figure'];
        $client_paid_number     = $row['client_paid_number'];
        $client_number_inc_vat  = $row['client_number_inc_vat'];
        $client_details         = $row['client_details'];
        $client_total_paid      = $row['client_total_paid'];
        $client_proof           = $row['client_proof'];
        $debit_order            = $row['client_paid_number'];


        echo "<tr>";
        echo "<td>$app_id</td>";
        echo "<td>$client_date_paid</td>";
        echo "<td>$client_deposit</td>";
        echo "<td>$client_ex_vat</td>";
        echo "<td>$client_vat</td>";
        echo "<td>$client_inc_vat</td>";
        echo "<td>$client_inv</td>";
        echo "<td>$debit_order </td>";
        echo "<td>$client_due</td>";
        echo "<td>$client_comment</td>";
        echo "<td>$client_board_figure</td>";
        echo "<td>$client_total_paid</td>";
        echo "<td><a href='../proofs/$client_proof'><i class='fas fa-download'></i></a><img class='img-responsive' src=''></td>";


        echo "</tr>";

         }
        } else {

          
$query2 = "SELECT * FROM clients_accounts WHERE c_id = $c_id ORDER BY client_date_paid DESC" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $app_id                 = $row['client_id'];
        $client_date_paid       = $row['client_date_paid'];
        $client_deposit         = $row['client_deposit'];
        $client_ex_vat          = $row['client_ex_vat'];
        $client_vat             = $row['client_vat'];
        $client_inc_vat         = $row['client_inc_vat'];
        $client_duration        = $row['client_duration'];
        $client_method          = $row['client_method'];
        $client_inv             = $row['client_inv'];
        $client_due             = $row['client_due'];
        $client_os_inc_vat      = $row['client_os_inc_vat'];
        $client_comment         = $row['client_comment'];
        $client_board_figure    = $row['client_board_figure'];
        $client_paid_number     = $row['client_paid_number'];
        $client_number_inc_vat  = $row['client_number_inc_vat'];
        $client_location        = $row['client_location'];
        $client_sales           = $row['client_sales'];
        $client_team            = $row['client_team'];
        $client_telesales       = $row['client_telesales'];
        $client_status          = $row['client_status'];
        $client_details         = $row['client_details'];
        $client_total_paid      = $row['client_total_paid'];
        $client_proof           = $row['client_proof'];
        $debit_order            = $row['client_paid_number'];


        echo "<tr>";
        echo "<td>$app_id</td>";
        echo "<td>$client_date_paid</td>";
        echo "<td>$client_deposit</td>";
        echo "<td>$client_ex_vat</td>";
        echo "<td>$client_vat</td>";
        echo "<td>$client_inc_vat</td>";
        echo "<td>$client_inv</td>";
        echo "<td>$debit_order </td>";
        echo "<td>$client_due</td>";
        echo "<td>$client_comment</td>";
        echo "<td>$client_board_figure</td>";
        echo "<td>$client_total_paid</td>";
        echo "<td><a href='../proofs/$client_proof'><i class='fas fa-download'></i></a><img class='img-responsive' src=''></td>";
        echo "</tr>";
}
        }
                

 ?>
 

 

<table id="mytable" class="table table-bordered exportable"  >
    <thead>
       <tr>
           <th colspan="18"> <h3 class="text-center">CLIENT ADDONS</h3></th>
       </tr>
        <tr>
            <th>ID</th>
            <th>ITEM</th>
            <th>HOURS</th>
            <th>PRICE</th>
            <th>DATE</th>
            <th>DUE</th>
            <th>PAID</th>
                 
                  
        </tr>

    </thead>
    <tbody id="myTable">
    
   <?php 

              
              
        $query2 = "SELECT * FROM client_addons WHERE c_id = $c_id " ;
        $select_appointments = mysqli_query($connection,$query2);
        while($row = mysqli_fetch_assoc($select_appointments)) {

        $c_id = $row['c_id'];
        $addon_type     = $row['addon_type'];
        $addon_hours    = $row['addon_hours'];
        $addon_price    = $row['addon_price'];
        $addon_date     = $row['addon_date'];
        $addon_due      = $row['addon_due'];
        $addon_paid     = $row['addon_paid'];
    

    if ($addon_paid != 'yes') {
        $background = '#d93335';
    } else {
        $background = '#73975c';
    }

echo "<tr>";
echo "<td>$c_id</td>";
echo "<td>$addon_type</td>";
echo "<td>$addon_hours</td>";
echo "<td>$addon_price</td>";
echo "<td>$addon_date</td>";
echo "<td>$addon_due</td>";
echo "<td style='background-color:$background;'>$addon_paid</td>";
echo "</tr>";

     }
        
echo "</tbody>";
echo "</table>";
              
          
        if(isset($_GET['pay'])) {
    
    $the_app_id = $_GET['pay'];
    
    
    $query = "UPDATE client_addons SET addon_paid = 'yes' WHERE c_id = $the_app_id ";
    $send_payments = mysqli_query($connection, $query);
    confirm($send_payments );
    header("Location:clients.php?source=all_addons");
}
 ?>
 <table id="myTable" class="table table-bordered exportable">
 
  <thead>
       <tr>
           <th colspan="10"><h3 class="text-center">CROSS SELLS</h3></th>
       </tr>
        <tr>
           
            <th>ID</th>
            <th>ITEM</th>
            <th>DESCRIPTION</th>
            <th>PRICE</th>
            <th>DATE</th>
            <th>DUE</th>
            <th>PAID</th>
                 
                  
        </tr>

    </thead>
    <tbody id="myTable">

       
   <?php 
        
            $query2 = "SELECT * FROM all_clients JOIN cross_sell ON all_clients.c_id = cross_sell.c_id " ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {
    $the_client = $row['c_client'];
}   
        
           if(isset($_POST['filter'])){
            $sale_name              = $_POST['app_consultant'];
            $app_selected_status    = $_POST['app_status'];
            $from_date              = $_POST['from_date'];
            $to_date                = $_POST['to_date'];
          
$query2 = "SELECT * FROM cross_sell WHERE cross_sell_date BETWEEN '$from_date' AND '$to_date'" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $c_id               = $row['c_id'];
        $cross_type         = $row['cross_sell_type'];
        $cross_price        = $row['cross_sell_price'];
        $cross_description  = $row['cross_sell_description'];
        $cross_date         = $row['cross_sell_date'];
        $cross_due          = $row['cross_sell_due'];
        $cross_paid         = $row['cross_sell_paid'];
    

    if ($cross_paid != 'yes') {
        $background = '#d93335';
    } else {
        $background = '#73975c';
    }

echo "<tr>";
echo "<td>$c_id</td>";
echo "<td>$cross_type</td>";
echo "<td>$cross_price</td>";
echo "<td>$cross_description</td>";
echo "<td>$cross_date</td>";
echo "<td>$cross_due</td>";
echo "<td style='background-color:$background;'>$cross_paid</td>";
echo "</tr>";

     }} else {
               
            $query2 = "SELECT * FROM cross_sell" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $app_id             = $row['app_id'];
        $c_id               = $row['c_id'];
        $cross_type         = $row['cross_sell_type'];
        $cross_price        = $row['cross_sell_price'];
        $cross_description  = $row['cross_sell_description'];
        $cross_date         = $row['cross_sell_date'];
        $cross_due          = $row['cross_sell_due'];
        $cross_paid         = $row['cross_sell_paid'];
    

    if ($cross_paid != 'yes') {
        $background = '#d93335';
    } else {
        $background = '#73975c';
    }

echo "<tr>";
echo "<td>$c_id</td>";
echo "<td>$cross_type</td>";
echo "<td>$cross_price</td>";
echo "<td>$cross_description</td>";
echo "<td>$cross_date</td>";
echo "<td>$cross_due</td>";
echo "<td style='background-color:$background;'>$cross_paid</td>";
echo "</tr>";   
               
           }}

        if(isset($_GET['pay'])) {
    
    $the_app_id = $_GET['pay'];
    
    
    $query = "UPDATE cross_sell SET cross_sell_paid = 'yes' WHERE app_id = $the_app_id ";
    $send_payments = mysqli_query($connection, $query);
    confirm($send_payments );
    header("Location:clients.php?source=cross_sells");
}
 ?>
                         </tbody>
                       </table>
                       
                       <div style="overflow-x:auto">
 <table id="mytable" class="table table-bordered exportable"  >
    <thead>
       <tr>
           <th colspan="10"><h3 class="text-center">UP SELLS</h3></th>
       </tr>
        <tr>
            <th>Id</th>
            <th>Item</th>
            <th>Description</th>
            <th>Price</th>
            <th>Date</th>
            <th>Due</th>
            <th>Paid</th>
                 
                  
        </tr>

    </thead>
    <tbody id="myTable">

     
   <?php 
        
            $query2 = "SELECT * FROM all_clients JOIN up_sells ON all_clients.c_id = up_sells.c_id " ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {
    $the_client = $row['c_client'];
    $c = $row['c_id'];
}   
        
         if(isset($_POST['filter'])){
            $sale_name = $_POST['app_consultant'];
            $app_selected_status = $_POST['app_status'];
            $from_date = $_POST['from_date'];
            $to_date = $_POST['to_date'];
} 
             
           $query2 = "SELECT * FROM up_sells WHERE c_id = $c_id" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $c_id = $row['c_id'];
        $up_type = $row['up_sell_type'];
        $up_price = $row['up_sell_price'];
        $up_description = $row['up_sell_description'];
        $up_date = $row['up_sell_date'];
        $up_due = $row['up_sell_due'];
        $up_paid = $row['up_sell_paid'];
    

    if ($up_paid != 'yes') {
        $background = '#d93335';
    } else {
        $background = '#73975c';
    }

echo "<tr>";
echo "<td>$c_id</td>";
echo "<td>$up_type</td>";
echo "<td>$up_price</td>";
echo "<td>$up_description</td>";
echo "<td>$up_date</td>";
echo "<td>$up_due</td>";
echo "<td style='background-color:$background;'>$up_paid</td>";
echo "</tr>";

     }  
             
         

        
        if(isset($_GET['pay'])) {
    
    $the_app_id = $_GET['pay'];
    
    
    $query = "UPDATE up_sells SET up_sell_paid = 'yes' WHERE c_id = $the_app_id ";
    $send_payments = mysqli_query($connection, $query);
    confirm($send_payments );
    header("Location:clients.php?source=up_sells");
}
 ?>
     </tbody>
    </table>

