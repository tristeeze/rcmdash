<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
  <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.13/xlsx.full.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.js"></script>

<script>
    $(document).ready(function() {
  
  function saveScreenshot(canvas) {
    var downloadLink = document.createElement('a');
    downloadLink.download = 'download.jpg';
    canvas.toBlob(function(blob) {
      downloadLink.href = URL.createObjectURL(blob)
      downloadLink.click();
    });
  }


  $(".download-btn").on("click", function(e) {
    e.preventDefault();
    html2canvas(document.querySelector(".container-fluid"), {
        scrollX: 0,
        scrollY: 0
      }).then(function(canvas) {
        var image = canvas.toDataURL('image/jpeg');
        document.getElementById("created-element").src = image;
        $(this).attr('href', image);
        saveScreenshot(canvas);
      });
  });
});
</script>
<script>
    $(document).ready(function(){
    $(".saveAsExcel").click(function(){
        var workbook = XLSX.utils.book_new();
        
        //var worksheet_data  =  [['hello','world']];
        //var worksheet = XLSX.utils.aoa_to_sheet(worksheet_data);
      
        var worksheet_data  = document.getElementById("mytable");
        var worksheet = XLSX.utils.table_to_sheet(worksheet_data);
        
        workbook.SheetNames.push("Test");
        workbook.Sheets["Test"] = worksheet;
      
         exportExcelFile(workbook);
      
     
    });
})

function exportExcelFile(workbook) {
    return XLSX.writeFile(workbook, "bookName.xlsx");
}
</script>
<button type="button" class="btn download-btn">Download element!</button> <button type="button" class="btn saveAsExcel">Export Excel</button>  
<p>&nbsp;</p>

<img style="display:none" src="" id="created-element"/>
<input id="myInput" type="text" placeholder="Search..">
                  
                   <div style="overflow-x:auto">
                   <table id="mytable" class="table table-bordered exportable"  >
    <thead>
        <tr>
            <th>Id</th>
            <th>Rate Item</th>
            <th>Rate Price</th>
            <th>Rate Price Vat</th>
            <th>Description</th>
             <th>Edit Item</th>
                  
        </tr>

    </thead>
    <tbody id="myTable">

      <script type="text/javascript">
      var tableToExcel = (function() { 
        var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
          , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(table, name) {
          if (!table.nodeType) table = document.getElementById(table)
          var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
          window.location.href = uri + base64(format(template, ctx))
        }
      })()
      </script>

      <script src="../js/tableexport.min.js" type="text/javascript"></script>
      <script src="../js/FileSaver.min.js" type="text/javascript"></script>
     
   <?php 

          
$query2 = "SELECT * FROM ratecards ORDER BY rate_item DESC" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $rate_id = $row['rate_id'];
        $rate_item = $row['rate_item'];
        $rate_price = $row['rate_price'];
        $rate_price_vat = $row['rate_price_vat'];
        $rate_description = $row['rate_description'];

    
        

echo "<tr>";
echo "<td>$rate_id</td>";
echo "<td>$rate_item</td>";
echo "<td>$rate_price</td>";
echo "<td>$rate_price_vat</td>";
echo "<td>$rate_description</td>";
echo "<td><a href='rates.php?source=edit_rate_item&p_id={$rate_id}'><i class='fas fa-edit'></i></a></td>";
echo "</tr>";

     }
echo "</tbody>";
echo "</table>";
 ?>