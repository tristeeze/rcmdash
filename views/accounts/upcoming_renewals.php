<?php include("../includes/date_form.php"); ?>
<?php include("../includes/export_element.php"); ?>
<?php include("../includes/search_form.php"); ?>
                  
                   <div style="overflow-x:auto">
                   <table id="mytable" class="table table-bordered exportable"  >
    <thead>
        <tr>
            <th>Id</th>
            <th>Renew Client</th>
            <th>Pay</th>
            <th>Client</th>
            <th>Started</th>
            <th>Renew</th>
            <th>Renewals</th>
            <th>Sales</th>
            <th>Team</th>
            <th>Status</th>
            <th>Package</th>
<!--            <th>Payments Left</th>-->
            
                  
        </tr>

    </thead>
    <tbody id="myTable">

      <script type="text/javascript">
      var tableToExcel = (function() { 
        var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
          , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(table, name) {
          if (!table.nodeType) table = document.getElementById(table)
          var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
          window.location.href = uri + base64(format(template, ctx))
        }
      })()
      </script>

      <script src="../js/tableexport.min.js" type="text/javascript"></script>
      <script src="../js/FileSaver.min.js" type="text/javascript"></script>
     

   <?php
        
          if(isset($_POST['filter'])){
            $sale_name = $_POST['app_consultant'];
            $app_selected_status = $_POST['app_status'];
            $from_date = $_POST['from_date'];
            $to_date = $_POST['to_date'];
      
$query2 = "SELECT * FROM all_clients WHERE c_renew BETWEEN '$from_date' AND '$to_date' ORDER BY c_renew DESC" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $app_id = $row['app_id'];
        $c_id = $row['c_id'];
        $client_name = $row['c_client'];
        $client_date = $row['c_date'];
        $client_renew = $row['c_renew'];
        $client_renewals = $row['c_renewals'];
        $client_duration = $row['c_duration'];
        $client_pay_intervals = $row['c_pay_intervals'];
        $client_method = $row['c_method'];
        $client_payment_nr = $row['c_payment_nr'];
        $client_location = $row['c_location'];
        $client_sales = $row['c_sales'];
        $client_team = $row['c_team'];
        $client_telesales = $row['c_telesales'];
        $client_status = $row['c_status'];
        $client_package = $row['c_package'];
        $client_total = $row['c_total'];
        $client_due = $row['c_due'];
        $client_paid = $row['c_paid'];
        $client_payments_left = $row['c_payments_left'];
        $monthly_payment = round($row['c_total'] / $row['c_pay_intervals'],2);
    
$current_renew = date("Y-m", strtotime($client_renew));
$month = date('Y-m');
$final = date('Y-m',strtotime("+1 month"));

  

echo "<tr>";
echo "<td>$app_id</td>";
echo "<td><a href='clients.php?source=renew_client&p_id={$app_id}'><i class='fas fa-retweet'></i></a></td>";
echo "<td><a href='clients.php?source=add_payment&p_id={$app_id}'><i class='fas fa-plus'></i></a></td>";
echo "<td>$client_name</td>";
echo "<td>$client_date</td>";
echo "<td>$client_renew</td>";
echo "<td>$client_renewals</td>";
echo "<td>$client_sales</td>";
echo "<td>$client_team</td>";
echo "<td>$client_status</td>";
echo "<td><a href='clients.php?source=price_breakdown&p_id={$c_id}'>VIEW</a></td>";
echo "</tr>";

     } } else {
              
              
      $query2 = "SELECT * FROM all_clients ORDER BY c_renew DESC" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $app_id = $row['app_id'];
        $client_name = $row['c_client'];
        $c_id = $row['c_id'];
        $client_date = $row['c_date'];
        $client_renew = $row['c_renew'];
        $client_renewals = $row['c_renewals'];
        $client_duration = $row['c_duration'];
        $client_pay_intervals = $row['c_pay_intervals'];
        $client_method = $row['c_method'];
        $client_payment_nr = $row['c_payment_nr'];
        $client_location = $row['c_location'];
        $client_sales = $row['c_sales'];
        $client_team = $row['c_team'];
        $client_telesales = $row['c_telesales'];
        $client_status = $row['c_status'];
        $client_package = $row['c_package'];
        $client_total = $row['c_total'];
        $client_due = $row['c_due'];
        $client_paid = $row['c_paid'];
        $client_payments_left = $row['c_payments_left'];
        $monthly_payment = round($row['c_total'] / $row['c_pay_intervals'],2);
    
$current_renew = date("Y-m", strtotime($client_renew));
$month = date('Y-m');
$final = date('Y-m',strtotime("+1 month"));

  

echo "<tr>";
echo "<td>$app_id</td>";
echo "<td><a href='clients.php?source=renew_client&p_id={$app_id}'><i class='fas fa-retweet'></i></a></td>";
echo "<td><a href='clients.php?source=add_payment&p_id={$app_id}'><i class='fas fa-plus'></i></a></td>";
echo "<td>$client_name</td>";
echo "<td>$client_date</td>";
echo "<td>$client_renew</td>";
echo "<td>$client_renewals</td>";
echo "<td>$client_sales</td>";
echo "<td>$client_team</td>";
echo "<td>$client_status</td>";
echo "<td><a href='clients.php?source=price_breakdown&p_id={$c_id}'>VIEW</a></td>";
              
              
          }} 
echo "</tbody>";
echo "</table>";
 ?>