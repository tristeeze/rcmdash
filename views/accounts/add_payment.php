<?php

if(isset($_GET['p_id'])) {

   $the_client_id = ($_GET['p_id']);

}


$query = "SELECT * FROM all_clients WHERE c_id = $the_client_id";
$select_app_by_id = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_app_by_id)) {

        $c_id               = $row['c_id'];
        $c_client           = $row['c_client'];
        $c_duration         = $row['c_pay_intervals'];
        $c_total            = $row['c_total'];
        $monthly_payment    = round($row['c_total'] / $row['c_pay_intervals'],2);
        $vat                = 14;
        $vat_amount         = round(($vat / 100) * $monthly_payment,2);
        $ex_vat_amount      = round($monthly_payment - $vat_amount,2); 
        $client_paid_number = $row['client_paid_number'];
        $client_duration = $row['c_duration'];

    }


$timeError = "Add Payment";

if(isset($_POST['payment_client'])) {
    
        $folder_path = '../proofs/';

        $app_business = $_POST['app_business'];
        $date_paid = $_POST['date_paid'];
        $client_deposit = $_POST['client_deposit'];
        $ex_vat = $_POST['ex_vat'];
        $vat_amount = $_POST['vat_amount'];
        $inc_vat = $_POST['inc_vat'];
        $pay_duration = $_POST['pay_duration']; 
        $client_method = $_POST['client_method'];
        $date_inv = $_POST['date_inv'];
        $inv_due = $_POST['inv_due'];
        $next_due = $_POST['next_due'];
        $out_amount = $_POST['out_amount'];
        $client_comment = $_POST['client_comment'];
        $client_board_figure = $_POST['client_board_figure'];
        $payment_nr = $_POST['payment_nr'];
        $client_amount_paid = $_POST['client_amount_paid'];
        $client_area = $_POST['client_area'];
        $client_sales = $_POST['client_sales'];
        $client_telesales = $_POST['client_telesales'];
        $client_status = $_POST['client_status'];
        $client_details = $_POST['client_details'];
        $client_total = $_POST['client_total'];

        $processed_proof = basename($_FILES['file']['name']);
        $post_proof_temp = $folder_path . $processed_proof;

        move_uploaded_file($_FILES['file']['tmp_name'], $post_proof_temp);


        $query = "INSERT INTO clients_accounts(c_id, client_name, client_date_paid,client_deposit, client_ex_vat, client_vat, client_inc_vat,  client_inv, client_due, client_comment, client_board_figure, client_paid_number, client_total_paid, client_next_payment, client_proof)";

        $query .= "VALUES('{$c_id}','{$app_business}', '{$date_paid}', '{$client_deposit}', '{$ex_vat}', '{$vat_amount}','{$inc_vat}', '{$date_inv}','{$inv_due}','{$client_comment}','{$client_board_figure}','{$payment_nr}','{$client_total}','{$next_due}','{$processed_proof}')";

        $add_payment_query = mysqli_query($connection, $query);

        confirm($add_payment_query);

        $timeError = "CLIENT ADDED: " . " " . "<a href='appointments.php?source=processed_deals'>VIEW CLIENTS</a>";

}
    
        echo $timeError;
?>

        <script>

        $(document).ready(function(){

            $('#myForm input, select').blur(function(){

                if(!$(this).val()){

                    $(this).addClass("error");

                } else{

                    $(this).removeClass("error");

                }

            });

        });

        </script>

<div class="card">

    <div class="card-content">

    <form id="myForm" action="" method="post" enctype="multipart/form-data">    

     <div class="form-group">

         <label>Name of Business</label>

        <?php echo "<input type='text' class='form-control' name='app_business' value='$c_client' required>" ?>

         <span class="error"></span>

     </div>

          <div class="form-group">

         <label for="post_status">Date Paid</label>

        <?php echo  " <input type='date' class='form-control' name='date_paid' required>" ?>

     </div>
        
        <div class="form-group">

         <label for="title">Depsit Amount</label>
          <?php echo  " <input type='number' class='form-control' name='client_deposit' step='.01' required>" ?>

     </div>   

        <div class="form-group">

         <label for="title">Amount Due Ex Vat</label>
          <?php echo  " <input type='number' class='form-control' name='ex_vat'  step='.01' required>" ?>

     </div>        

       <div class="form-group">

         <label for="post_status">Vat Amount Due</label>
           <?php echo  " <input type='number' class='form-control' name='vat_amount'  step='.01' required>" ?> 
     </div> 

                     <div class="form-group">
         <label for="post_status">Amount Inc Vat Due</label>
          <?php echo  " <input type='number' class='form-control' name='inc_vat' step='.01' required>" ?>
      
     </div> 
         <div class="form-group">
         <label for="post_status">Client Total Paid</label>
          
          
          <?php echo "<input type='text' class='form-control' name='client_total' value='0' >" ?>
         
     </div>

  
     <div class="form-group">

         <label for="post_status">Payment Method</label>
           <input type="text" class="form-control" name="pay_method" value="INV" required>
     </div>      
    <div class="form-group">

         <label for="post_status">Client Invoiced Date</label>
            <input type="date" class="form-control" name="date_inv" required>
     </div> 

             <div class="form-group">

         <label for="post_status">Client Invoiced Due</label>
            <input type="date" class="form-control" name="inv_due" required>
     </div> 
         <div class="form-group">

         <label for="post_status">Next Payment Due</label>
            <input type="date" class="form-control" name="next_due" required>
     </div> 
        

            <div class="form-group">
         <label for="post_status">Client Comment</label>
           <textarea type="text" class="form-control" name="client_comment"></textarea>

     </div> 

          <div class="form-group">
         <label for="post_status">Client Board Figure</label>
           <input type="text" class="form-control" name="client_board_figure" value="0">
     </div> 
        <div class="form-group">
         <label for="post_status">Debit Order Ref</label>
         <?php  echo "<input type='number' class='form-control' name='payment_nr' value='$paymnt_nr'>" ?>
         
     </div> 
    
       <div class="form-group">

        <label>Upload Proof of Payment</label>

                 <input type="file"  name="file">

        </div>
      
<div class="form-group">

    <input class="btn btn-primary" type="submit" name="payment_client" value="Add Payment">

     </div>

</form> 

    </div>

</div>

<script type="text/javascript">

$(document).ready(function() {

    $('select').material_select();

});

</script>