<?php include("../includes/date_form.php"); ?>
<?php include("../includes/export_element.php"); ?>
<?php include("../includes/search_form.php"); ?>

<div style="overflow-x:auto">
                    <table id="mytable" class="table table-bordered exportable">
    <thead>
        <tr>
            <th>Id</th>
            <th>Client</th>
            <th>Date Paid</th>
            <th>Deposit</th>
            <th>Ex Vat</th>
            <th>Vat</th>
            <th>Inc Vat</th>
            <th>Invoiced</th>
            <th>Debit Order Ref</th>
            <th>Due</th>
            <th>Outs inc Vat</th>
            <th>Comment</th>
            <th>Board Fig</th>
            <th>Inc Vat</th>
            <th>Total</th>
            <th>Proof</th>
          
        </tr>

    </thead>
    <tbody id="myTable">

   <?php

        if(isset($_POST['filter'])){
            $sale_name = $_POST['app_consultant'];
            $app_selected_status = $_POST['app_status'];
            $from_date = $_POST['from_date'];
            $to_date = $_POST['to_date'];


$query2 = "SELECT * FROM clients_accounts WHERE client_date_paid BETWEEN '$from_date' AND '$to_date' ORDER BY client_date_paid DESC" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $app_id                 = $row['client_id'];
        $client_name            = $row['client_name'];
        $client_date_paid       = $row['client_date_paid'];
        $client_deposit         = $row['client_deposit'];
        $client_ex_vat          = $row['client_ex_vat'];
        $client_vat             = $row['client_vat'];
        $client_inc_vat         = $row['client_inc_vat'];
        $client_method          = $row['client_method'];
        $client_inv             = $row['client_inv'];
        $client_due             = $row['client_due'];
        $client_os_inc_vat      = $row['client_os_inc_vat'];
        $client_comment         = $row['client_comment'];
        $client_board_figure    = $row['client_board_figure'];
        $client_paid_number     = $row['client_paid_number'];
        $client_number_inc_vat  = $row['client_number_inc_vat'];
        $client_total_paid      = $row['client_total_paid'];
        $client_proof           = $row['client_proof'];
        $debit_order            = $row['client_paid_number'];


        echo "<tr>";
        echo "<td>$app_id</td>";
        echo "<td>$client_name</td>";
        echo "<td>$client_date_paid</td>";
        echo "<td>$client_deposit</td>";
        echo "<td>$client_ex_vat</td>";
        echo "<td>$client_vat</td>";
        echo "<td>$client_inc_vat</td>";
        echo "<td>$client_inv</td>";
        echo "<td>$debit_order </td>";
        echo "<td>$client_due</td>";
        echo "<td>$client_os_inc_vat</td>";
        echo "<td>$client_comment</td>";
        echo "<td>$client_board_figure</td>";
        echo "<td>$client_number_inc_vat</td>";
        echo "<td>$client_total_paid</td>";
            echo "<td><a href='../proofs/$client_proof'><i class='fas fa-download'></i></a><img class='img-responsive' src=''></td>";


        echo "</tr>";

         }
        } else {

          
$query2 = "SELECT * FROM clients_accounts ORDER BY client_date_paid DESC" ;
$select_appointments = mysqli_query($connection,$query2);
while($row = mysqli_fetch_assoc($select_appointments)) {

        $app_id                     = $row['client_id'];
        $client_name                = $row['client_name'];
        $client_date_paid           = $row['client_date_paid'];
        $client_deposit             = $row['client_deposit'];
        $client_ex_vat              = $row['client_ex_vat'];
        $client_vat                 = $row['client_vat'];
        $client_inc_vat             = $row['client_inc_vat'];
        $client_duration            = $row['client_duration'];
        $client_method              = $row['client_method'];
        $client_inv                 = $row['client_inv'];
        $client_due                 = $row['client_due'];
        $client_os_inc_vat          = $row['client_os_inc_vat'];
        $client_comment             = $row['client_comment'];
        $client_board_figure        = $row['client_board_figure'];
        $client_paid_number         = $row['client_paid_number'];
        $client_number_inc_vat      = $row['client_number_inc_vat'];
        $client_location            = $row['client_location'];
        $client_sales               = $row['client_sales'];
        $client_team                = $row['client_team'];
        $client_telesales           = $row['client_telesales'];
        $client_status              = $row['client_status'];
        $client_details             = $row['client_details'];
        $client_total_paid          = $row['client_total_paid'];
        $client_proof               = $row['client_proof'];
        $debit_order                = $row['client_paid_number'];


        echo "<tr>";
        echo "<td>$app_id</td>";
        echo "<td>$client_name</td>";
        echo "<td>$client_date_paid</td>";
        echo "<td>$client_deposit</td>";
        echo "<td>$client_ex_vat</td>";
        echo "<td>$client_vat</td>";
        echo "<td>$client_inc_vat</td>";
        echo "<td>$client_inv</td>";
        echo "<td>$debit_order </td>";
        echo "<td>$client_due</td>";
        echo "<td>$client_os_inc_vat</td>";
        echo "<td>$client_comment</td>";
        echo "<td>$client_board_figure</td>";
        echo "<td>$client_number_inc_vat</td>";
        echo "<td>$client_total_paid</td>";
        echo "<td><a href='../proofs/$client_proof'><i class='fas fa-download'></i></a><img class='img-responsive' src=''></td>";
        echo "</tr>";
}
        }

 ?>