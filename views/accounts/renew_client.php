<?php

if(isset($_GET['p_id'])) {

   $the_app_id = ($_GET['p_id']);

}

$query = "SELECT * FROM appointments WHERE app_id = $the_app_id ";
$select_app_by_id = mysqli_query($connection,$query);

while($row = mysqli_fetch_assoc($select_app_by_id)) {

        $app_id                 = $row['app_id'];
        $app_business           = $row['app_business'];
        $app_consultant         = $row['app_consultant'];
        $app_address            = $row['app_address'];
        $app_area               = $row['app_area'];
        $app_name               = $row['app_name'];
        $app_status             = $row['app_status'];
        $app_number             = $row['app_number'];
        $app_alt_number         = $row['app_alt_number'];
        $app_website            = $row['app_website'];
        $app_comments           = $row['app_comments'];
        $app_email              = $row['app_email'];
        $app_competitors        = $row['app_competitors'];
        $app_rescheduled        = $row['app_rescheduled'];
        $app_time               = $row['app_time'];
        $app_old_date           = $row['app_date'];
        $app_telesales          = $row['app_telesales'];
        $app_who_rescheduled    = $row['app_who_rescheduled'];

}

$query = "SELECT * FROM all_clients WHERE app_id = $the_app_id ";
$select_app_by_id = mysqli_query($connection,$query);

while($row = mysqli_fetch_assoc($select_app_by_id)) {

        $app_id                 = $row['app_id'];
        $c_id                   = $row['c_id'];
        $client_name            = $row['c_client'];
        $client_date            = $row['c_date'];
        $client_renew           = $row['c_renew'];
        $client_renewals        = $row['c_renewals'];
        $client_duration        = $row['c_duration'];
        $client_pay_intervals   = $row['c_pay_intervals'];
        $client_method          = $row['c_method'];
        $client_payment_nr      = $row['c_payment_nr'];
        $client_location        = $row['c_location'];
        $client_sales           = $row['c_sales'];
        $client_team            = $row['c_team'];
        $client_telesales       = $row['c_telesales'];
        $client_status          = $row['c_status'];
        $client_package         = $row['c_package'];
        $client_total           = $row['c_total'];
        $client_due             = $row['c_due'];
        $client_paid            = $row['c_paid'];
        $newClientDate          = date("Y-m-d", strtotime($client_date));
        $newRenewDate           = date("Y-m-d", strtotime($client_renew));

}


//$query = "SELECT * FROM processed_deals WHERE app_id = $the_app_id ";
//$select_app_by_id = mysqli_query($connection,$query);
//
//while($row = mysqli_fetch_assoc($select_app_by_id)) {
//
//        $app_id            = $row['app_id'];
//        $process_value     = $row['process_value'];
//
//
//}


$timeError = "Renew Client";

if(isset($_POST['renew_client'])) {

    $date_signed        = $_POST['date_signed'];
    $date_renewed       = $_POST['date_renewed'];
    $renew_number       = $_POST['renewal_num'];
    $pay_duration       = $_POST['pay_duration']; 
    $c_pay_intervals    = $_POST['c_pay_intervals']; 
    $pay_method         = $_POST['pay_method'];
    $debit_date         = $_POST['debit_date'];
    $payment_nr         = $_POST['payment_nr'];
    $client_area        = $_POST['client_area'];
    $client_status      = $_POST['client_status'];
    $client_details     = $_POST['client_details'];
    $client_total_ex    = $_POST['client_total_ex'];
    $client_total_inc   = $_POST['client_total_inc'];
    $client_month_ex    = $_POST['client_month_ex'];
    $client_month_inc   = $_POST['client_month_inc'];
    
    // PACKAGES
    
    $seo_price          = $_POST['seo_price'];
    $display_price      = $_POST['display_price'];
    $search_price       = $_POST['search_price'];
    $development_price  = $_POST['development_price'];
    $maintenance_price  = $_POST['maintenance_price'];
    $animation_price    = $_POST['animation_price'];
    $video_price        = $_POST['video_price'];
    $hosting_price      = $_POST['hosting_price'];
    $domain_price       = $_POST['domain_price'];
    $ssl_price          = $_POST['ssl_price'];
    $content_price      = $_POST['content_price'];
    $newsletter_price   = $_POST['newsletter_price'];
    $fb_social_price    = $_POST['fb_social_price'];
    $fb_lead_price      = $_POST['fb_lead_price'];
    $instagram_price    = $_POST['instagram_price'];
    $pinterest_price    = $_POST['pinterest_price'];
    $linkedin_price     = $_POST['linkedin_price'];
    $youtube_price      = $_POST['youtube_price'];
    $graphic_price      = $_POST['graphic_price'];
    $ci_price           = $_POST['ci_price'];
    $app_dev_price      = $_POST['app_dev_price'];
    $app_maint_price    = $_POST['app_maint_price'];
    $manage_price       = $_POST['manage_price'];
    $mportal_price      = $_POST['mportal_price'];
    $cars_price         = $_POST['cars_price'];
    $petrol_price       = $_POST['petrol_price'];
    
    $query = "UPDATE all_clients SET ";
    $query .="c_date            = '{$date_signed}', ";
    $query .="c_renew           = '{$date_renewed}', ";
    $query .="c_renewals        = '{$renew_number}', ";
    $query .="c_duration        = '{$pay_duration}', ";
    $query .="c_pay_intervals   = '{$c_pay_intervals}', ";
    $query .="c_method          = '{$pay_method}', ";
    $query .="c_debit_date      = '{$debit_date}', ";
    $query .="c_status          = '{$client_status}', ";
    $query .="c_package         = '{$client_details}', ";
    $query .="c_total           = '{$client_total}', ";
    $query .="c_total_inc       = '{$client_total_inc}', ";
    $query .="c_month_ex        = '{$client_month_ex}', ";
    $query .="c_month_inc       = '{$client_month_inc}' ";
    $query .= "WHERE app_id     = {$the_app_id} ";
    
    // UPDATE PACKAGES
    
    $query  = "UPDATE price_breakdown SET ";
    $query .="seo_price            = '{$seo_price}', ";
    $query .="display_price        = '{$display_price}', ";
    $query .="search_price         = '{$search_price}', ";
    $query .="development_price    = '{$development_price}', ";
    $query .="maintenance_price    = '{$maintenance_price}', ";
    $query .="animation_price      = '{$animation_price}', ";
    $query .="video_price          = '{$video_price}', ";
    $query .="hosting_price        = '{$hosting_price}', ";
    $query .="domain_price         = '{$domain_price}', ";
    $query .="ssl_price            = '{$ssl_price}', ";
    $query .="content_price        = '{$content_price}', ";
    $query .="newsletter_price     = '{$newsletter_price}', ";
    $query .="fb_lead_price        = '{$fb_social_price}', ";
    $query .="instagram_price      = '{$instagram_price}', ";
    $query .="pinterest_price      = '{$pinterest_price}', ";
    $query .="linkedin_price       = '{$linkedin_price}', ";
    $query .="youtube_price        = '{$youtube_price}', ";
    $query .="graphic_price        = '{$graphic_price}', ";
    $query .="ci_price             = '{$ci_price}', ";
    $query .="app_dev_price        = '{$app_dev_price}', ";
    $query .="app_maint_price      = '{$app_maint_price}', ";
    $query .="manage_price         = '{$manage_price}', ";
    $query .="mportal_price        = '{$mportal_price}', ";
    $query .="cars_price           = '{$cars_price}', ";
    $query .="petrol_price         = '{$petrol_price}' ";
    $query .= "WHERE c_id          = {$c_id} ";

    $update_app = mysqli_query($connection,$query);
//    $update_app = mysqli_query($connection, $query3);
    
    confirm($update_app); 
    
        $timeError = "CLIENT RENEWED: " . " " . "<a href='clients.php?source=view_clients'>VIEW CLIENTS</a>";

}

        echo $timeError;
?>

        <script>

        $(document).ready(function(){

            $('#myForm input, select').blur(function(){

                if(!$(this).val()){

                    $(this).addClass("error");

                } else{

                    $(this).removeClass("error");

                }

            });

        });

        </script>

<div class="card">

    <div class="card-content">

    <form id="myForm" action="" method="post" enctype="multipart/form-data">    

     <div class="form-group">

         <label>Name of Business</label>

        <?php echo "<input type='text' class='form-control' name='app_business' value='$app_business' required>" ?>

         <span class="error"></span>

     </div>
          <div class="form-group">

         <label for="post_status">Date Signed</label>

        <?php echo  " <input type='date' class='form-control' name='date_signed' value='$newClientDate' required>" ?>

     </div>
           <div class="form-group">

         <label for="post_status">Next Renewal</label>

        <?php echo  " <input type='date' class='form-control' name='date_renewed' value='$newRenewDate'  required>" ?>

     </div>  
         
         <div class="form-group">

         <label for="post_status">Renewal Number</label>

        <?php echo  " <input type='text' class='form-control' name='renewal_num' value='$client_renewals'  required>" ?>

     </div>
     
          

       <div class="form-group">
         <label for="post_status">Payment Duration</label>
         <select class="form-control" name="pay_duration">
           <option value="1">1 Month</option>
           <option value="2">2 Month</option>
           <option value="3">3 Month</option>
           <option value="4">4 Month</option>
           <option value="5">5 Month</option>
           <option value="6">6 Month</option>
           <option value="7">7 Month</option>
           <option value="8">8 Month</option>
           <option value="9">9 Month</option>
           <option value="10">10 Month</option>
           <option value="11">11 Month</option>
           <option value="12">12 Month</option>
           <option value="Monthly">Month 2 Month</option>
         </select>
           
     </div>  <div class="form-group">
         <label for="post_status">Payment Intervals</label>
         <select class="form-control" name="c_pay_intervals">
           <option value="1">1 Month</option>
           <option value="2">2 Month</option>
           <option value="3">3 Month</option>
           <option value="4">4 Month</option>
           <option value="5">5 Month</option>
           <option value="6">6 Month</option>
           <option value="7">7 Month</option>
           <option value="8">8 Month</option>
           <option value="9">9 Month</option>
           <option value="10">10 Month</option>
           <option value="11">11 Month</option>
           <option value="12">12 Month</option>
           <option value="1">Month 2 Month</option>
         </select>
           
     </div> 
        <script type="text/javascript">
        
    function displayQuestion(answer) {
        
            document.getElementById('noQuestion').style.display = "block";
            if (answer == "Debit Order") { // hide the div that is not selected
            document.getElementById('noQuestion').style.display = "block";
            } else  {
            document.getElementById('noQuestion').style.display = "none";
                
            }

}    
    </script>
      <div class="form-group">

         <label for="post_status">Payment Method</label>
          <select name="pay_method" class="form-control" onchange="displayQuestion(this.value)" >
           <option value="EFT">EFT</option>
          <option value="Debit Order">Debit Order</option>
           <option value="Cash">Cash</option>
           </select>
     </div>     
       <div class="form-group" id="noQuestion" style="display:none;"><br/>
     <label for="post_status">Debit Order Date</label>
        <?php echo  " <input type='date' class='form-control' name='debit_date' >" ?>

  </div>    
  

             <div class="form-group">
         <label for="post_status">Cape Town or JHB Client</label>
         <select name="client_area" class="form-control">
           
            <option value="JHB">Johannesburg</option>
            <option value="CT">Cape Town</option>
         </select>
          
     </div> 


           

        <div class="form-group">
         <label for="post_status">Client Status</label>
            <?php echo "<input type='text' class='form-control' name='client_status' value='$app_status' >" ?>
     </div>
    
     <div class="form-group">
         <label for="post_status">Client Package Details</label><br />
         
    
<input type="checkbox" id="checkSEO" name="client_seo"  value="SEO" onclick="myFunction()"> SEO<br> 

<div class="form-group" id="seo" style="display:none">
<label   for="post_status">SEO PRICE</label><br />
<input class='form-control' name='seo_price' type="text"  >
</div>

<input type="checkbox" id="checkDisplay" name="client_display" value="Google Display" onclick="myFunction()"> Google Display<br> 

<div class="form-group" id="display" style="display:none">
<label for="post_status">Google Display Price</label><br />
<input  class='form-control' type="text" name='display_price' >
</div>

<input type="checkbox" id="checkSearch" name="client_search" value="Google Search" onclick="myFunction()"> Google Search<br>

<div class="form-group" id="gsearch" style="display:none">
<label   for="post_status">Google Search Price</label><br />
<input class='form-control' type="text" name='search_price'  >
</div>


<input type="checkbox" id="checkDev" name="client_dev" value="Web Development" onclick="myFunction()"> Web Development<br>

<div class="form-group" id="webdev" style="display:none">
<label   for="post_status">Web Development Price</label><br />
<input class='form-control' type="text" name='development_price' >
</div>

<input type="checkbox" id="checkMaint" name="client_maint" value="Web Maintenance" onclick="myFunction()"> Web Maintenance<br>

<div class="form-group" id="webmaint" style="display:none">
<label   for="post_status">Web Maintenance Price</label><br />
<input class='form-control' type="text" name='maintenance_price' >
</div>


<input type="checkbox" id="checkAnimation" name="client_anim" value="Web Animation" onclick="myFunction()"> Animation<br>

<div class="form-group" id="animation" style="display:none">
<label   for="post_status">Animation Price</label><br />
<input class='form-control' type="text"  name='animation_price'>
</div>

<input type="checkbox" id="checkVideo" name="client_video" value="Video Production" onclick="myFunction()"> Video Production<br>

<div class="form-group" id="video" style="display:none">
<label   for="post_status">Video Production Price</label><br />
<input class='form-control' type="text"  name='video_price'>
</div>

<input type="checkbox" id="checkHosting" name="client_hosting" value="Hosting" onclick="myFunction()"> Hosting<br>

<div class="form-group" id="hosting" style="display:none">
<label   for="post_status">Hosting Price</label><br />
<input class='form-control' type="text" name='hosting_price' >
</div>

<input type="checkbox" id="checkDomain" name="client_domain" value="Domain Registration" onclick="myFunction()"> Domain Registration<br>

<div class="form-group" id="domain" style="display:none">
<label   for="post_status">Domain Registration Price</label><br />
<input class='form-control' type="text"  name='domain_price'>
</div>

<input type="checkbox" id="checkSsl" name="client_ssl" value="Domain SSL" onclick="myFunction()"> SSL<br>

<div class="form-group" id="ssl" style="display:none">
<label   for="post_status">SSL Price</label><br />
<input class='form-control' type="text" name='ssl_price' >
</div>

<input type="checkbox" id="checkContent" name="client_content" value="Content Creation" onclick="myFunction()"> Content Creation<br>

<div class="form-group" id="content" style="display:none">
<label   for="post_status">Content Creation Price</label><br />
<input class='form-control' type="text"  name='content_price'>
</div>


<input type="checkbox" id="checkNews" name="client_news" value="Newsletters" onclick="myFunction()"> Newsletters<br>

<div class="form-group" id="newsletter" style="display:none">
<label   for="post_status">Newsletters Price</label><br />
<input class='form-control' type="text"  name='newsletter_price'>
</div>


<input type="checkbox" id="checkFbSocial" name="client_fb_social" value="Facebook Social" onclick="myFunction()"> Facebook Social<br>

<div class="form-group" id="fsocial" style="display:none">
<label for="post_status">Facebook Social Price</label><br />
<input class='form-control' type="text" name='fb_social_price' >
</div>

<input type="checkbox" id="checkFbLead" name="client_fb_lead" value="Facebook Lead Gen" onclick="myFunction()"> Facebook Lead Gen<br>

<div class="form-group" id="flead" style="display:none">
<label for="post_status">Facebook Lead Gen Price</label><br />
<input class='form-control' type="text"  name='fb_lead_price'>
</div>

<input type="checkbox" id="checkInsta" name="client_insta" value="Instagram" onclick="myFunction()"> Instagram<br>

<div class="form-group" id="instagram" style="display:none">
<label for="post_status">Instagram Price</label><br />
<input class='form-control' type="text"  name='instagram_price'>
</div>

<input type="checkbox" id="checkPint" name="client_pint" value="Pinterest" onclick="myFunction()"> Pinterest<br>

<div class="form-group" id="pinterest" style="display:none">
<label for="post_status">Pinterest Price</label><br />
<input class='form-control' type="text" name='pinterest_price' >
</div>

<input type="checkbox" id="checkLinkd" name="client_linkd" value="LinkedIn" onclick="myFunction()"> LinkedIn<br>

<div class="form-group" id="linkedin" style="display:none">
<label for="post_status">LinkedIn Price</label><br />
<input class='form-control' type="text" name='linkedin_price' >
</div>

<input type="checkbox" id="checkYt" name="client_yt" value="YouTube Marketing" onclick="myFunction()"> YouTube Marketing<br>

<div class="form-group" id="ytmarketing" style="display:none">
<label for="post_status">YouTube Marketing Price</label><br />
<input class='form-control' type="text" name='youtube_price' >
</div>

<input type="checkbox" id="checkGraphic" name="client_graphic" value="Graphic Design" onclick="myFunction()"> Graphic Design<br>

<div class="form-group" id="graphic" style="display:none">
<label for="post_status">Graphic Design Price</label><br />
<input class='form-control' type="text"  name='graphic_price'>
</div>

<input type="checkbox" id="checkCorp" name="client_corp" value="Graphic Design" onclick="myFunction()"> Corporate Identity<br>

<div class="form-group" id="corp" style="display:none">
<label for="post_status">Corporate Identity Price</label><br />
<input class='form-control' type="text"  name='ci_price'>
</div>


<input type="checkbox" id="checkAppDev" name="client_appdev" value="Application Development" onclick="myFunction()"> Application Development<br>

<div class="form-group" id="appdev" style="display:none">
<label for="post_status">Application Development Price</label><br />
<input class='form-control' type="text" name='app_dev_price' >
</div>

<input type="checkbox" id="checkAppM" name="client_appm" value="Application Maintenance" onclick="myFunction()"> Application Maintenance<br>

<div class="form-group" id="appmaint" style="display:none">
<label for="post_status">Application Maintenance Price</label><br />
<input class='form-control' type="text" name='app_maint_price' >
</div>

<input type="checkbox" id="checkManagement" name="client_management" value="Management" onclick="myFunction()"> Management<br>

<div class="form-group" id="management" style="display:none">
<label for="post_status">Management Price</label><br />
<input class='form-control' type="text"  name='manage_price'>
</div>

<input type="checkbox" id="checkRetainer" name="client_retainer" value="Retainer" onclick="myFunction()"> Retainer<br>

<div class="form-group" id="retainer" style="display:none">
<label for="post_status">Retainer Price</label><br />
<input class='form-control' type="text" name='retainer_price' >
</div>

<input type="checkbox" id="checkMportal" name="client_mportal" value="Marketing Portal" onclick="myFunction()"> Marketing Portal<br>

<div class="form-group" id="marketingp" style="display:none">
<label for="post_status">Marketing Portal Price</label><br />
<input class='form-control' type="text"  name='mportal_price'>
</div>

<input type="checkbox" id="checkCars" name="client_cars" value="Secondhand Cars" onclick="myFunction()"> Secondhand Cars<br>

<div class="form-group" id="cars" style="display:none">
<label for="post_status">Secondhand Cars Price</label><br />
<input class='form-control' type="text"  name='cars_price'>
</div>

<input type="checkbox" id="checkPetrol" name="client_petrol" value="E-petrol" onclick="myFunction()"> E-petrol<br>

<div class="form-group" id="price" style="display:none">
<label for="post_status">E-petrol Price</label><br />
<input class='form-control' type="text" name='petrol_price' >
</div>

        <script>
function myFunction() {
  var checkBox = document.getElementById("checkSEO");
  var checkBoxDisplay = document.getElementById("checkDisplay");
  var checkBoxSearch = document.getElementById("checkSearch");
  var checkBoxDev = document.getElementById("checkDev");
  var checkBoxMaint = document.getElementById("checkMaint");
  var checkBoxAnimation = document.getElementById("checkAnimation");
  var checkBoxVideo = document.getElementById("checkVideo");
  var checkBoxHosting = document.getElementById("checkHosting");
  var checkBoxDomain = document.getElementById("checkDomain");
  var checkBoxSsl = document.getElementById("checkSsl");
  var checkBoxContent = document.getElementById("checkContent");
  var checkBoxNews = document.getElementById("checkNews");
  var checkBoxFbSocial = document.getElementById("checkFbSocial");
  var checkBoxFbLead = document.getElementById("checkFbLead");
  var checkBoxInsta = document.getElementById("checkInsta");
  var checkBoxPint = document.getElementById("checkPint");
  var checkBoxLinkd = document.getElementById("checkLinkd");
  var checkBoxYt = document.getElementById("checkYt");
  var checkBoxGraphic = document.getElementById("checkGraphic");
  var checkBoxCorp = document.getElementById("checkCorp");
  var checkBoxAppDev = document.getElementById("checkAppDev");
  var checkBoxAppM = document.getElementById("checkAppM");
  var checkBoxManagement = document.getElementById("checkManagement");
  var checkBoxRetainer = document.getElementById("checkRetainer");
  var checkBoxMportal = document.getElementById("checkMportal");
  var checkBoxCars = document.getElementById("checkCars");
  var checkBoxPetrol = document.getElementById("checkPetrol");

  var seo = document.getElementById("seo");
  var display = document.getElementById("display");
  var gsearch = document.getElementById("gsearch");
  var webdev = document.getElementById("webdev");
  var webmaint = document.getElementById("webmaint");
  var animation = document.getElementById("animation");
  var video = document.getElementById("video");
  var hosting = document.getElementById("hosting");
  var domain = document.getElementById("domain");
  var ssl = document.getElementById("ssl");
  var content = document.getElementById("content");
  var newsletter = document.getElementById("newsletter");
  var fsocial = document.getElementById("fsocial");
  var flead = document.getElementById("flead");
  var instagram = document.getElementById("instagram");
  var pinterest = document.getElementById("pinterest");
  var linkedin = document.getElementById("linkedin");
  var ytmarketing = document.getElementById("ytmarketing");
  var graphic = document.getElementById("graphic");
  var corp = document.getElementById("corp");
  var appdev = document.getElementById("appdev");
  var appmaint = document.getElementById("appmaint");
  var management = document.getElementById("management");
  var retainer = document.getElementById("retainer");
  var marketingp = document.getElementById("marketingp");
  var cars = document.getElementById("cars");
  var petrol = document.getElementById("petrol");
  
  if (checkBox.checked == true){
    seo.style.display = "block";
  } else {
     seo.style.display = "none";
  }
  
   
  if (checkBoxDisplay.checked == true){
    display.style.display = "block";
  } else {
     display.style.display = "none";
  } 
  
    if (checkBoxSearch.checked == true){
    gsearch.style.display = "block";
  } else {
     gsearch.style.display = "none";
  } 
  
    
    if (checkBoxDev.checked == true){
    webdev.style.display = "block";
  } else {
     webdev.style.display = "none";
  } 
  
      
    if (checkBoxMaint.checked == true){
    webmaint.style.display = "block";
  } else {
     webmaint.style.display = "none";
  } 
  
      if (checkBoxAnimation.checked == true){
    animation.style.display = "block";
  } else {
     animation.style.display = "none";
  } 
        if (checkBoxVideo.checked == true){
    video.style.display = "block";
  } else {
     video.style.display = "none";
  } 
  
          if (checkBoxHosting.checked == true){
    hosting.style.display = "block";
  } else {
     hosting.style.display = "none";
  } 
            if (checkBoxDomain.checked == true){
    domain.style.display = "block";
  } else {
     domain.style.display = "none";
  } 
  
              if (checkBoxSsl.checked == true){
    ssl.style.display = "block";
  } else {
     ssl.style.display = "none";
  } 
  
                if (checkBoxContent.checked == true){
    content.style.display = "block";
  } else {
     content.style.display = "none";
  } 
  
                  if (checkBoxNews.checked == true){
    newsletter.style.display = "block";
  } else {
     newsletter.style.display = "none";
  } 
  
                    if (checkBoxFbSocial.checked == true){
    fsocial.style.display = "block";
  } else {
     fsocial.style.display = "none";
  } 
  
                      if (checkBoxFbLead.checked == true){
    flead.style.display = "block";
  } else {
     flead.style.display = "none";
  }
  
    if (checkBoxInsta.checked == true){
    instagram.style.display = "block";
  } else {
     instagram.style.display = "none";
  }
  
      if (checkBoxPint.checked == true){
    pinterest.style.display = "block";
  } else {
     pinterest.style.display = "none";
  }
  

  
    
        if (checkBoxLinkd.checked == true){
    linkedin.style.display = "block";
  } else {
     linkedin.style.display = "none";
  }
  
          if (checkBoxYt.checked == true){
    ytmarketing.style.display = "block";
  } else {
     ytmarketing.style.display = "none";
  }
  
          if (checkBoxGraphic.checked == true){
    graphic.style.display = "block";
  } else {
     graphic.style.display = "none";
  }
  
            if (checkBoxCorp.checked == true){
    corp.style.display = "block";
  } else {
     corp.style.display = "none";
  }
  
              if (checkBoxAppDev.checked == true){
    appdev.style.display = "block";
  } else {
     appdev.style.display = "none";
  }
  
                if (checkBoxAppM.checked == true){
    appmaint.style.display = "block";
  } else {
     appmaint.style.display = "none";
  }
  
    
                if (checkBoxManagement.checked == true){
    management.style.display = "block";
  } else {
     management.style.display = "none";
  }
                  if (checkBoxRetainer.checked == true){
    retainer.style.display = "block";
  } else {
     retainer.style.display = "none";
  }
  
                    if (checkBoxMportal.checked == true){
    marketingp.style.display = "block";
  } else {
     marketingp.style.display = "none";
  }
  
                      if (checkBoxCars.checked == true){
    cars.style.display = "block";
  } else {
     cars.style.display = "none";
  }
  
   if (checkBoxPetrol.checked == true){
    petrol.style.display = "block";
  } else {
     petrol.style.display = "none";
  }
}
</script>
    
    </div>
     
        <div class="form-group">
         <label for="post_status">Client Total Ex Vat</label>
           <?php echo "<input type='number' class='form-control' name='client_total_ex' value='" . $client_total . "' >" ?>
     </div>  
      
      
            <div class="form-group">
         <label for="post_status">Client Total Inc Vat</label>
           <?php echo "<input type='number' class='form-control' name='client_total_inc' value='" . $client_total_inc . "' >" ?>
     </div>  
 
         <div class="form-group">
         <label for="post_status">Client Montly Ex Vat</label>
           <?php echo "<input type='number' class='form-control' name='client_month_ex' value='" . $client_month_ex . "' >" ?>
     </div>  
           
     <div class="form-group">
         <label for="post_status">Client Montly Inc Vat</label>
           <?php echo "<input type='number' class='form-control' name='client_month_inc' value='" . $client_month_inc . "'>" ?>
     </div>       


      
    
<div class="form-group">

    <input class="btn btn-primary" type="submit" name="renew_client" value="Renew Client">

     </div>

</form> 

    </div>

</div>

<script type="text/javascript">

$(document).ready(function() {

    $('select').material_select();

});

</script>
<script>
        
        (function($) {
  $.fn.conditionize = function(options){ 
    
     var settings = $.extend({
        hideJS: true
    }, options );
    
    $.fn.showOrHide = function(listenTo, listenFor, $section) {
      if ($(listenTo).is('select, input[type=text]') && $(listenTo).val() == listenFor ) {
        $section.slideDown();
      }
      else if ($(listenTo + ":checked").val() == listenFor) {
        $section.slideDown();
      }
      else {
        $section.slideUp();
      }
    } 

    return this.each( function() {
      var listenTo = "[name=" + $(this).data('cond-option') + "]";
      var listenFor = $(this).data('cond-value');
      var $section = $(this);
  
      //Set up event listener
      $(listenTo).on('change', function() {
        $.fn.showOrHide(listenTo, listenFor, $section);
      });
      //If setting was chosen, hide everything first...
      if (settings.hideJS) {
        $(this).hide();
      }
      //Show based on current value on page load
      $.fn.showOrHide(listenTo, listenFor, $section);
    });
  }
}(jQuery));
  
 $('.conditional').conditionize();</script>