
 <?php //include("../includes/date_form.php"); ?>
<?php //include("../includes/export_element.php"); ?>
<?php include("../includes/search_form.php"); ?>

 <style>
* {
  box-sizing: border-box;
}

#myInput {
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
}

#myTable th, #myTable td {
  text-align: left;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>
<div style="overflow-x:auto">
<!--    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">-->

<table  class="table table-bordered" style="table-layout: fixed">
    <thead>
        <tr class="header">
            <th>ID</th>
            <th>STATUS</th>
            <th>DATE</th>
            <th>TIME</th>
            <th>SALES</th>
            <th>BUSINESS</th>
            <th>AREA</th>
            <th>VIEW</th>
            <th>EDIT</th>
<!--            <th>Approve</th>-->
            <th>RESCHEDULE</th>
 

        </tr>

    </thead>
    <tbody id="myTable">
                               
                                   
   <?php
   
$query = "SELECT * FROM appointments WHERE app_rescheduled = 'No' ORDER BY app_date DESC";
$select_appointments = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_appointments)) {
    
    $app_id             = $row['app_id'];
    $app_business       = $row['app_business'];
    $app_address        = $row['app_address'];
    $app_area           = $row['app_area'];
    $app_name           = $row['app_name'];
    $app_number         = $row['app_number'];
    $app_alt_number     = $row['app_alt_number'];
    $app_website        = $row['app_website'];
    $app_comments       = $row['app_comments'];
    $app_email          = $row['app_email'];
    $app_competitors    = $row['app_competitors'];
    $app_time           = $row['app_time'];
    $app_date           = $row['app_date'];
    $app_approved       = $row['app_approved'];
    $app_telesales      = $row['app_telesales'];
    $app_consultant     = $row['app_consultant'];

$querya = "SELECT * FROM users WHERE user_firstname = '$app_telesales' ";
$select_users = mysqli_query($connection,$querya);
while($row = mysqli_fetch_assoc($select_users)) {
    
$user_id        = $row['user_id'];
$user_team      = $row['user_team'];
$user_number    = $row['user_number'];
 
echo "<tr>";
echo "<td>$app_id</td>";
echo "<td>$app_approved" . "<br />" . "<a href='appointments.php?approve={$app_id}'><i class='fa fa-thumbs-up'></i></a> <a href='appointments.php?source=comment_appointment&p_id={$app_id}'><i class='fa fa-thumbs-down'></i></a>" . "</td>";
echo "<td>$app_date</td>";
echo "<td>$app_time</td>";
echo "<td>$app_consultant</td>";
echo "<td>$app_business</td>";


echo "<td>$app_area</td>";    

echo "<td><a href='appointments.php?source=single_appointment&p_id={$app_id}'>View</a></td>";
echo "<td><a href='appointments.php?source=edit_appointment&p_id={$app_id}'>Edit</a></td>";
echo "<td><a href='appointments.php?source=reschedule_appointment&p_id={$app_id}'>Reschedule</a></td>";
echo "</tr>";

       }    }                     
 ?>
                           
              </tbody>
                        </table>
                        <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 1; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</div>
                        
                        <?php


if(isset($_GET['approve'])) {
    
    $the_app_id = $_GET['approve'];
    
    
    $query = "UPDATE appointments SET app_approved = 'approved' WHERE app_id = $the_app_id ";
    $send_payments = mysqli_query($connection, $query);
    confirm($send_payments );
    header("Location:appointments.php");
}

if(isset($_GET['unapproved'])) {
    
    $the_app_id = $_GET['unapproved'];
    $query = "UPDATE appointments SET app_approved = 'unapproved' WHERE app_id = $the_app_id ";
    $approve_app_query = mysqli_query($connection, $query);
    header("Location:appointments.php");
 
}

?>