<div style="overflow-x:auto">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>User Number</th>
            <th>User Group</th>
            <th>User Team</th>
            <th>User Status</th>
            <th>User Colour</th>
            <th>Payment Details</th>
            <th>ID Doc</th>
            <th>Activate User</th>
            <th>Edit</th>
        </tr>

    </thead>
    <tbody>           
                                   
   <?php
   
$query = "SELECT * FROM users WHERE (user_group = 'telesales' OR user_group = 'telesalesr') AND user_status = 'active'";
$select_users = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_users)) {
$user_id            = $row['user_id'];
$user_firstname     = $row['user_firstname'];
$user_lastname      = $row['user_lastname'];
$user_email         = $row['user_email'];
$user_number        = $row['user_number'];
$user_group         = $row['user_group'];
$user_team          = $row['user_team'];
$user_status        = $row['user_status'];
$user_colour        = $row['user_colour'];
$user_payment       = $row['user_pay_details'];
$user_docs          = $row['user_documents'];

echo "<tr>";
echo "<td>$user_id</td>";
echo "<td>$user_firstname</td>";
echo "<td>$user_lastname</td>";
echo "<td>$user_email</td>";    
echo "<td>$user_number</td>";    
echo "<td>$user_group</td>";
echo "<td>$user_team</td>";
echo "<td>$user_status</td>";
echo "<td>$user_colour</td>";
echo "<td>$user_payment</td>";
echo "<td><a href='../user_documents/$user_docs' target='_blank'>Download ID</td>";
echo "<td><a href='users.php?activate={$user_id}'>Activate</a></td>";
echo "<td><a href='users.php?source=edit_user&edit_user={$user_id}'>Edit</a></td>";
echo "</tr>";

       }                        
 ?>
                            </tbody>
                        </table>
                        </div>
<?php

if(isset($_GET['activate'])) {
    
    $the_user_id = $_GET['activate'];
    $query = "UPDATE users SET user_status = 'active' WHERE user_id = $the_user_id ";
    $activate_user_query = mysqli_query($connection, $query);
    header("Location:users.php");
    
}

if(isset($_GET['change_to_sub'])) {
    
    $the_user_id = $_GET['change_to_sub'];
    $query = "UPDATE users SET user_role = 'subscriber' WHERE user_id = $the_user_id ";
    $change_to_sub_query = mysqli_query($connection, $query);
    header("Location:users.php");
    
}

if(isset($_GET['delete'])) {
    
    $the_user_id = $_GET['delete'];
    $query = "DELETE FROM users WHERE user_id = {$the_user_id}";
    $delete_user_query = mysqli_query($connection, $query);
    header("Location:users.php");
    
}

?>