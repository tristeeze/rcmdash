<?php include "../includes/dashboard_header.php" ?>
    <div id="wrapper">

        <?php include "includes/ext_telesales_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                   <h1 class="page-header">
                            BOOKED APPOINTMENTS
                        
                        </h1>
    
                    
                    <?php
// Set your timezone
date_default_timezone_set('Africa/Johannesburg');

// Get prev & next month
if (isset($_GET['ym'])) {
    $ym = $_GET['ym'];
} else {
    // This month
    $ym = date('Y-m');
}
// Check format
$timestamp = strtotime($ym . '-01');
if ($timestamp === false) {
    $ym = date('Y-m');
    $timestamp = strtotime($ym . '-01');
}
// Today
$today = date('Y-m-j', time());
// For H3 title
$html_title = date('Y / m', $timestamp);
// Create prev & next month link     mktime(hour,minute,second,month,day,year)
$prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
$next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));
// You can also use strtotime!
// $prev = date('Y-m', strtotime('-1 month', $timestamp));
// $next = date('Y-m', strtotime('+1 month', $timestamp));
// Number of days in the month
$day_count = date('t', $timestamp);
 
// 0:Sun 1:Mon 2:Tue ...
$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));
//$str = date('w', $timestamp);
// Create Calendar!!
$weeks = array();
$week = '';

   

// Add empty cell
$week .= str_repeat('<td></td>', $str);
 
for ( $day = 1; $day <= $day_count; $day++, $str++) {
     
    $date = $ym . '-' . $day;
        
    if ($today == $date ) {
        $week .= '<td class="today">' . $day . '<br />';
        
    } 
else {
     $week .= '<td>' . $day . '<br />';
}
    
       $query = "SELECT * FROM appointments WHERE app_date = '{$date}'";
    
$select_appointments = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_appointments)) {
$app_time = $row['app_time'];
$app_consultant = $row['app_consultant'];
    


    $query = "SELECT * FROM users WHERE user_firstname = '$app_consultant'";
    
$get_colour = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($get_colour)) {

    $user_colour = $row['user_colour'];

    $week .= $app_time . " " . "<span class='usercolor' title='User " . $user_colour . "' style='background-color:" . $user_colour . ";'></span>" . '<br />';
 }
    }
   
    $week .= '</td>';
     

    // End of the week OR End of the month
    if ($str % 7 == 6 || $day == $day_count) {
        if ($day == $day_count) {
            // Add empty cell
            $week .= str_repeat('<td></td>', 6 - ($str % 7));
        }
        $weeks[] = '<tr>' . $week . '</tr>';
        // Prepare for new week
        $week = '';
    }
}


?>
                    
                    <div class="container">
        <h3><a href="?ym=<?php echo $prev; ?>">&lt;</a> <?php echo $html_title; ?> <a href="?ym=<?php echo $next; ?>">&gt;</a></h3>
        <table class="table table-bordered">
            <tr>
           <th>SUNDAY</th>
                <th>MONDAY</th>
                <th>TUESDAY</th>
                <th>WEDNESDAY</th>
                <th>THURSDAY</th>
                <th>FRIDAY</th>
                <th>SATURDAY</th>
            </tr>
            <?php
            
             
      foreach ($weeks as $week) {
                    echo  $week;
                   
                }
    
              
            ?>
        </table>
    </div>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        
        <!-- /#page-wrapper -->

  <?php include "includes/ext_telesales_footer.php" ?>   