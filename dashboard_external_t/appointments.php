<?php include "../includes/dashboard_header.php" ?>
    <div id="wrapper">

        <?php include "includes/ext_telesales_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                   <h1 class="page-header">
                            BOOK AN APPOINTMENT
                            <small>TELESALES</small>
                        </h1>
    <?php
    
        if(isset($_GET['source'])) {
            
            $source = $_GET['source'];
            
            
        } else {
            
            $source = '';
        }

switch($source) {
        case 'add_appointment_ext';
        include "../views/telesales/add_appointment_ext.php";
        break ;
            
        case 'view_feedback';
        include "../views/telesales/view_feedback_telesales.php";
        break;
        
        case 'reschedule_appointment';
        include "../views/telesales/reschedule_appointment_telesales.php";
        break;        
        case 'view_appointments_telesales';
        include "../views/telesales/view_appointments_telesales.php";
        break;
            
        default: 
        include "../views/telesales/view_appointments_telesales.php";
        break;
}
    
    ?>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        
        <!-- /#page-wrapper -->

  <?php include "includes/ext_telesales_footer.php" ?>   