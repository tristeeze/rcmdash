<?php

session_start();

function redirect($location){
    header("Location:" . $location);
//    exit;
    die();
}

function confirm($result) {

    global $connection;
     if(!$result) {

        die("FAIL" . mysqli_error($connection));
    }
}


//----------------------------------------------------------------------------------------- FILTER DATE PROCESSED DEALS
function filter_date_processed() {

    global $connection;

if(isset($_POST['filter'])) {

    $from_date = $_POST['from_date'];
    $to_date = $_POST['to_date'];

$query = "SELECT * FROM processed_deals WHERE process_date BETWEEN '$from_date' AND '$to_date'";
$select_deals = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_deals))

{

$process_id = $row['process_id'];
$process_business = $row['process_business'];
$process_consultant = $row['process_consultant'];
$process_value = $row['process_value'];
$process_comments = $row['process_comments'];
$process_proof = $row['process_proof'];
$process_date = $row['process_date'];

echo "<tr>";
echo "<td>$process_id</td>";
echo "<td>$process_business</td>";
echo "<td>$process_consultant</td>";
echo "<td>$process_value</td>";
echo "<td>$process_comments</td>";
echo "<td><a href='../proofs/$process_proof'>DOWNLOAD</a><img class='img-responsive' src=''></td>";
echo "<td>$process_date</td>";
echo "</tr>";

       }
} else {
    $query = "SELECT * FROM processed_deals";
$select_deals = mysqli_query($connection,$query);
while($row = mysqli_fetch_assoc($select_deals))  {

$process_id = $row['process_id'];
$process_business = $row['process_business'];
$process_consultant = $row['process_consultant'];
$process_value = $row['process_value'];
$process_comments = $row['process_comments'];
$process_proof = $row['process_proof'];
$process_date = $row['process_date'];
    echo "<tr>";
echo "<td>$process_id</td>";
echo "<td>$process_business</td>";
echo "<td>$process_consultant</td>";
echo "<td>$process_value</td>";
echo "<td>$process_comments</td>";
echo "<td><a href='../proofs/$process_proof'>DOWNLOAD</a><img class='img-responsive' src=''></td>";
echo "<td>$process_date</td>";

}}
}

////////////////////////////////////////////////////////////////////password_hash

function ifItIsMethod($method=null){

    if($_SERVER['REQUEST_METHOD'] == strtoupper($method)){

        return true;

    }

    return false;

}

function countLikes($connection, $post_id, $all = false) {


    $user_id = $_SESSION['user_id'];

    if(!$all)
    {
      $stmt = mysqli_prepare($connection, "SELECT COUNT(id) as 'l_count' FROM likes WHERE post_id = ? AND user_id = ?");
      mysqli_stmt_bind_param($stmt, 'ii', $post_id, $user_id);

    }
    else
    {
      $stmt = mysqli_prepare($connection, "SELECT COUNT(id) as 'l_count' FROM likes WHERE post_id = ?");
      mysqli_stmt_bind_param($stmt, 'i', $post_id);

    }

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $l_count);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);





    return $l_count;
}

function updateLike($post_id)
{
    include 'includes/db.php';

    $user_id = $_SESSION['user_id'];

    $l_count = countLikes($connection, $post_id);

    if($l_count == 0)
    {
        $stmt = mysqli_prepare($connection, "INSERT INTO likes (post_id, user_id) VALUES(?, ?)");

        mysqli_stmt_bind_param($stmt, 'ii', $post_id, $user_id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        echo "<strong> <span class='glyphicon glyphicon-thumbs-down'> </span> Dislike </strong>";
    }
    else
    {
        $stmt = mysqli_prepare($connection, "DELETE FROM likes WHERE post_id = ? AND user_id = ?");

        mysqli_stmt_bind_param($stmt, 'ii', $post_id, $user_id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        echo "<strong> <span class='glyphicon glyphicon-thumbs-up'> </span> Like </strong>";
    }
}


if(ifItIsMethod('GET'))
{
    if(isset($_GET['p_count']))
    {
        include 'includes/db.php';
        $post_id = $_GET['p_count'];

        echo countLikes($connection, $post_id, true);

        exit;
    }
}

if(ifItIsMethod('POST'))
{
    if(isset($_POST['post_id']))
    {
        $post_id = $_POST['post_id'];

        updateLike($post_id);

        exit;
    }
}

function isLoggedIn(){

    if(isset($_SESSION['role'])){

        return true;


    }


   return false;

}

function checkIfUserIsLoggedInAndRedirect($redirectLocation=null){

    if(isLoggedIn()){

        redirect($redirectLocation);

    }

}

function escape($string) {

    global $connection;

    return mysqli_real_escape_string($connection, trim($string));


}



function set_message($msg){

    if(!$msg) {

    $_SESSION['message']= $msg;

    } else {

    $msg = "";


        }

}


function display_message() {

    if(isset($_SESSION['message'])) {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }


}


function confirmQuery($result) {

    global $connection;

    if(!$result ) {

          die("QUERY FAILED ." . mysqli_error($connection));


    }

    return true;

}



function insert_categories(){

    global $connection;

        if(isset($_POST['submit'])){

            $cat_title = $_POST['cat_title'];

        if($cat_title == "" || empty($cat_title)) {

             echo "This Field should not be empty";

    } else {


    $stmt = mysqli_prepare($connection, "INSERT INTO categories(cat_title) VALUES(?) ");

    mysqli_stmt_bind_param($stmt, 's', $cat_title);

    mysqli_stmt_execute($stmt);


        if(!$stmt) {
        die('QUERY FAILED'. mysqli_error($connection));

                  }



             }


    mysqli_stmt_close($stmt);


       }

}


function findAllCategories() {
global $connection;

    $query = "SELECT * FROM categories";
    $select_categories = mysqli_query($connection,$query);

    while($row = mysqli_fetch_assoc($select_categories)) {
    $cat_id = $row['cat_id'];
    $cat_title = $row['cat_title'];

    echo "<tr>";

    echo "<td>{$cat_id}</td>";
    echo "<td>{$cat_title}</td>";
   echo "<td><a href='categories.php?delete={$cat_id}'>Delete</a></td>";
   echo "<td><a href='categories.php?edit={$cat_id}'>Edit</a></td>";
    echo "</tr>";

    }


}


function deleteCategories(){
global $connection;

    if(isset($_GET['delete'])){
    $the_cat_id = $_GET['delete'];
    $query = "DELETE FROM categories WHERE cat_id = {$the_cat_id} ";
    $delete_query = mysqli_query($connection,$query);
    header("Location: categories.php");


    }



}


function UnApprove() {
    global $connection;
    if(isset($_GET['unapprove'])){

        $the_comment_id = $_GET['unapprove'];

        $query = "UPDATE comments SET comment_status = 'unapproved' WHERE comment_id = $the_comment_id ";
        $unapprove_comment_query = mysqli_query($connection, $query);
        header("Location: comments.php");


    }
}


function is_admin($user_firstname) {

    global $connection;

    $query = "SELECT user_role FROM users WHERE username = '$user_firstname'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);

    $row = mysqli_fetch_array($result);

    if($row['user_role'] == 'admin'){
        return true;
    }else {
        return false;
    }
}

function username_exists($user_firstname){

    global $connection;

    $query = "SELECT username FROM users WHERE username = '$user_firstname'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);

    if(mysqli_num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function email_exists($email){

    global $connection;

    $query = "SELECT user_email FROM users WHERE user_email = '$email'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);

    if(mysqli_num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

?>