<?php include'includes/header.php' ?>
    <?php include'includes/db.php' ?>


               <div class="form-gap"></div>
                <div class="container">
                   <div class="row">
                       <div class="col-md-4 col-md-offset-4">
                           <div class="panel panel-default">
                               <div class="panel-body formstyle">
                                 <div class="site-branding">

                                     <img src="images/rcm-logo.png" width="100%" style="width:100% !important;" />

                                 </div>
                                   <div class="text-center">


                                           <h3><i class="fa fa-lock fa-4x"></i></h3>
                                           <h2 class="text-center">Registration</h2>
                                           <div class="panel-body">

                                             <!-- Blog Entries Column -->
                                              <h4 align="center">Thank You for your registration<br />Please keep a lookout on your email for your account activation</h4>

                                            </div>
                                        </div>
                                  </div>
                          </div>
                    </div>
                </div>
              </div>

<?php
class Redirect {
  public static function to($location = null){
    if($location) {
      header('Location: ' . $location);
      exit();
    }
  }
}
?>

        <hr>
<?php include'includes/footer.php' ?>
