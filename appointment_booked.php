<?php include'includes/header.php' ?>
    <?php include'includes/db.php' ?>

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

<style>
/* Login */
.bookCallwrap {
    width: 100%;
    background: url(images/background_login.jpg);
    background-attachment: fixed;
    background-size: 100%;
    overflow: hidden;
    min-height: 100vh;
}
body {
    padding-top: 0px !important;
    font-size: 12px;
    font-family: 'Montserrat', sans-serif;
    
}
.callwrap {
    background: #000000bd;
    color: #fff;
    width:60%;
    margin: auto;
    min-height: 100vh;
}

.panel-default {
    border-color: rgba(0,0,0,0);
}
button.btn.btn-primary.btn-lg.btn-block {
    border-radius:0 50px 50px 0;
}
    .input-group-addon {
    padding: 6px 12px;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    color: #555;
    text-align: center;
    background-color: #fff;
    border: 0px solid #ccc;
    border-radius: 100%;
}
.panel {
    background-color: #fff0 !important;
}
/* Login */
</style>

<div class="bookCallwrap">

    <!-- Page Content -->
<!--    <div class="container">-->

    <div class="row">
  <div class="col-sm-12">
        <div class="form-gap"></div>
<!--        <div class="container">-->
            <div class="row bookCall">
                <div class="callwrap">
                    <div class="panel panel-default">
                        <div class="panel-body ">
     
                            <div class="text-center">


                                    <h1 class="text-center">APPOINTMENT BOOKED</h1>
                                    <div class="panel-body">

                                     <p>Thank you your appointment has been booked</p>

                                        

                                        <!-- Blog Sidebar Widgets Column -->
                                   
                                        <hr>
                                        <?php include'includes/footer.php' ?>

                                    </div><!-- Body-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--        </div> container -->
  </div>
  <div class="col-sm-6"></div>
</div>

</div><!-- loginwrap -->