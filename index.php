<?php include'includes/header.php' ?>
    <?php include'includes/db.php' ?>

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

<style>
/* Login */
.loginwrap {
    width: 100%;
    height: 100vh;
    background: url(images/background_login.jpg);
    background-size: 100%;
    overflow: hidden;
}
body {
    padding-top: 0px !important;
    font-size: 12px;
    font-family: 'Montserrat', sans-serif;
}
.loginleft {
    background: #000000bd;
    height: 100vh;
    color: #fff;
}
.formwrap {
    top: %;
    width: 50%;
    margin: auto;
    display: block;
    position: relative;
}
.panel-default {
    border-color: rgba(0,0,0,0);
}
button.btn.btn-primary.btn-lg.btn-block {
    border-radius:0 50px 50px 0;
}
    .input-group-addon {
    padding: 6px 12px;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    color: #555;
    text-align: center;
    background-color: #fff;
    border: 0px solid #ccc;
    border-radius: 100%;
}
.panel {
    background-color: #fff0 !important;
}
/* Login */
</style>

<div class="loginwrap">

    <!-- Page Content -->
<!--    <div class="container">-->

    <div class="row">
  <div class="col-sm-6">
        <div class="form-gap"></div>
<!--        <div class="container">-->
            <div class="row loginleft">
                <div class="formwrap">
                    <div class="panel panel-default">
                        <div class="panel-body formstyle">
                            <h4 align="center">WELCOME TO</h4>
                          <div class="site-branding">

                              <img src="images/rcm-logo.png" width="100%" style="width:100% !important;" />

                          </div>
                            <div class="text-center">


                                    <h3><i class="fa fa-lock fa-4x"></i></h3>
                                    <p class="text-center">Login to your profile</p>
                                    <div class="panel-body">

                                      <?php if(isset($message)) { ?>
                                      <div class="messagereq <?php echo $type; ?>"><?php echo $message; ?></div>
                                      <?php } ?>

                                        <form action="includes/login.php" id="register-form" role="form" autocomplete="on" class="form" method="post">

                                          <div class="form-group">
                                              <div class="input-group">
                                              <span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
                                                <input name="user_firstname" type="text" class="form-control" placeholder="Enter Username" required>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <div class="input-group">
                                              <span class="input-group-addon"><i class="glyphicon glyphicon-lock color-blue"></i></span>
                                                 <input name="password" type="password" class="form-control" placeholder="Enter Password" required>
                                              </div>
                                          </div>

                                            <span class="input-group-btn">
                                                <button class="btn btn-primary btn-lg btn-block" name="login" type="submit"><span>LOGIN</span></button>

                                            </span>
                                        </form>

                                        <!-- Blog Sidebar Widgets Column -->
                                    <div ><a href="forgot.php?forgot=<?php echo uniqid(true); ?>">FORGOT PASSWORD</a> | <a href="register.php?source=register_user">REGISTER</a></div>
                                        <hr>
                                        <?php include'includes/footer.php' ?>

                                    </div><!-- Body-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--        </div> container -->
  </div>
  <div class="col-sm-6"></div>
</div>

</div><!-- loginwrap -->