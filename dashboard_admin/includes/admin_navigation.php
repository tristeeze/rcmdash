<!-- ADMIN Navigation -->
             <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                 <li class="useravatarmobi">
                        <div class="useravatar">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
            </div>
                <a class="navbar-brand" href="index.php">RCM ADMIN CMS  <span style="color:#fff;"></span></a>
                           
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php 
                        
                        if(isset($_SESSION['user_firstname'])) {
                        echo $_SESSION['user_firstname']; 
                        }
                        
                        
                        ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                      
                        <li class="divider"></li>
                        <li>
                            <a href="../includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li>
                        <div class="useravatar useravatardrop">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
                    <li>
                        <a href="index.php"><i class="fas fa-globe"></i> Dashboard</a>
                    </li> 
               <!--    
                                   <li>
                                          <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="far fa-calendar-plus" ></i> My Appointments <i class="fa fa-fw fa-caret-down"></i></a>
                                         
                                                              <ul id="demo" class="collapse">
                                                                         <li>
                                                  <a href="appointments.php?source=add_appointment">Add Appointment</a>
                                              </li>
                                            
                                               <li>
                                                  <a href="appointments.php?source=view_appointments">View All Appointments</a>
                                              </li>
                                          </ul>
                                      </li> -->
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fas fa-users"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="demo3" class="collapse">
                            <li>
                                <a href="users.php">View All Users</a>
                            </li>
                            <li>
                                <a href="users.php?source=add_user">Add User</a>
                            </li>
                        </ul>
                    </li>
                     <!-- <li >
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fas fa-chart-pie"></i> Statistics<i class="fas fa-fw fa-caret-down"></i></a>
                        
                        <ul id="demo2" class="collapse">
                         <li >
                        <a href="statistics.php?source=view_statistics"> View Appointments</a>
                                         </li>
                         <li >
                        <a href="statistics.php?source=appointment_summaries"> Consultant</a>
                                         </li>
                          <li >
                        <a href="statistics.php?source=telesales_summaries"> Telesales </a>
                                         </li>
                         <li >
                        <a href="statistics.php?source=p_telesales_summaries">Public Telesales </a>
                                         </li>
                            <li >
                        <a href="statistics.php?source=sales_vs_telesales">Appointment Leaderboard</a>
                                         </li>
                           <li >
                        <a href="statistics.php?source=revenue_chart">Revenue Chart </a>
                                         </li>
                         </ul>
                                         </li> -->
                <!--      <li >
                        <a href="pipeline.php"><i class="fas fa-user"></i> Pipline</a>
                                         </li>  -->
                                         <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#van"><i class="fa fa-calendar"></i> Book Transport <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="van" class="collapse">
                            <li>
                                <a href="book_van.php?source=book_van">BOOK VAN</a>
                            </li> 
                               <li>
                                <a href="book_van.php?source=view_van">VAN AVAILABILITY</a>
                            </li> 
                           
                          
                        </ul>
                    </li>
                        <li >
                        <a href="profile.php"><i class="fas fa-user"></i> Profile</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>