<?php include "includes/telesales_header.php" ?>
    <div id="wrapper">

        <?php include "includes/telesales_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                   <h1 class="page-header">
                            CALENDAR
                            <small>TELESALES</small>
                        </h1>
    <?php
    
        if(isset($_GET['source'])) {
            
            $source = $_GET['source'];
            
            
        } else {
            
            $source = '';
        }

        switch($source) {
                
        case 'view_calendar';
        include "../views/global/calendar.php";
        break ;
                         
        case 'lead_calendar';
        include "../views/telesales/lead_calendar.php";
        break ;
                  
        default: 
        include "../views/global/calendar.php";
        break;
}
    
    ?>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        
        <!-- /#page-wrapper -->

  <?php include "includes/telesales_footer.php" ?>   