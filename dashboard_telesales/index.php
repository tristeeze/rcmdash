<?php include "includes/telesales_header.php" ?>

    <div id="wrapper">
        

        <?php include "includes/telesales_navigation.php" ?>

        <div id="page-wrapper">

            
                
                <div class="container-fluid">

                <!-- Page Heading -->
<div class="row">
<div class="col-lg-12">
    <h1 class="page-header">
    WELCOME TO THE TELESALES DASHBOARD
    
    <small><?php echo $_SESSION['user_firstname'] ?></small>
    </h1>

                        
                       <?php
              
    $current_month = date("m");
   
    $sql="SELECT sum(feedback_sale_value) as total FROM feedback WHERE MONTH(feedback_date) = $current_month AND feedback_telesales = '{$_SESSION['user_firstname']}'";
    $result = mysqli_query($connection,$sql);
    $row = mysqli_fetch_assoc($result);   

    ?>
     
                <a class="navbar-brand" href="index.php">YOUR MONTHLY PROCESSED REVENUE <?php echo "<strong>R {$row['total']}</strong>"; ?></a>
    </div>
          <div class="col-lg-12">
         
                   
  <form method="post">
   
   <div class="row">

  <div class="col-sm-4"><div class="form-group">
         <label for="post_status">From Date</label>
           <input type="date" class="form-control" name="from_date">
     </div></div>
  <div class="col-sm-4"> <div class="form-group">
         <label for="post_status">To Date</label>
           <input type="date" class="form-control" name="to_date">
     </div></div>
</div>

        <input type="submit" value="search" name="filter" class="button btn"> 
</form> 
   <?php

?>

     <div class="row">
    <div class="col-sm-6">
        <?php include "../views/telesales/telesales_chart_approved.php" ?>
    </div>
    <div class="col-sm-6">
        <?php include "../views/telesales/telesales_chart_status.php" ?>
    </div>
      </div>
      
     
      
                    </div>
                </div>

            </div>
            
            
            <!-- /.container-fluid -->

        </div>
        
        
        <!-- /#page-wrapper -->

  <?php include "includes/telesales_footer.php" ?>
   