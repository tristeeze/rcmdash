<?php include "../includes/dashboard_header.php" ?>
            <div id="wrapper">

        <?php include "includes/admin_navigation.php" ?>

        <div id="page-wrapper">

                <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                   <h1 class="page-header">
                          ARTICLES
                            
                        </h1>
    <?php
    
        if(isset($_GET['source'])) {
            
            $source = $_GET['source'];
            
        } else {
            
            $source = '';
        }

switch($source) {
 case 'add_keywords';
        include "../views/articles_manager/add_keywords.php";
        break ; 
    
case 'claim_article';
        include "../views/articles_manager/claim_article.php";
        break ;  
        
case 'approved_articles';
        include "../views/articles_manager/approved_articles.php";
        break ;
                     
case 'submit_article';
        include "../views/articles_manager/submit_article.php";
        break ;
    
    case 'article_feedback';
        include "../views/articles_manager/article_feedback.php";
        break ;       
        
case 'submit_article_form';
        include "../views/articles_manager/submit_article_form.php";
        break ;
    
    case 'my_articles';
        include "../views/articles_manager/my_articles.php";
        break ;   
        
    case 'approved_articles';
        include "../views/articles_manager/approved_articles.php";
        break ;
             
        default: 
        include "../views/articles_manager/all_articles.php";
        break;
}
    
    ?>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
        </div>
        
        <!-- /#page-wrapper -->

  <?php include "includes/admin_footer.php" ?>   