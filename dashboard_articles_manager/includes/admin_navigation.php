<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <li class="useravatarmobi">
                        <div class="useravatar">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
            </div>
                <a class="navbar-brand" href="index.php">RCM ADMIN CMS  <span style="color:#fff;"></span></a>
                           
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php 
                        
                        if(isset($_SESSION['user_firstname'])) {
                        echo $_SESSION['user_firstname']; 
                        }
                        
                        
                        ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                      
                        <li class="divider"></li>
                        <li>
                            <a href="../includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li>
                        <div class="useravatar useravatardrop">
                            <?php
                                $user_firstname = $_SESSION['user_firstname'];
                                echo $user_firstname[0];
                          ?>
                        </div>
                    </li>
                    <li>
                        <a href="index.php"><i class="fas fa-globe"></i> Dashboard</a>
                    </li> 
               <!--    
                                   <li>
                                          <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="far fa-calendar-plus" ></i> My Appointments <i class="fa fa-fw fa-caret-down"></i></a>
                                         
                                                              <ul id="demo" class="collapse">
                                                                         <li>
                                                  <a href="appointments.php?source=add_appointment">Add Appointment</a>
                                              </li>
                                            
                                               <li>
                                                  <a href="appointments.php?source=view_appointments">View All Appointments</a>
                                              </li>
                                          </ul>
                                      </li> -->
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fas fa-users"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="demo3" class="collapse">
                            <li>
                                <a href="users.php">View All Users</a>
                            </li>
                            <li>
                                <a href="users.php?source=add_user">Add User</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#articles"><i class="fas fa-users"></i> Articles <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="articles" class="collapse">
                            <li>
                                <a href="articles.php">View All Articles</a>
                            </li>
                            <li>
                                <a href="articles.php?source=add_keywords">Add Keywords</a>
                            </li>   
                               <li>
                                <a href="articles.php?source=claim_article">Claim Article</a>
                            </li>    
                               <li>
                                <a href="articles.php?source=submit_article">Submit Article</a>
                            </li>
                               
                               <li>
                                <a href="articles.php?source=my_articles">My Articles</a>
                            </li>   
                               <li>
                                <a href="articles.php?source=approved_articles">Approved Articles</a>
                            </li>
                        </ul>
                    </li>
                     
                                         <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#van"><i class="fa fa-calendar"></i> Book Transport <i class="fa fa-fw fa-caret-down"></i></a>
                       
                                            <ul id="van" class="collapse">
                            <li>
                                <a href="book_van.php?source=book_van">Book Van</a>
                            </li> 
                               <li>
                                <a href="book_van.php?source=view_van">Van Availability</a>
                            </li> 
                           
                          
                        </ul>
                    </li>
                        <li >
                        <a href="profile.php"><i class="fas fa-user"></i> Profile</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>